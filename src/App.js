import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./redux/global_auth/actions";
import IdleTimer from "react-idle-timer";
import postAPI from "./utils/postAPITemplate";
// ...
// handleConnectivityChange = (isConnected) => {
//   store.dispatch(action_saveNetInfo(isConnected));
// };
// import { DashboardPage } from './dashboard/Dashboard'
// import PropTypes from 'prop-types';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import './App.css'

// import config from "./config/dev_keys";
import "./App.scss";
import isEmpty from "./utils/isEmpty";

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">Loading...</div>
);

// Containers
const DefaultLayout = React.lazy(() => import("./containers/DefaultLayout"));

// Pages
// const Login = React.lazy(() => import("./modules/billing/components/group"));

const Login = React.lazy(() => import("./modules/global_auth/login"));
const Register = React.lazy(() => import("./containers/DefaultLayout"));
const Page404 = React.lazy(() => import("./containers/DefaultLayout"));
const Page500 = React.lazy(() => import("./containers/DefaultLayout"));

// Check if token exists
if (localStorage.dfs_clientSecret) {
  // Set auth token header auth
  setAuthToken(localStorage.dfs_clientSecret);
  // Decode token and get user info and exp
  //const decoded = jwt_decode(localStorage.clientSecret);
  const decoded = jwt_decode(localStorage.dfs_clientSecret);

  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded, localStorage.usertype));

  // Check for expired token
  const currentTime = Date.now() / 1000;

  if (decoded.exp < currentTime) {
    // const token = store.dispatch(refreshToken(localStorage.clientSecret));
    // localStorage.setItem("clientSecret", token);
    // setAuthToken(token);
    // const decoded = jwt_decode(token);
    // store.dispatch(setCurrentUser(decoded));
    // Token Refresh
    // Logout user
    store.dispatch(logoutUser());
    // Redirect to login
    window.location.href = "/login";
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { ...store.getState() };
    this.state = {
      timeout: 1000 * 60 * 5,
      showModal: false,
      userLoggedIn: false,
      isTimedOut: false,
    };

    //  this.idleTimer = null;
    this.onAction = this._onAction.bind(this);
    this.onActive = this._onActive.bind(this);
    this.onIdle = this._onIdle.bind(this);
  }

  _onActive(e) {
    this.setState({ isTimedOut: false });
  }
  _onAction = (e) => {
    this.setState({ isTimedOut: false });
  };

  _onIdle(e) {
    const isTimedOut = this.state.isTimedOut;
    if (isTimedOut) {
    } else {
      this.logoutUserForce();
      this.idleTimer.reset();
      this.setState({ isTimedOut: true });
    }
  }

  logoutUserForce = async () => {
    let url = "";
    if (localStorage.getItem("usertype") === "U") {
      url = "dfs/user/logout";
    } else {
      url = "dfs/agent/logout";
    }
    const data = { url: url, showMessage: "S" };

    if (localStorage.getItem("userData")) {
      const resp = await postAPI(data);

      if (resp && !isEmpty(resp)) {
      }

      localStorage.removeItem("dfs_clientMenu");
      localStorage.removeItem("dfs_clientSecret");
      localStorage.removeItem("usertype");
      localStorage.removeItem("userData");
      setAuthToken(false);

      window.location.href = "/login";
    }
  };

  render() {
    return (
      <Provider store={store}>
        <IdleTimer
          ref={(ref) => {
            this.idleTimer = ref;
          }}
          //   element={document}
          // onActive={this.onActive}
          // stopOnIdle={true}
          onIdle={this.onIdle}
          //onAction={this.onAction}
          debounce={0}
          timeout={this.state.timeout}
        />
        <BrowserRouter basename="/">
          <React.Suspense fallback={loading()}>
            <Switch>
              <Route
                exact
                path="/login"
                name="Login Page"
                render={(props) => <Login {...props} />}
              />
              <Route
                exact
                path="/register"
                name="Register Page"
                render={(props) => <Register {...props} />}
              />
              <Route
                exact
                path="/404"
                name="Page 404"
                render={(props) => <Page404 {...props} />}
              />
              <Route
                exact
                path="/500"
                name="Page 500"
                render={(props) => <Page500 {...props} />}
              />
              <Route
                path="/"
                name="Home"
                render={(props) => <DefaultLayout {...props} />}
              />
            </Switch>
          </React.Suspense>
        </BrowserRouter>
      </Provider>
    );
  }
}
//const mapStateToProps = (state) => ({});
export default App;
//export default connect(mapStateToProps, {})(App);
