export default [
  {
    name: "Dashboard",
    url: "/",
    icon: "icon-speedometer"
  },
  {
    name: "Create user",
    url: "/create-user",
    icon: "icon-speedometer"
  },
  {
    name: "Update user",
    url: "/update-user",
    icon: "icon-speedometer"
  }
];
