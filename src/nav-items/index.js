let menu = [];

if (localStorage.dfs_clientMenu) {
  menu = JSON.parse(localStorage.dfs_clientMenu);
  // console.log(menu[1]);
  //Testing URL
}

const staticLinks = [
  {
    name: "Dashboard",
    url: "/",
    icon: "icon-doc",
  },
];

export default { items: [...staticLinks, ...menu] };
