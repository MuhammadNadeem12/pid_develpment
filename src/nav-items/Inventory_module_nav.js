export default [
  {
    name: "Inventory",
    url: "/inventory",
    icon: "icon-speedometer"
  },
  {
    name: "Base",
    url: "/base",
    icon: "icon-puzzle",
    children: [
      {
        name: "Breadcrumbs",
        url: "/base/breadcrumbs",
        icon: "icon-puzzle"
      }
    ]
  }
];
