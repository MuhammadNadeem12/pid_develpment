import axios from "axios";

import config from "../config";
//import isEmpty from "./isEmpty";

import ErrorHandler from "./errorHandler";
import { notificationMessages } from "./messageNotification";
export default async (params) => {
  try {
    const url = config.BASE_URL + params.url;
    //return "DAta";
    // console.log(url);

    const res = await axios.get(url);
    if (res && res.data) {
      if (res.data.responsecode && res.data.responsecode === 1) {
        if (params.showMessage === "S")
          notificationMessages("success", res.data.messages);
        return res.data.data;
      } else if (res.data.responsecode && res.data.responsecode === 0) {
        if (params.showMessage === "S")
          notificationMessages("error", res.data.messages);
        return res.data.data;
      }
    } else notificationMessages("error", "Something went Wrong,Try Again!");
  } catch (err) {
    const e = ErrorHandler(err);
    if (e && e.length > 0) {
      console.log(e[0].payload.type, e[0].payload.msg);
      // notificationMessages(e[0].payload.type, e[0].payload.msg);
    }
  }
};
