import jsonData from "./visit-policy.json";
import isEmpty from "../isEmpty";

const checkCase1 = (data, case1) => {
  const billgroupindex = case1.billgrp.findIndex(
    (item) => item === data.billgrp
  );
  const retationindex = case1.relation.findIndex(
    (item) => item === data.relation
  );
  if (
    billgroupindex > -1 &&
    retationindex > -1 &&
    case1.gender === data.gender &&
    case1.age < data.age &&
    case1.disable === data.disable
  ) {
    return true;
  } else {
    return false;
  }
};

const checkCase2 = (data, case2) => {
  const billgroupindex = case2.billgrp.findIndex(
    (item) => item === data.billgrp
  );
  const retationindex = case2.relation.findIndex(
    (item) => item === data.relation
  );
  if (
    billgroupindex > -1 &&
    retationindex > -1 &&
    case2.gender === data.gender &&
    case2.maristatus === data.maristatus &&
    case2.disable === data.disable
  ) {
    return true;
  } else {
    return false;
  }
};

const checkCase3 = (data, case3) => {
  const billgroupindex = case3.billgrp.findIndex(
    (item) => item === data.billgrp
  );

  const retationindex = case3.relation.findIndex(
    (item) => item === data.relation
  );

  if (
    billgroupindex > -1 &&
    retationindex > -1 &&
    case3.pensioner === data.pensioner &&
    case3.type === data.type
  ) {
    return true;
  } else {
    return false;
  }
};

export default (patientDate) => {
  const allPolicies = jsonData.data;
  if (!isEmpty(patientDate)) {
    if (!isEmpty(allPolicies) && allPolicies.length > 0) {
      // process here
      if (
        checkCase1(patientDate, allPolicies[0]) ||
        checkCase2(patientDate, allPolicies[1]) ||
        checkCase3(patientDate, allPolicies[2])
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return false; // no policy defined
    }
  } else {
    return false; // no patield data passed
  }
};

// -------- false // show add visit - policy doesn't apply
// -------- true // don't show visit - policy applies
