import isEmptyCheck from "./isEmpty";
import moment from "moment";

const formateDate = (rowData) => {
  if (!isEmptyCheck(rowData)) {
    let dob_date = moment(rowData).format("YYYY-MM-DD HH:mm:ss");
    return dob_date;
  }
};

export default formateDate;
