const getTree = (array) => {
  var o = {};
  array.forEach((a) => {
    if (o[a.menuId] && o[a.menuId].children) {
      a.children = o[a.menuId] && o[a.menuId].children;
    }
    if (a.parent_id !== null) a.icon = "";
    o[a.menuId] = a;
    o[a.parent_id] = o[a.parent_id] || {};
    o[a.parent_id].children = o[a.parent_id].children || [];

    o[a.parent_id].children.push(a);
  });
  return o.null.children;
};

export default getTree;
