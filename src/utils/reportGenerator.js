import jsPDF from "jspdf";
import "jspdf-autotable";

export const generatePdf = (groups) => {
  const unit = "pt";
  const size = "A4"; // Use A1, A2, A3 or A4
  const orientation = "landscape"; // portrait or landscape

  // nature  is the one
  // nature is nature and naut
  const doc = new jsPDF(orientation, unit, size);

  // var width = doc.internal.pageSize.getWidth();
  //var height = doc.internal.pageSize.getHeight();

  doc.setFontSize(11);

  // const title = "Fauji Foundation  Hospital Rawalpindi";
  let col = [
    { dataKey: "Initiated_By", header: "Initiated By" },
    { dataKey: "Transaction_Amount", header: "Transaction Amount" },
    { dataKey: "Transaaction_Date", header: "Transaction Date" },
    { dataKey: "Transaction_Type", header: "Transaction Type" },
    { dataKey: "From_GL_Wallet", header: "From GL/Wallet" },
    { dataKey: "To_GL_Wallet", header: "To GL/Wallet " },
  ];
  // const headers = [
  //   "Mr No",
  //   "Inv#",
  //   "Patient Name",
  //   "Type",
  //   "Doctor/Dept Name",
  //   "Date& Time",
  //   "Amount",
  // ];

  // const data = groups.map(elt=> [elt.mrno,elt.invnum,elt.patientname,elt.billgrp,elt.doctorname,elt.invcredate, elt.amount]);

  // let content = {
  //   startY: 50,
  //   head: headers,
  //   body: data
  // };

  var header = function () {
    // var imgData =  // Convert the image to base64 and place it here //

    doc.setFontStyle("normal");

    // move_from_left, move_from_height, width, height
    // doc.addImage(imgData, 'JPEG', 5, 10, width-10, 65)

    doc.setFontSize(14);
    doc.setFontStyle("bold");

    // move_from_left, move_from_height
    doc.text(330, 30, "GL STATEMENT\n");

    //doc.addImage(imageBase64);
    // doc.addImage(imageBase64, "JPEG", 200, 20, 200, 50);
  };

  var footer = function () {
    // var imgData = // Convert the image to base64 and place it here //
    //print number bottom right
    // doc.setFontSize(7);
    // doc.text(width - 40, height - 30, "Page - " + doc.page);
    // doc.page++;
    //_________________________________
    // doc.addImage(imgData, 'JPEG', 5, height - 25, width-10, 30)
  };

  var options = {
    beforePageContent: header,
    afterPageContent: footer,
    theme: "grid",

    style: { cellWidth: "auto" },
    margin: { top: 50, bottom: 100, horizontal: 10 },

    // didDrawCell: (data) => {
    //   if (data.section === 'body' && data.column.index === 0) {
    //     console.log("date on row",data)

    //   }
    // }
  };

  // doc.text(title, marginLeft, 40);
  doc.autoTable(col, groups, options);
  //generateSummary(groups, doc);

  // doc.autoTable(content);
  //doc.save("report.pdf");
  window.open(doc.output("bloburl"), "_blank");
};

export const generateSummary = (groups, doc) => {
  // const title = "Fauji Foundation  Hospital Rawalpindi";
  // let col = [
  //   { dataKey: "mrnum", header: "Mr#" },
  //   { dataKey: "patientname", header: "Patient Name" },
  //   { dataKey: "calDateTime", header: "Date & Time" },
  //   { dataKey: "billnum", header: "BillNum" },
  //   { dataKey: "billto", header: "Bill To" },
  //   { dataKey: "calReceivedBy", header: "User " },
  //   { dataKey: "creditcash", header: "Type " },
  //   { dataKey: "amount", header: "Amount " },
  // ];
  // var options = {
  //   theme: "plain",

  //   style: { cellWidth: "auto" },
  //   margin: { top: 50, bottom: 100, horizontal: 10 },
  // colsapn

  //   // drawHeaderRow: function (row, data) {
  //   //   return false;
  //   // },
  // };

  doc.autoTable({
    head: [["Total:", "", "", "", "", "", "" + getTotal(groups)]],

    columnStyles: { Total: { halign: "center" }, colSpan: 5 }, // European countries centered

    theme: "plain",
  });
};

const getTotal = (cashLog) => {
  let total = 0;
  cashLog.forEach((element) => {
    total = total + parseFloat(element.amount);
  });

  return total;
};
