import { NotificationManager } from "react-notifications";
export const notificationMessages = (type, msg) => {
  // console.log(params);
  //   if (
  //     params &&
  //     !isEmptyCheck(params) &&
  //     params.payload.type &&
  //     params.payload.msg
  //   ) {
  //     const type = params.payload.type;
  //     const msg = params.payload.msg;
  switch (type) {
    case "info":
      NotificationManager.info(msg);
      break;
    case "success":
      NotificationManager.success(msg);
      break;
    case "warning":
      NotificationManager.warning(msg);
      break;
    case "error":
      NotificationManager.error(msg);
      break;
    default:
      break;
  }
  //  }
};
