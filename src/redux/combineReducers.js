import { combineReducers } from "redux";

import authReducer from "./global_auth/reducer";
import alertReducer from "./alert-error/alertReducer";
import errorReducer from "./alert-error/errorReducer";
import usersReducer from "./user_module/reducer";
import lovReducer from "./lov/reducer";
import roleReducer from "./role_module/reducer";
import menuReducer from "./menu_module/reducer";

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  alerts: alertReducer,
  users: usersReducer,
  lov: lovReducer,
  role_module: roleReducer,
  menu_Reducer: menuReducer,
});
