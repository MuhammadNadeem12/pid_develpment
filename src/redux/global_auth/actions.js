import axios from "axios";
import { GET_ALERTS, LOGIN_USER, CLEAR_ERRORS, CLEAR_ALERTS } from "../types";

import ErrorHandler from "../../utils/errorHandler";

//import jwt_decode from "jwt-decode";
import setAuthToken from "../../utils/setAuthToken";

import generateNav from "../../utils/generateNav";

import config from "../../config";
import isEmpty from "../../utils/isEmpty";

export const loginUser = (params) => (dispatch) => {
  let url = "";
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  url = config.BASE_URL + "pid/user/login";
  axios.defaults.headers.common["Authorization"] = config.INIT_TOKEN;
  axios
    .post(url, params)
    .then((res) => {
      const { data } = res.data;
      //console.log(data);
      const { token } = data;
      let menu = "";
      if (data.menu && data.menu.length > 0) {
        menu = JSON.stringify(generateNav(data.menu));
      }
      localStorage.setItem("dfs_clientMenu", menu);
      localStorage.setItem("dfs_clientSecret", token);
      localStorage.setItem("usertype", params.usertype);
      setAuthToken(token);
      let decoded = "";  
      //userData = "";
      if (data.tblUsers && !isEmpty(data.tblUsers)) {
        decoded = data.tblUsers;
        //userData = JSON.stringify(data.tblUsers);
      }
      dispatch(setCurrentUser(decoded, "U"));
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
  delete axios.defaults.headers.common["Authorization"];
 // dispatch(setCurrentUser(true));
};

// Set logged in user
export const setCurrentUser = (decoded, usertype) => {
  return {
    type: LOGIN_USER,
    payload: decoded,
    usertype: usertype,
  };
};

export const welcomeMsg = (name) => (dispatch) => {
  dispatch({
    type: GET_ALERTS,
    payload: {
      type: "success",
      msg: "Welcome To EMO",
    },
  });
};

// Log user out
export const logoutUser = (params) => (dispatch) => {
  //Remove token from localStorage

  // Remove auth header for future requests

  // Set current user to {} which will set isAuthenticated to false

  //let url = "";
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });
  localStorage.removeItem("dfs_clientMenu");
  localStorage.removeItem("dfs_clientSecret");
  localStorage.removeItem("usertype");
  localStorage.removeItem("userData");
  setAuthToken(false);
  dispatch(setCurrentUser({}));
  window.location.href = "/login";
};

export const refreshToken = (currToken) => (dispatch) => {
  // API call for refresh token
};
