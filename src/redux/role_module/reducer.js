import {
  GET_ALL_ROLES,
  ADD_ROLE,
  ADD_ROLE_DETAIL,
  GET_ALL_ROLERIGHT,
} from "../types";

const initialState = {
  roles: [],
  roleright: [],
  allmodules: [],
  role: {},
  loading: true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_ROLES:
      return {
        ...state,
        roles: action.payload,
        loading: false,
      };
    case GET_ALL_ROLERIGHT:
      return {
        ...state,
        roleright: action.payload,
        loading: false,
      };
    case ADD_ROLE:
      return {
        ...state,
        roles: [action.payload, ...state.roles],
        loading: false,
      };

    case ADD_ROLE_DETAIL:
      return {
        ...state,
        roleright: [action.payload, ...state.roleright],
        loading: false,
      };

    default:
      return state;
  }
};
