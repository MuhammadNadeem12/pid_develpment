import axios from "axios";

import config from "../../config";

import ErrorHandler from "../../utils/errorHandler";

import {
  GET_ALL_ROLES,
  CLEAR_ALERTS,
  CLEAR_ERRORS,
  GET_ALERTS,
  ADD_ROLE,
  ADD_ROLE_DETAIL,
  GET_ALL_ROLERIGHT,
} from "../types";

export const getAllRoles = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });

  const url = config.BASE_URL + "pid/getAllRoles";

  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;

      dispatch({
        type: GET_ALL_ROLES,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const createRole = (params) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/createRole";

  axios
    .post(url, params)
    .then((res) => {
      const { data } = res.data;

      dispatch({
        type: ADD_ROLE,
        payload: data,
      });
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: "success",
          msg: "Role has been created successfully",
        },
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};
export const updateRole = (params) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/updateRole";

  axios
    .post(url, params)
    .then((res) => {
      //  const { data } = res.data;

      dispatch(getAllRoles());
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: "success",
          msg: res.data.messages,
        },
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const createRoleDetail = (params) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/createRoleRights";

  axios
    .post(url, params)
    .then((res) => {
      const { data } = res.data;

      dispatch({
        type: ADD_ROLE_DETAIL,
        payload: data,
      });
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: "success",
          msg: "Role Detail has been created successfully",
        },
      });
    })
    .catch((err) => {
      console.log(err);
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getAllRoleRights = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });

  const url = config.BASE_URL + "pid/getAllRoleRights";

  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;

      dispatch({
        type: GET_ALL_ROLERIGHT,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const updateRoleDetail = (params) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "/pid/updateRoleRights";

  axios
    .post(url, params)
    .then((res) => {
      // const { data } = res.data;

      dispatch(getAllRoleRights());
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: "success",
          msg: res.data.messages,
        },
      });
    })
    .catch((err) => {
      console.log(err);
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};
