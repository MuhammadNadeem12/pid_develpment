import {
  GET_USERS,
  CHANGE_PASS,
  GET_USER_AGAINSTNAME,
  CHANGE_SPAC_USER_PASS,
  USER_PROFILE,
  GET_EMPLOYEES,
} from "../types";

const initialState = {
  users: [],
  employees: [],
  user: {},
  userprofile: {},
  password: {},
  useraginstusername: {},
  loading: true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        users: action.payload,
        loading: false,
      };
    case CHANGE_PASS:
      return {
        ...state,
        password: action.payload,
        loading: false,
      };
    case CHANGE_SPAC_USER_PASS:
      return {
        ...state,
        password: action.payload,
        loading: false,
      };
    case GET_USER_AGAINSTNAME:
      return {
        ...state,
        useraginstusername: action.payload,
      };
    case USER_PROFILE:
      return {
        ...state,
        userprofile: action.payload,
        loading: false,
      };
    case GET_EMPLOYEES:
      return {
        ...state,
        employees: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};
