import axios from "axios";

import config from "../../config";

import ErrorHandler from "../../utils/errorHandler";

import {
  GET_USERS,
  GET_ALERTS,
  CLEAR_ALERTS,
  CLEAR_ERRORS,
  CHANGE_PASS,
  GET_USER_AGAINSTNAME,
  CHANGE_SPAC_USER_PASS,
  USER_PROFILE,
  GET_EMPLOYEES,
} from "../types";

export const getUsers = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });

  const url = config.BASE_URL + "dfs/getAllUsers";

  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_USERS,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const updateUser = (params, history) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "dfs/updateUser";

  axios
    .post(url, params)
    .then((res) => {
      console.log(res.data);
      dispatch(getUsers());
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: "success",
          msg: res.data.messages,
        },
      });
      // history.push(`${config.BASE_URL}/users`);
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const changeUserPassword = (params) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/changePassword";

  axios
    .post(url, params)
    .then((res) => {
      const { data } = res.data;

      dispatch({
        type: CHANGE_PASS,
        payload: data,
      });
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: "success",
          msg: res.data.messages,
        },
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const addUser = (params) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });
  const url = config.BASE_URL + "dfs/createUser";
  axios
    .post(url, params)
    .then((res) => {
      dispatch(getUsers());
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: "success",
          msg: res.data.messages,
        },
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
          dispatch({
            type: GET_ALERTS,
            payload: {
              type: "danger",
              msg: "User is not created",
            },
          });
        });
      }
    });
};
export const getUserAgaintUserName = (name) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });

  const url = config.BASE_URL + "dfs/getUserByUserName/" + name;

  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_USER_AGAINSTNAME,
        payload: data,
      });
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: "success",
          msg: res.data.messages,
        },
      });
    })

    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const changeSpecificUserPassword = (params) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "dfs/changeUserPassword";

  axios
    .post(url, params)
    .then((res) => {
      const { data } = res.data;

      dispatch({
        type: CHANGE_SPAC_USER_PASS,
        payload: data,
      });
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: "success",
          msg: res.data.messages,
        },
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const loginUserprofile = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "dfs/getloginResponseInfo";

  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      console.log(data);
      dispatch({
        type: USER_PROFILE,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getEmployes = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });

  const url = config.BASE_URL + "pid/getAllEmployees";

  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_EMPLOYEES,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};
