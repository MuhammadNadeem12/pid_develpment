import axios from "axios";
import config from "../../config";
import ErrorHandler from "../../utils/errorHandler";
import {
  GET_ROLE_LOV,
  CLEAR_ALERTS,
  CLEAR_ERRORS,
  GET_COUNTRY_LOV,
  GET_CITY_LOV,
  GET_PROVINCE_LOV,
  GET_CITYDIS_LOV,
  GET_DISTRICT_LOV,
  GET_PARENTMENU_LOV,
  GL_ACCOUNT_LOV,
  GL_TYPE_LOV,
  GET_MENU_LOV,
  GET_CHARGES_LOV,
  GET_RECPROVINCE_LOV,
  GET_RECCITYDIS_LOV,
  GET_RECDISTRICT_LOV,
  GET_PARENT_AGENTS_LOV,
  GET_CHANNEL_LOV,
  GET_TRANSDOC_LOV,
  GET_FEEGL_LOV,
  GET_COMISSIONGL_LOV,
  GET_POSTALPRODUCT_LOV,
  GET_COMPTYPE_LOV,
  GET_ALLAGENTS_LOV,
  GET_ACCOUNTSTATUS_LOV,
  GET_ACCOUNTLEVEL_LOV,
  GET_TEHSIL,
  GET_TOWN_LOV,
  GET_TALUKA_LOV,
  GET_TRANSMOD_LOV,
} from "../types";

export const getRoleLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovRole";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_ROLE_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getCountryLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovCountry";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_COUNTRY_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getCityLov = (countryid) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovCityWithCountry/" + countryid;
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_CITY_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getProvinceLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovProvince";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_PROVINCE_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getCityWithDistrictidLov = (districtid) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovCityWithDistrict/" + districtid;
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_CITYDIS_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};
export const getDisctrictWithProvinceidLov = (provinceid) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovDistrict/" + provinceid;
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_DISTRICT_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getParentMenuLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovParentMenu";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_PARENTMENU_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getMenuLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovMenu";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_MENU_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getGlAccountLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovGlChildAccount";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GL_ACCOUNT_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};
export const getGlTypeLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovGlType";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GL_TYPE_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getChargesLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lkpCharge";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_CHARGES_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getReceiverProvinceLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovProvince";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;

      dispatch({
        type: GET_RECPROVINCE_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getReceiverCityWithDistrictidLov = (districtid) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovCityWithDistrict/" + districtid;
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_RECCITYDIS_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};
export const getReceiverDisctrictWithProvinceidLov = (provinceid) => (
  dispatch
) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });
  const url = config.BASE_URL + "pid/lovDistrict/" + provinceid;
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_RECDISTRICT_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getparentAgentsLOV = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovParentAgent";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_PARENT_AGENTS_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getlkpChannel = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });
  const url = config.BASE_URL + "pid/lkpChannel";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_CHANNEL_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getlovFeeGl = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });
  const url = config.BASE_URL + "pid/lovFeeGl";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_FEEGL_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getlovCommissionGl = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });
  const url = config.BASE_URL + "pid/lovCommissionGl";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_COMISSIONGL_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getlovTransDocs = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });
  const url = config.BASE_URL + "pid/lovTransDocs";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_TRANSDOC_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};
export const getPostalProductLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovPostalProduct";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_POSTALPRODUCT_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getlovCompanyType = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovCompanyType";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_COMPTYPE_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getlovAgents = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovAgents";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_ALLAGENTS_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getlovAccountStatus = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovAccountStatus";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_ACCOUNTSTATUS_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getlovAccountLevel = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovAccountLevel";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_ACCOUNTLEVEL_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getTehsilLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovTehsil";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_TEHSIL,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getTownLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovTown";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_TOWN_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const getTalukaLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovTaluka";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_TALUKA_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};
export const getTransModLov = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/lovTransMode";
  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;
      dispatch({
        type: GET_TRANSMOD_LOV,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};
