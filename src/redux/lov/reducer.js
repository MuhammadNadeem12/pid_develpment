import {
  GET_ROLE_LOV,
  GET_CITY_LOV,
  GET_COUNTRY_LOV,
  GET_PROVINCE_LOV,
  GET_CITYDIS_LOV,
  GET_DISTRICT_LOV,
  GET_PARENTMENU_LOV,
  GL_ACCOUNT_LOV,
  GL_TYPE_LOV,
  GET_MENU_LOV,
  GET_CHARGES_LOV,
  GET_RECPROVINCE_LOV,
  GET_RECCITYDIS_LOV,
  GET_RECDISTRICT_LOV,
  GET_PARENT_AGENTS_LOV,
  GET_CHANNEL_LOV,
  GET_TRANSDOC_LOV,
  GET_FEEGL_LOV,
  GET_COMISSIONGL_LOV,
  GET_POSTALPRODUCT_LOV,
  GET_COMPTYPE_LOV,
  GET_ALLAGENTS_LOV,
  GET_ACCOUNTSTATUS_LOV,
  GET_ACCOUNTLEVEL_LOV,
  GET_TEHSIL,
  GET_TOWN_LOV,
  GET_TALUKA_LOV,
  GET_TRANSMOD_LOV,
} from "../types";

const initialState = {
  role_lov: [],
  country_lov: [],
  city_lov: [],
  province_lov: [],
  citydis_lov: [],
  district_lov: [],
  recpro_lov: [],
  reccitydis_lov: [],
  recdistrict_lov: [],
  parentmanu_lov: [],
  manu_lov: [],
  glaccount_lov: [],
  gttype_lov: [],
  charges_lov: [],
  parentAgents_lov: [],
  channel_lov: [],
  transdoc_lov: [],
  feegl_lov: [],
  comgl_lov: [],
  postalproduct_lov: [],
  companytype_lov: [],
  agents_Lov: [],
  accstatus_Lov: [],
  accLevel_Lov: [],
  tehsil_Lov: [],
  town_Lov: [],
  taluka_Lov: [],
  transmod_Lov: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ROLE_LOV:
      return {
        ...state,
        role_lov: action.payload,
      };
    case GET_CITY_LOV:
      return {
        ...state,
        city_lov: action.payload,
      };
    case GET_COUNTRY_LOV:
      return {
        ...state,
        country_lov: action.payload,
      };
    case GET_PROVINCE_LOV:
      return {
        ...state,
        province_lov: action.payload,
      };
    case GET_CITYDIS_LOV:
      return {
        ...state,
        citydis_lov: action.payload,
      };
    case GET_DISTRICT_LOV:
      return {
        ...state,
        district_lov: action.payload,
      };
    case GET_RECPROVINCE_LOV:
      return {
        ...state,
        recpro_lov: action.payload,
      };
    case GET_RECCITYDIS_LOV:
      return {
        ...state,
        reccitydis_lov: action.payload,
      };
    case GET_RECDISTRICT_LOV:
      return {
        ...state,
        recdistrict_lov: action.payload,
      };
    case GET_PARENTMENU_LOV:
      return {
        ...state,
        parentmanu_lov: action.payload,
      };
    case GET_MENU_LOV:
      return {
        ...state,
        manu_lov: action.payload,
      };
    case GL_ACCOUNT_LOV:
      return {
        ...state,
        glaccount_lov: action.payload,
      };
    case GL_TYPE_LOV:
      return {
        ...state,
        gttype_lov: action.payload,
      };
    case GET_CHARGES_LOV:
      return {
        ...state,
        charges_lov: action.payload,
      };
    case GET_PARENT_AGENTS_LOV:
      return {
        ...state,
        parentAgents_lov: action.payload,
      };
    case GET_CHANNEL_LOV:
      return {
        ...state,
        channel_lov: action.payload,
      };
    case GET_COMISSIONGL_LOV:
      return {
        ...state,
        comgl_lov: action.payload,
      };
    case GET_TRANSDOC_LOV:
      return {
        ...state,
        transdoc_lov: action.payload,
      };
    case GET_FEEGL_LOV:
      return {
        ...state,
        feegl_lov: action.payload,
      };
    case GET_POSTALPRODUCT_LOV:
      return {
        ...state,
        postalproduct_lov: action.payload,
      };
    case GET_COMPTYPE_LOV:
      return {
        ...state,
        companytype_lov: action.payload,
      };
    case GET_ALLAGENTS_LOV:
      return {
        ...state,
        agents_Lov: action.payload,
      };
    case GET_ACCOUNTSTATUS_LOV:
      return {
        ...state,
        accstatus_Lov: action.payload,
      };
    case GET_ACCOUNTLEVEL_LOV:
      return {
        ...state,
        accLevel_Lov: action.payload,
      };
    case GET_TEHSIL:
      return {
        ...state,
        tehsil_Lov: action.payload,
      };
    case GET_TOWN_LOV:
      return {
        ...state,
        town_Lov: action.payload,
      };
    case GET_TALUKA_LOV:
      return {
        ...state,
        taluka_Lov: action.payload,
      };
    case GET_TRANSMOD_LOV:
      return {
        ...state,
        transmod_Lov: action.payload,
      };

    default:
      return state;
  }
};
