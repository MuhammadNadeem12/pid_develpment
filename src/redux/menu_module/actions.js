import axios from "axios";

import config from "../../config";

import ErrorHandler from "../../utils/errorHandler";
import {
  GET_ALL_MENUS,
  ADD_MENU,
  CLEAR_ERRORS,
  CLEAR_ALERTS,
  GET_ALERTS,
} from "../types";

export const getAllMenus = () => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });

  const url = config.BASE_URL + "pid/getAllMenu";

  axios
    .get(url)
    .then((res) => {
      const { data } = res.data;

      dispatch({
        type: GET_ALL_MENUS,
        payload: data,
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const addMenu = (params) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });

  const url = config.BASE_URL + "pid/createMenu";

  axios
    .post(url, params)
    .then((res) => {
      const { data } = res.data;

      dispatch({
        type: ADD_MENU,
        payload: data,
      });
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: "success",
          msg: "Menu has been created successfully",
        },
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};

export const updateMenuRow = (params) => (dispatch) => {
  dispatch({
    type: CLEAR_ALERTS,
  });
  dispatch({
    type: CLEAR_ERRORS,
  });
  const url = config.BASE_URL + "pid/updateMenu";
  axios
    .post(url, params)
    .then((res) => {
      dispatch(getAllMenus());
      dispatch({
        type: GET_ALERTS,
        payload: {
          type: "success",
          msg: "Menu  has been Updated successfully",
        },
      });
    })
    .catch((err) => {
      const e = ErrorHandler(err);
      if (e && e.length > 0) {
        e.forEach((item) => {
          dispatch(item);
        });
      }
    });
};
