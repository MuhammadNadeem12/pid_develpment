import { GET_ALL_MENUS, ADD_MENU, GET_MENU } from "../types";

const initialState = {
  allMenus: [],
  menu: {},
  loading: true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_MENUS:
      return {
        ...state,
        allMenus: action.payload,
        loading: false,
      };
    case ADD_MENU:
      return {
        ...state,
        allMenus: [action.payload, ...state.allMenus],
        loading: false,
      };
    case GET_MENU:
      return {
        ...state,
        MENU: action.payload,
        loading: false,
      };

    default:
      return state;
  }
};
