import DEV from "./dev_keys";
import PROD from "./prod_keys";

import ENV from "./environment";

let environment = {};

if (ENV === "development") {
  environment = DEV;
} else {
  environment = PROD;
}

export default environment;
