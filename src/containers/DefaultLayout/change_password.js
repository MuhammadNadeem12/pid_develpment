import React, { Component } from "react";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import PasswordValidation from "./validation/ChangePasswordValidation";
import Loading from "../../utils/spinner";

const initialState = {
  current_pass: "",
  new_pass: "",
  confirm_pass: "",
  errors: {}
};

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  onChangeInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  passwordChange = params => {};

  formSubmit = e => {
    e.preventDefault();

    const data = {
      current_pass: this.state.current_pass,
      new_pass: this.state.new_pass,
      confirm_pass: this.state.confirm_pass
    };

    const { haserror, errors } = PasswordValidation(data);
    if (haserror) {
      this.setState({ errors });
    } else {
      //   this.props.createRole(data);
      //   this.setState(initialState);
      //   this.props.modelOpen();
    }
  };
  render() {
    const { errors, loading } = this.state;
    let content = "";
    if (loading) {
      content = <Loading />;
    } else {
      content = (
        <Row>
          <Col md="12">
            <FormGroup>
              <Label>Current Password</Label>
              <Input
                type="password"
                value={this.state.current_pass}
                name="current_pass"
                onChange={this.onChangeInput}
                placeholder="Enter Current Password"
              />
              <p className="error">{errors.current_pass}</p>
            </FormGroup>
          </Col>
          <Col md="12">
            <FormGroup>
              <Label>New Password</Label>
              <Input
                type="password"
                value={this.state.new_pass}
                name="new_pass"
                onChange={this.onChangeInput}
                placeholder="Enter New Password"
              />
              <p className="error">{errors.new_pass}</p>
            </FormGroup>
          </Col>

          <Col md="12">
            <FormGroup>
              <Label>Confirm Password</Label>
              <Input
                type="password"
                value={this.state.confirm_pass}
                name="confirm_pass"
                onChange={this.onChangeInput}
                placeholder="Enter Confirm Password"
              />
              <p className="error">{errors.confirm_pass}</p>
            </FormGroup>
          </Col>
        </Row>
      );
    }

    return (
      <div>
        <Modal
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader toggle={this.props.modelOpen}>
            Change Password
          </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button type="submit" color="primary">
                Submit
              </Button>
              <Button color="secondary" onClick={this.props.modelOpen}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

ChangePassword.propTypes = {
  modelOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors //STORE
});

export default connect(mapStateToProps)(ChangePassword);
