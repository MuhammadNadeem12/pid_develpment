import validator from "validator";
import isEmptyCheck from "../../../utils/isEmpty";

const ChangePasswordValidation = data => {
  let errors = {};
  const curr_pass = isEmptyCheck(data.current_pass)
    ? ""
    : data.current_pass.trim().toString();
  const new_pass = isEmptyCheck(data.new_pass) ? "" : data.new_pass.toString();
  const conf_pass = isEmptyCheck(data.confirm_pass)
    ? ""
    : data.confirm_pass.toString();

  if (validator.isEmpty(curr_pass)) {
    errors.current_pass = "Current Password is Required";
  }
  if (validator.isEmpty(new_pass)) {
    errors.new_pass = "New Password is Required";
  }
  if (validator.isEmpty(conf_pass)) {
    errors.confirm_pass = "Confirm Password is Required";
  }
  if (!validator.equals(new_pass, conf_pass)) {
    errors.confirm_pass = "Both passwords must be same";
  }

  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors
  };
  return feedback;
};
export default ChangePasswordValidation;
