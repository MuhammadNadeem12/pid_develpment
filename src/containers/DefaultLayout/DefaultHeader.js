import React, { Component } from "react";
import {
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
} from "reactstrap";
import PropTypes from "prop-types";
import {} from "react-router";
import { AppNavbarBrand, AppSidebarToggler } from "@coreui/react";
import { connect } from "react-redux";
import { logoutUser } from "../../redux/global_auth/actions";
import logo1 from "../../assets/HomePageIcon.png";
import logo from "../../assets/img/logo.png";
import { withRouter } from "react-router-dom";
const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor() {
    super();
    this.state = { modelOpen: false, currentBalance: 1000, usertype: "" };
  }
  Logout = () => {
    let data = {};
    this.props.logoutUser(data);
  };

  changePassword = () => {
    this.props.history.push("/changepassword");
  };
  changeAgentPassword = () => {
    this.props.history.push("/changeagentpassword");
  };
  userProfile = () => {
    this.props.history.push("/userprofile");

    //window.open("/dfs/userprofile");
  };
  agentProfile = () => {
    this.props.history.push("/agentprofile");
  };
  componentDidMount() {
    this.setState({ usertype: this.props.authenticated.usertype });
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.authenticated && nextProps.authenticated.user) {
    }
    if (
      nextProps &&
      nextProps.authenticated &&
      nextProps.authenticated.usertype
    ) {
      this.setState({ usertype: nextProps.authenticated.usertype });
    }
  }
  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;
    let { usertype } = this.state;
    let content = "";
    if (usertype === "A") {
      content = (
        <div>
          <DropdownItem onClick={this.agentProfile}>
            <i className="fa fa-user" />
            Agent Profile
          </DropdownItem>
          <DropdownItem onClick={this.changeAgentPassword}>
            <i className="icon-lock"></i>
            Change Password
          </DropdownItem>
        </div>
      );
    } else {
      content = (
        <div>
          {/* <DropdownItem onClick={this.userProfile}>
            <i className="fa fa-user" />
            User Profile
          </DropdownItem> */}
          <DropdownItem onClick={this.changePassword}>
            <i className="icon-lock"></i>
            Change Password
          </DropdownItem>
        </div>
      );
    }

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 60, height: 50, alt: "PID" }}
          minimized={{ src: logo, width: 30, height: 30, alt: "PID" }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar></Nav>
        <Nav className="ml-auto" navbar>
          {/* <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link">
              <i className="icon-bell"></i>
              <Badge pill color="danger">
                5
              </Badge>
            </NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link">
              <i className="icon-list"></i>
            </NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link">
              <i className="icon-location-pin"></i>
            </NavLink>
          </NavItem> */}
          {/* <h4>
            <span style={{ color: "white" }}>
              Current Balance : {this.state.currentBalance}
            </span>
          </h4> */}
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <img src={logo1} className="img-avatar" alt="" />
            </DropdownToggle>
            <DropdownMenu right>
              {/* <DropdownItem header tag="div" className="text-center">
                <strong>Account</strong>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-bell-o"></i> Updates
                <Badge color="info">42</Badge>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-envelope-o"></i> Messages
                <Badge color="success">42</Badge>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-tasks"></i> Tasks
                <Badge color="danger">42</Badge>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-comments"></i> Comments
                <Badge color="warning">42</Badge>
              </DropdownItem>
              <DropdownItem header tag="div" className="text-center">
                <strong>Settings</strong>
              </DropdownItem> */}

              {content}
              <DropdownItem onClick={this.Logout}>
                <i className="fa fa-lock" /> Logout
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        {/* <AppAsideToggler className="d-md-down-none" /> */}
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const mapStateToProps = (state) => ({
  authenticated: state.auth,
});

export default connect(mapStateToProps, { logoutUser })(
  withRouter(DefaultHeader)
);
