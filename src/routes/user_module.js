import React from "react";
const User = React.lazy(() => import("../modules/user_module/home"));
const ModuleBusinessModel = React.lazy(() => import("../modules/menu_module"));
const RoleManagementModule = React.lazy(() => import("../modules/role_module"));
const ChangePassword = React.lazy(() =>
  import("../modules/user_module/home/changePassword")
);
const ChangeUserPassword = React.lazy(() =>
  import("../modules/user_module/home/changeUserPassword")
);
const UserProfile = React.lazy(() =>
  import("../modules/user_module/home/userprofile")
);
const routes = [
  {
    path: "/users",
    exact: true,
    privateRoute: true,
    name: "Manage User",
    component: User,
  },
  {
    path: "/roles",
    exact: true,
    privateRoute: true,
    name: "Roles",
    component: RoleManagementModule,
  },
  {
    path: "/menu",
    exact: true,
    privateRoute: true,
    name: "Business Function Module",
    component: ModuleBusinessModel,
  },
  {
    path: "/changepassword",
    exact: true,
    privateRoute: true,
    name: "Change User Password",
    component: ChangePassword,
  },
  {
    path: "/userprofile",
    exact: true,
    privateRoute: true,
    name: "User Profile",
    component: UserProfile,
  },
  {
    path: "/changeuserpassword",
    exact: true,
    privateRoute: true,
    name: "Change Specific User Password",
    component: ChangeUserPassword,
  },
];

export default routes;
