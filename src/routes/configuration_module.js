import React from "react";
const emptype = React.lazy(() => import("../modules/configuration/empType"));
const Grade = React.lazy(() => import("../modules/configuration/grade"));
const Job = React.lazy(() => import("../modules/configuration/job"));
const maritalStatus = React.lazy(() => import("../modules/configuration/martialstatus"));
const position = React.lazy(() => import("../modules/configuration/position"));
const section = React.lazy(() => import("../modules/configuration/section"));
const department = React.lazy(() => import("../modules/configuration/department"));
const designation = React.lazy(() => import("../modules/configuration/designation"));
const language = React.lazy(() => import("../modules/configuration/language"));
const size = React.lazy(() => import("../modules/configuration/size"));
const station = React.lazy(() => import("../modules/configuration/station"));

const routes = [
  {
    path: "/emptype",
    exact: true,
    privateRoute: true,
    name: "Manage Employee Type",
    component: emptype,
  },
  {
    path: "/grade",
    exact: true,
    privateRoute: true,
    name: "Manage Grade",
    component: Grade,
  },
  {
    path: "/job",
    exact: true,
    privateRoute: true,
    name: "Manage Job",
    component: Job,
  },
  {
    path: "/maritalstatus",
    exact: true,
    privateRoute: true,
    name: "Manage Marital Status",
    component: maritalStatus,
  },
  {
    path: "/position",
    exact: true,
    privateRoute: true,
    name: "Manage Position",
    component: position,
  },
  {
    path: "/section",
    exact: true,
    privateRoute: true,
    name: "Manage Section",
    component: section,
  },
  {
    path: "/department",
    exact: true,
    privateRoute: true,
    name: "Manage department",
    component: department,
  },
  {
    path: "/designation",
    exact: true,
    privateRoute: true,
    name: "Manage designation",
    component: designation,
  },
  {
    path: "/language",
    exact: true,
    privateRoute: true,
    name: "Manage language",
    component: language,
  },
  {
    path: "/size",
    exact: true,
    privateRoute: true,
    name: "Manage size",
    component: size,
  },
  {
    path: "/station",
    exact: true,
    privateRoute: true,
    name: "Manage station",
    component: station,
  },
];

export default routes;
