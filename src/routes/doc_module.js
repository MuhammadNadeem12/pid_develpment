import React from "react";
const Document = React.lazy(() => import("../modules/document_module/index"));
const mediaRelCell=React.lazy(() => import("../modules/document_module/mediaRelCell"));
const pendingDoc=React.lazy(() => import("../modules/document_module/pending_Doc"));
const viewDocument = React.lazy(() => import("../modules/document_module/viewDocs"));
const ddAdv=React.lazy(() => import("../modules/document_module/ddAdv"));
const pio=React.lazy(() => import("../modules/document_module/pio"));
const editUploadFile = React.lazy(() => import("../modules/document_module/editUploadFiles"));
const routes = [
  {
    path: "/document",
    exact: true,
    privateRoute: true,
    name: "Manage Document",
    component: Document,
  },
  {
    path: "/mediarelcell",
    exact: true,
    privateRoute: true,
    name: "Manage Document",
    component: mediaRelCell,
  }, {
    path: "/pendingdoc",
    exact: true,
    privateRoute: true,
    name: "Manage Document",
    component: pendingDoc,
  }, {
    path: "/viewdoc/:id",
    exact: true,
    privateRoute: true,
    name: "Manage Document",
    component: viewDocument,
  },
  {
    path: "/ddadv",
    exact: true,
    privateRoute: true,
    name: "Manage Document",
    component: ddAdv,
  },
  {
    path: "/pio",
    exact: true,
    privateRoute: true,
    name: "Manage Document",
    component: pio,
  }, {
    path: "/edituploadfile/:id",
    exact: true,
    privateRoute: true,
    name: "Manage Document",
    component: editUploadFile,
  },
];

export default routes;
