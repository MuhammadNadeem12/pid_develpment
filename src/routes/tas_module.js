import React from "react";
const attendanceDashboard=React.lazy(() => import("../modules/TAS_module/attendanceDashboard"));

const employeeDetail=React.lazy(() => import("../modules/TAS_module/employeeDetail"));
const searchEmployee=React.lazy(() => import("../modules/TAS_module/searchEmployee"));


const routes = [
    {
        path: "/dashboard",
        exact: true,
        privateRoute: true,
        name: "Dashboard",
        component: attendanceDashboard,
      },
      {
        path: "/empdetail",
        exact: true,
        privateRoute: true,
        name: "Dashboard",
        component: employeeDetail,
      },
      {
        path: "/searchemp",
        exact: true,
        privateRoute: true,
        name: "Dashboard",
        component: searchEmployee,
      },
];

export default routes;
