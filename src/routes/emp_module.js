import React from "react";
const Employee = React.lazy(() => import("../modules/emp_module/index"));

const routes = [
  {
    path: "/createemployeee",
    exact: true,
    privateRoute: true,
    name: "Manage Employee",
    component: Employee,
  },
];

export default routes;
