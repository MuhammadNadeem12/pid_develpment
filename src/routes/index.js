import UserRoute from "./user_module";
import EmployeeRoute from "./emp_module";
import DocumentRoute from "./doc_module";
import TasRoute from "./tas_module";
import Dashboard from "../modules/dashboard";
import Configuration from "./configuration_module";


const staticRoutes = [
 
  {
    path: "/",
    exact: true,
    privateRoute: true,
    name: "Dashboard",
    component: Dashboard,
  },
];

export default [
  ...staticRoutes,
  ...UserRoute,
  ...EmployeeRoute,
  ...DocumentRoute,
  ...TasRoute,
  ...Configuration
];
