import validator from "validator";
import isEmpty from "../../../utils/isEmpty";
const validateLogin = (data) => {
  let errors = {};

  if (validator.isEmpty(data.userName)) {
    errors.userName = "User Name is Required *";
  }
  if (validator.isEmpty(data.password)) {
    errors.password = "Password is Required";
  } else if (!validator.isLength(data.password, { min: 5, max: 20 })) {
    errors.password = "Password must be Between 5 and 20 characters *";
  }

  const feedback = {
    haserror: !isEmpty(errors),
    errors,
  };
  return feedback;
};
export default validateLogin;
