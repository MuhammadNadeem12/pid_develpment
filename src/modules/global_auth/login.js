import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { loginUser } from "../../redux/global_auth/actions";
import LoadingOverlay from "react-loading-overlay";
import isEmpty from "../../utils/isEmpty";
import loginValidation from "./validations/index";
//import loginGIF from "../../../src/assets/loginGif.gif";
import "../../scss/_custom.scss";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      usertype: "",
      errors: {},
      alert: {},
      loading: false,
    };

    document.title = "Login :: PID";
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (e) => {
    e.preventDefault();

    const data = {
      userName: this.state.username.toString(),
      password: this.state.password.toString(),
    };
    this.setState({ loading: true });
    const { haserror, errors } = loginValidation(data);
    if (haserror) {
      this.setState({ errors });
      this.setState({ loading: false });
    } else {
      this.props.loginUser(data);
      this.setState({ errors: "" });
    }
  };

  componentDidMount() {
    if (this.props.authentication.isAuthenticated) {
      window.location.href = "/";
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.authentication.isAuthenticated) {
      window.location.href = "/";
    }

    if (nextProps && nextProps.errors) {
      if (nextProps.errors.errors && !isEmpty(nextProps.errors.errors)) {
        this.setState({ errors: nextProps.errors.errors, loading: false });
      }
    }
    if (nextProps && nextProps.alerts) {
      if (nextProps.alerts.alerts && !isEmpty(nextProps.alerts.alerts)) {
        this.setState({ alert: nextProps.alerts.alerts, loading: false });
      }
    }
  }

  showPassword = () => {
    var x = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="app flex-row align-items-center bg-login">
        {/* <video autoPlay muted loop id="myVideo">
          <source src={loginVedio} type="video/mp4" />
        </video> */}
        <Container className="content">
          <Row className="justify-content-center">
            <Col md="8">
              <LoadingOverlay
                styles={{
                  wrapper: {
                    width: "100%",
                    height: "100%",
                  },
                }}
                active={this.state.loading}
                spinner
                text="Please wait..."
              >
                <CardGroup>
                  <Card
                    className="text-white  py-5 d-md-down-none bg-white transparent"
                    style={{
                      width: "44%",
                    }}
                  >
                    <CardBody className="text-center login__side"></CardBody>
                  </Card>
                  <Card className="p-4 transparent">
                    <CardBody className="transparent">
                      <Form onSubmit={this.onSubmit}>
                        <h1 className="text-white">Login</h1>
                        <p className="text-white">Sign In to your account</p>
                        {this.state.alert && this.state.alert.msg && (
                          // <Alert color={this.state.alert.type}>
                          //   {this.state.alert.msg}
                          // </Alert>
                          <p className="error">{this.state.alert.msg}</p>
                        )}

                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            placeholder="Username"
                            autoComplete="username"
                            name="username"
                            value={this.state.username}
                            onChange={this.onChange}
                            style={{ width: "70%" }}
                          />
                          <p style={{ color: "white" }}>
                            {errors && errors.userName}
                          </p>
                        </InputGroup>
                        <InputGroup className="mb-4">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-lock"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            id="password"
                            type="password"
                            placeholder="Password"
                            autoComplete="current-password"
                            name="password"
                            value={this.state.password}
                            onChange={this.onChange}
                            style={{ width: "70%" }}
                          />
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i
                                onClick={this.showPassword}
                                className="icon-eye"
                              ></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <p style={{ color: "white" }}>
                            {errors && errors.password}
                          </p>
                        </InputGroup>

                        <Row>
                          <Col xs="6">
                            <Button type="submit" className="px-4 btn-white">
                              Login
                              {/* <i className="fa fa-sign-in">Login</i> */}
                            </Button>
                          </Col>
                        </Row>
                      </Form>
                    </CardBody>
                  </Card>
                </CardGroup>
              </LoadingOverlay>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  authentication: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  alerts: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  authentication: state.auth,
  errors: state.errors,
  alerts: state.alerts,
});

export default connect(mapStateToProps, { loginUser })(Login);
