import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
//import Loading from "../../../utils/spinner";
import FullScreen from "../../../utils/maximize-dialog";
//import { createProject } from "../../../redux/configration_module/actions";
//import projectValidation from "./validation/projectValidation";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

const initialState = {
  projectname: "",
  projectdescr: "",
  projectstatus: "",
  loading: true,
  errors: {},
};

class AddRegProjectModel extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }
  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {}
  formSubmit = (e) => {
    e.preventDefault();
    this.setState({ errors: {} });
    const data = {
      projectname: this.state.projectname.toUpperCase(),
      projectdescr: this.state.projectdescr.toUpperCase(),
      projectstatus: this.state.projectstatus.toUpperCase(),
    };
console.log(data);
    // const { haserror, errors } = projectValidation(data);
    // if (haserror) {
    //   this.setState({ errors });
    // } else {
    //   this.props.createProject(data);
    //   this.setState(initialState);
    //   this.props.modelOpen();
    // }
  };

  UNSAFE_componentWillReceiveProps(nextProps) {}
  render() {
    const { errors } = this.state;
    let content = "";
    // if (loading) {
    //   content = <Loading />;
    // } else {
    content = (
      <div>
        <Row>
          <Col md="4">
            <FormGroup>
              <Label>
                Name<span style={{ color: "red" }}> *</span>
              </Label>
              <Input
                type="text"
                value={this.state.projid}
                name="projectname"
                onChange={this.onChangeInput}
                placeholder="Enter  Name"
              />
              <p className="error">{errors.projectname}</p>
            </FormGroup>
          </Col>
        

          <Col md="4">
            <FormGroup>
              <Label>
                Decription<span style={{ color: "red" }}> *</span>
              </Label>
              <Input
                type="text"
                value={this.state.descr}
                name="projectdescr"
                onChange={this.onChangeInput}
                placeholder="Enter Decription"
              />
              {/* <Input
                type="select"
                value={this.state.projectstatus}
                name="projectstatus"
                onChange={this.onChangeInput}
              >
                <option value="">Please Select Status</option>
                <option value="I">Inactive</option>
                <option value="A">Active</option>
              </Input> */}
              <p className="error">{errors.projectstatus}</p>
            </FormGroup>
          </Col>
          <Col md="4">
            <FormGroup>
              <Label>
                Status<span style={{ color: "red" }}> *</span>
              </Label>
              
               <Input
                type="select"
                value={this.state.projectstatus}
                name="projectstatus"
                onChange={this.onChangeInput}
              >
                <option value="">Please Select Status</option>
                <option value="I">Inactive</option>
                <option value="A">Active</option>
              </Input> 
              <p className="error">{errors.projectstatus}</p>
            </FormGroup>
          </Col>
        </Row>
      </div>
    );
    // }

    return (
      <div>
        <Modal
          id="addRegProjectType"
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader>
            New Marital Status
            <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("addRegProjectType")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.props.modelOpen}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button type="submit" color="success">
                Save
              </Button>
              <Button color="secondary" onClick={this.props.modelOpen}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

AddRegProjectModel.propType = {
  modelOpen: PropTypes.func.isRequired,
 
  isOpen: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
  lov: state.lov, //STORE
});

export default connect(mapStateToProps, {
 
})(AddRegProjectModel);
