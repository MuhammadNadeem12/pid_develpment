import React, { Component } from "react";
import { Card, Badge, CardBody, CardHeader, Row, Col } from "reactstrap";
import { Button } from "primereact/button";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import AddPosition from "./addPosition";
import UpdatePosition from "./updatePosition";
//import PropTypes from "prop-types";
import { connect } from "react-redux";
//import { getRegProjects } from "../../../redux/configration_module/actions";
import getAPI from '../../../utils/getAPITemplate';
//import Loading from "../../../utils/spinner";
import isEmpty from "../../../utils/isEmpty";
class RegProject extends Component {
  constructor() {
    super();
    this.state = {
      modelOpen: false,
      updateOpen: false,
      loading: true,
      row: {},
      regProjects: [{}],
    };
    document.title = "Position :: PID";
  }
  componentDidMount() {
   this.getListvalues();
  }
  getListvalues=async()=>{
    let data = {
      url: "pid/getAllPositions",
      showMessage: "",
    };

    const response = await getAPI(data);
    if (response && !isEmpty(response)) {
      this.setState({regProjects:response})
    }
  }

  showModel = () => {
    this.setState({ modelOpen: !this.state.modelOpen });
  };
  updateModel = (item) => {
    this.setState({ updateOpen: !this.state.updateOpen });
    if (item) {
      this.setState({ row: item });
    }
  };
  actionTemplate = (rowData, column) => {
    return (
      <div style={{ textAlign: "center" }}>
        <Button
          type="button"
          icon="pi pi-pencil"
          className="p-button-warning"
          tooltip="Edit Position"
          onClick={this.updateModel.bind(this, rowData)}
          tooltipOptions={{ position: "left" }}
        ></Button>
      </div>
    );
  };
  statusTemplate = (rowData, column) => {
    return (
      <div>
        {rowData.status && rowData.status === "A" ? (
          <Badge color="info">Active</Badge>
        ) : (
          <Badge color="secondary">InActive</Badge>
        )}
      </div>
    );
  };
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.configration && nextProps.configration.regProjectsList) {
      this.setState({
        regProjects: nextProps.configration.regProjectsList,
        loading: nextProps.configration.loading,
      });
    }
  }

  render() {
    let content = "";
   
      content = (
        <DataTable
        className="table-bordered"
        responsive={true}
        value={this.state.regProjects}
        dataKey="positionId"
        style={{ textAlign: "center" }}
        paginator={true}
        rows={10}
        rowsPerPageOptions={[10, 20, 30]}
      >
        <Column
          style={{ width: "15%" }}
          filter={true}
          sortable={true}
          field="code"
          header="Code"
        />
         <Column
          style={{ width: "15%" }}
          filter={true}
          sortable={true}
          field="descr"
          header="Description"
        />
        

        <Column
          style={{ width: "16%" }}
          filter={true}
          sortable={true}
          body={this.statusTemplate}
          field="status"
          header="Status"
        />

        <Column
          style={{ width: "7.5%" }}
          body={this.actionTemplate}
          field="actions"
          header="Actions"
        />
      </DataTable>);
    
    return (
      <Row>
      <AddPosition
          modelOpen={this.showModel}
          isOpen={this.state.modelOpen}
        />
           <UpdatePosition
          defaultvalue={this.state.row}
          updatOpen={this.updateModel}
          updateisOpen={this.state.updateOpen}
        />  
        <Col>
          <Card>
            <CardHeader>
              <Button
                icon="pi pi-plus"
                onClick={this.showModel}
                className="float-right p-button-success"
                tooltip="Add Position"
                tooltipOptions={{ position: "left" }}
              ></Button>
              <h5>Position</h5>
            </CardHeader>
            <CardBody>{content}</CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

RegProject.propTypes = {
 
};

const mapStateToProps = (state) => ({
 
});

export default connect(mapStateToProps, {  })(RegProject);
