import React, { Component } from "react";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import PropTypes from "prop-types";
//import RoleValidation from "./validation/RoleValidation";
import { connect } from "react-redux";
import FullScreen from "../../../utils/maximize-dialog";
import LoadingOverlay from "react-loading-overlay";
import postAPI from '../../../utils/postAPITemplate'
import isEmpty from "../../../utils/isEmpty";
import validator from "validator";
const initialState = {
  descr: "",
  code: "",
  status: "",
  id: "",
  disablefield: false,
  loading:false,
  errors: {},
};
export class updateModelScreen extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }
  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  formSubmit = async (e) => {
    e.preventDefault();

    const data = {
      code: this.state.code.toUpperCase(),
      descr: this.state.descr.toUpperCase(),
      id: this.state.id,
      status: this.state.status.toUpperCase(),
      url:"pid/updateSection",
      showMessage:"S",
    };
    let errors={};
    if (validator.isEmpty(data.code)) {
      errors.code = "Code is required";
    }
    if (validator.isEmpty(data.descr)) {
      errors.descr = "Description is required";
    }
    if (validator.isEmpty(data.status)) {
      errors.status = "Status is required";
    }
    this.setState({loading:true});
   if (errors && !isEmpty(errors)) {
       this.setState({ errors});
     } else {
    const response= await postAPI(data); 
    //const response={type:"ABC"} ;
    if(response && !isEmpty(response)){
     ///console.log(response);
      this.setState(initialState);
      this.props.updatOpen();
    }
  }
    this.setState({loading:false});
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.defaultvalue) {
      if (nextProps.defaultvalue.sectionId) {
        this.setState({ id: nextProps.defaultvalue.sectionId });
      }
      if (nextProps.defaultvalue.descr) {
        this.setState({ descr: nextProps.defaultvalue.descr});
      }
      if (nextProps.defaultvalue.code) {
        this.setState({ code: nextProps.defaultvalue.code });
      }
      if (nextProps.defaultvalue.status) {
        this.setState({ status: nextProps.defaultvalue.status });
      }
    }
  }

  render() {
    const { errors} = this.state;
    let content = (
        <Row>
          <Col md="4">
            <FormGroup>
              <Label>Code</Label>
              <Input
                type="text"
                value={this.state.code}
                name="code"
                disabled={this.state.disablefield}
                onChange={this.onChangeInput}
                placeholder="Enter Code"
              />
              <p className="error">{errors.code}</p>
            </FormGroup>
          </Col>
          <Col md="4">
            <FormGroup>
              <Label>Description</Label>
              <Input
                type="text"
                value={this.state.descr}
                name="descr"
                onChange={this.onChangeInput}
                placeholder="Enter Description"
              />
              <p className="error">{errors.descr}</p>
            </FormGroup>
          </Col>
          <Col md="4">
            <FormGroup>
              <Label>Status</Label>
              <Input
                type="select"
                value={this.state.status}
                onChange={this.onChangeInput}
                name="status"
              >
                  <option value="">Select Status</option>
                <option value="I">IN-ACTIVE</option>
                <option value="A">ACTIVE</option>
              </Input>
              <p className="error">{errors.status}</p>
            </FormGroup>
          </Col>
        </Row>
      );
   
    return (
      <div>
        <Modal
          id="updateModelid"
          size="lg"
          isOpen={this.props.updateisOpen}
          toggle={this.props.updatOpen}
        >
          <ModalHeader>Edit Section
          <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("updateModelid")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.props.updatOpen}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <LoadingOverlay
                active={this.state.loading}
                spinner
                text="Please wait..."
              >
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>   
              <Button type="submit" color="success">
                Update
              </Button>
              <Button color="secondary" onClick={this.props.updatOpen}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
          </LoadingOverlay>
        </Modal>
      </div>
    );
  }
}
updateModelScreen.propTypes = {
  updatOpen: PropTypes.func.isRequired,
  updateisOpen: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
  lov: state.lov, //STORE
});

export default connect(mapStateToProps, {})(updateModelScreen
);
