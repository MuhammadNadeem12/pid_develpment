import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import FullScreen from "../../../utils/maximize-dialog";
import LoadingOverlay from "react-loading-overlay";
//import projectValidation from "./validation/projectValidation";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import postAPI from '../../../utils/postAPITemplate'
import { isElement } from "react-dom/test-utils";
import isEmpty from "../../../utils/isEmpty";
import validator from "validator";

const initialState = {
  code: "",
  descr: "",
  status: "",
  loading: false,
  errors: {},
};

class AddRegProjectModel extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }
  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {}
  formSubmit = async (e) => {
    e.preventDefault();
    this.setState({ errors: {} });
    const data = {
      code: this.state.code.toUpperCase(),
      descr: this.state.descr.toUpperCase(),
      status: this.state.status.toUpperCase(),
      url:'pid/saveStation',
      showMessage:'S',
    };
    let errors={};
    if (validator.isEmpty(data.code)) {
      errors.code = "Code is required";
    }
    if (validator.isEmpty(data.descr)) {
      errors.descr = "Description is required";
    }
    if (validator.isEmpty(data.status)) {
      errors.status = "Status is required";
    }
    this.setState({loading:true});
   if (errors && !isEmpty(errors)) {
       this.setState({ errors});
     } else {
    const response= await postAPI(data); 
    //const response={type:"ABC"} ;
    if(response && !isEmpty(response)){
     ///console.log(response);
      this.setState(initialState);
      this.props.modelOpen();
    }
  }
    this.setState({loading:false});
  };

  UNSAFE_componentWillReceiveProps(nextProps) {}
  render() {
    const { errors } = this.state;
    let content = (
      <div>
        <Row>
          <Col md="4">
            <FormGroup>
              <Label>
                Code<span style={{ color: "red" }}> *</span>
              </Label>
              <Input
                type="text"
                value={this.state.projid}
                name="code"
                onChange={this.onChangeInput}
                placeholder="Enter Code"
              />
              <p className="error">{errors.code}</p>
            </FormGroup>
          </Col>
        

          <Col md="4">
            <FormGroup>
              <Label>
                Decription<span style={{ color: "red" }}> *</span>
              </Label>
              <Input
                type="text"
                value={this.state.descr}
                name="descr"
                onChange={this.onChangeInput}
                placeholder="Enter Decription"
              />
              <p className="error">{errors.descr}</p>
            </FormGroup>
          </Col>
          <Col md="4">
            <FormGroup>
              <Label>
                Status<span style={{ color: "red" }}> *</span>
              </Label>
              
               <Input
                type="select"
                value={this.state.status}
                name="status"
                onChange={this.onChangeInput}
              >
                <option value="">Please Select Status</option>
                <option value="I">Inactive</option>
                <option value="A">Active</option>
              </Input> 
              <p className="error">{errors.status}</p>
            </FormGroup>
          </Col>
        </Row>
      </div>
    );
    

    return (
      <div>
        <Modal
          id="addRegProjectType"
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader>
            New Station
            <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("addRegProjectType")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.props.modelOpen}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <LoadingOverlay
                active={this.state.loading}
                spinner
                text="Please wait..."
              >
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button type="submit" color="success">
                Save
              </Button>
              <Button color="secondary" onClick={this.props.modelOpen}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
          </LoadingOverlay>
        </Modal>
      </div>
    );
  }
}

AddRegProjectModel.propType = {
  modelOpen: PropTypes.func.isRequired,
 
  isOpen: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
  lov: state.lov, //STORE
});

export default connect(mapStateToProps, {
 
})(AddRegProjectModel);
