import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Card, CardBody } from "reactstrap";


class Dashboard extends Component {
  constructor() {
    super();
    this.state = {
      user: {},
      usermessage: "",
      alert: {},
    };
    document.title = "Dashboard :: PID";
  }

  componentDidMount() {
    //    this.setState({ user: JSON.parse(localStorage.getItem("userData")) });
    //    this.showSuccess();
  }
  showSuccess() {
    this.growl.show({
      severity: "success",
      summary: "LOGIN SUCCESSFULL!",
      detail: "Welcome to EMO",
    });
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.auth && nextProps.auth.user) {
      this.setState({ user: nextProps.auth.user });
    }
    if (nextProps.alerts && nextProps.alerts.alerts) {
      this.setState({ alert: nextProps.alerts.alerts });
    }
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardBody>
            <h1>Welcome To Press Information Department(PID)</h1>
          </CardBody>
        </Card>
      </div>
    );
  }
}

Dashboard.propTypes = {
  alerts: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  alerts: state.alerts,
});

export default connect(mapStateToProps)(Dashboard);
