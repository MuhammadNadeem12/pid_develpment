import React, { Component } from "react";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import Select from "react-select";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getAllMenus } from "../../redux/menu_module/actions";
import { createRoleDetail } from "../../redux/role_module/actions";
import { getRoleLov, getMenuLov } from "../../redux/lov/actions";

import AddRoleDetailValidation from "./validation/AddRoleDetailValidation";
import FullScreen from "../../utils/maximize-dialog";
const initialState = {
  menuId: "",
  roleId: "",
  hide: "",
  select: "",
  insert: "",
  update: "",
  status: "",
  delete: "",
  errors: {},
  roleList: [],
  menuList: [],

  loading: true,
};
export class AddRoleDetail extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }
  componentDidMount() {
    this.props.getRoleLov();
    this.props.getMenuLov();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.lov && nextProps.lov.role_lov) {
      this.setState({
        roleList: nextProps.lov.role_lov.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    }
    if (nextProps && nextProps.lov && nextProps.lov.manu_lov) {
      this.setState({
        menuList: nextProps.lov.manu_lov.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    }
  }

  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  cancelDialog = () => {
    this.props.modelOpen();
    this.setState(initialState);
  };

  formSubmit = (e) => {
    e.preventDefault();
    const data = {
      menuId: this.state.menuId,
      roleId: this.state.roleId,
      hide: this.state.hide,
      select: this.state.select,
      insert: this.state.insert,
      update: this.state.update,
      status: this.state.status,
      delete: this.state.delete,
    };

    const { haserror, errors } = AddRoleDetailValidation(data);
    if (haserror) {
      this.setState({ errors });
    } else {
      this.props.createRoleDetail(data);
      this.setState(initialState);
      this.props.modelOpen();
    }
  };
  setRoleValue = (e) => {
    this.setState({ roleId: e.value });
  };
  setMenuValue = (e) => {
    this.setState({ menuId: e.value });
  };

  render() {
    const { errors } = this.state;
    let content = "";
    // if (loading) {
    //   content = <Loading />;
    // } else {
    content = (
      <Row>
        <Col md="6">
          <FormGroup>
            <Label>
              Role<span style={{ color: "red" }}> *</span>
            </Label>
            <Select
              onChange={this.setRoleValue}
              options={this.state.roleList}
            />
            <p className="error">{errors.roleId}</p>
          </FormGroup>
        </Col>
        <Col md="6">
          <FormGroup>
            <Label>
              Menu<span style={{ color: "red" }}> *</span>
            </Label>
            <Select
              onChange={this.setMenuValue}
              options={this.state.menuList}
            />
            <p className="error">{errors.menuId}</p>
          </FormGroup>
        </Col>
        <Col md="6">
          <Label>
            Hide<span style={{ color: "red" }}> *</span>
          </Label>
          <Input
            type="select"
            value={this.state.hide}
            name="hide"
            onChange={this.onChangeInput}
          >
            <option value="">Please Select Status</option>
            <option value="N">NO</option>
            <option value="Y">YES</option>
          </Input>
          <p className="error">{errors.hide}</p>
        </Col>
        <Col md="6">
          <Label>
            Select<span style={{ color: "red" }}> *</span>
          </Label>
          <Input
            type="select"
            value={this.state.select}
            name="select"
            onChange={this.onChangeInput}
          >
            <option value="">Please Select Status</option>
            <option value="N">NO</option>
            <option value="Y">YES</option>
          </Input>
          <p className="error">{errors.select}</p>
        </Col>
        <Col md="6">
          <Label>
            Insert<span style={{ color: "red" }}> *</span>
          </Label>
          <Input
            type="select"
            value={this.state.insert}
            name="insert"
            onChange={this.onChangeInput}
          >
            <option value="">Please Select Status</option>
            <option value="N">NO</option>
            <option value="Y">YES</option>
          </Input>
          <p className="error">{errors.insert}</p>
        </Col>

        <Col md="6">
          <Label>
            Update<span style={{ color: "red" }}> *</span>
          </Label>
          <Input
            type="select"
            value={this.state.update}
            name="update"
            onChange={this.onChangeInput}
          >
            <option value="">Please Select Status</option>
            <option value="N">NO</option>
            <option value="Y">YES</option>
          </Input>
          <p className="error">{errors.update}</p>
        </Col>
        <Col md="6">
          <Label>
            Delete<span style={{ color: "red" }}> *</span>
          </Label>
          <Input
            type="select"
            value={this.state.delete}
            name="delete"
            onChange={this.onChangeInput}
          >
            <option value="">Please Select delete</option>
            <option value="N">NO</option>
            <option value="Y">YES</option>
          </Input>
          <p className="error">{errors.delete}</p>
        </Col>
        <Col md="6">
          <Label>
            Status<span style={{ color: "red" }}> *</span>
          </Label>
          <Input
            type="select"
            value={this.state.status}
            name="status"
            onChange={this.onChangeInput}
          >
            <option value="">Please Select Status</option>
            <option value="I">Inactive</option>
            <option value="A">Active</option>
          </Input>
          <p className="error">{errors.status}</p>
        </Col>
      </Row>
    );
    // }
    return (
      <div>
        <Modal
          id="viewroledetail"
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader>
            Add Role Detail
            <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("viewroledetail")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.cancelDialog}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button type="submit" color="success">
                Save
              </Button>
              <Button color="secondary" onClick={this.cancelDialog}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

AddRoleDetail.propTypes = {
  getRoleLov: PropTypes.func.isRequired, //STORE ;
  getMenuLov: PropTypes.func.isRequired, //STORE ;
};
const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
  lov: state.lov, //STORE
});

export default connect(mapStateToProps, {
  getAllMenus,
  createRoleDetail,
  getRoleLov,
  getMenuLov,
})(withRouter(AddRoleDetail));
