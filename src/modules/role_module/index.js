import React, { Component } from "react";
import { DataTable } from "primereact/datatable";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Column } from "primereact/column";
import { getRoleLov } from "../../redux/lov/actions";
import { Button } from "primereact/button";
import { Card, Badge, CardBody, Row, Col, CardHeader } from "reactstrap";
import { getAllRoles, getAllRoleRights } from "../../redux/role_module/actions";
import LoadingOverlay from "react-loading-overlay";
import AddRole from "../role_module/addRole";
import UpdateRoleRight from "./updateRoleRights";
import UpdateRole from "./updateRole";
import AddRoleRight from "./addRoleRights";
//import roleRight from "../../utils/checkRights";
//import isEmpty from "../../utils/isEmpty";
import { Accordion, AccordionTab } from "primereact/accordion";

export class RoleManageModule extends Component {
  constructor() {
    super();
    this.state = {
      roles: [],
      rolerightlist: [],
      expandedRows: null,
      updateModel: false,
      modelOpen: false,
      rowRole: {},
      rowRoleRight: {},
      alert: {},
      UpdateRoleRightModel: false,
      updateRoleModel: false,
      loading: false,
      addRoleRightModel: false,
    };
    document.title = "Manage Roles :: DFS";
  }

  showModel = () => {
    this.setState({ modelOpen: !this.state.modelOpen });
  };
  showUpdateRoleRightModel = (item) => {
    this.setState({
      UpdateRoleRightModel: !this.state.UpdateRoleRightModel,
      rowRoleRight: item,
    });
  };
  updateModel = (item) => {
    this.setState(() => {
      return {
        updateRoleModel: !this.state.updateRoleModel,
        rowRole: item,
      };
    });
  };
  RoleDetailModel = (item) => {
    this.props.getRoleLov();
    this.setState(() => {
      return {
        addRoleRightModel: !this.state.addRoleRightModel,
        rowRole: item,
      };
    });
  };
  statusTemplate = (rowData, column) => {
    return (
      <div>
        {rowData.status && rowData.status === "A" ? (
          <Badge color="info">Active</Badge>
        ) : (
          <Badge color="secondary">InActive</Badge>
        )}
      </div>
    );
  };

  minsertTemplate = (rowData, column) => {
    return (
      <div>
        {rowData.minsert && rowData.minsert === "Y" ? (
          <Badge color="info">YES</Badge>
        ) : (
          <Badge color="secondary">NO</Badge>
        )}
      </div>
    );
  };
  mdeleteTemplate = (rowData, column) => {
    return (
      <div>
        {rowData.mdelete && rowData.mdelete === "Y" ? (
          <Badge color="info">YES</Badge>
        ) : (
          <Badge color="secondary">NO</Badge>
        )}
      </div>
    );
  };
  mselectTemplate = (rowData, column) => {
    return (
      <div>
        {rowData.mselect && rowData.mselect === "Y" ? (
          <Badge color="info">YES</Badge>
        ) : (
          <Badge color="secondary">NO</Badge>
        )}
      </div>
    );
  };
  mupdateemplate = (rowData, column) => {
    return (
      <div>
        {rowData.mupdate && rowData.mupdate === "Y" ? (
          <Badge color="info">YES</Badge>
        ) : (
          <Badge color="secondary">NO</Badge>
        )}
      </div>
    );
  };
  actionTemplate = (rowData, column) => {
    return (
      <div style={{ textAlign: "center" }}>
        {/* <Button
          type="button"
          icon="pi pi-eye"
          tooltip="View Role Detail"
          tooltipOptions={{ position: "left" }}
          className="p-button-info"
          style={{ marginRight: "10px" }}
          onClick={this.showUpdateRoleRightModel.bind(this, rowData)}
        ></Button>
        <Button
          type="button"
          icon="pi pi-plus"
          tooltip="New Role Detail"
          tooltipOptions={{ position: "left" }}
          className=" p-button-info p-button-success"
          style={{ marginRight: "10px" }}
          onClick={this.RoleDetailModel.bind(this, rowData)}
        ></Button> */}

        <Button
          type="button"
          icon="pi pi-pencil"
          className="p-button-warning"
          onClick={this.updateModel.bind(this, rowData)}
        ></Button>
      </div>
    );
  };

  actionTemplateRoleRight = (rowData, column) => {
    return (
      <div style={{ textAlign: "center" }}>
        <Button
          type="button"
          icon="pi pi-pencil"
          className="p-button-warning"
          onClick={this.showUpdateRoleRightModel.bind(this, rowData)}
        ></Button>
      </div>
    );
  };

  componentDidMount = async () => {
    this.props.getAllRoles();
    this.props.getAllRoleRights();
  };
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.all_roles && nextProps.all_roles.roles) {
      this.setState({
        roles: nextProps.all_roles.roles,
        loading: nextProps.all_roles.loading,
      });
    }
    if (nextProps.all_roles && nextProps.all_roles.roleright) {
      this.setState({
        rolerightlist: nextProps.all_roles.roleright,
        loading: nextProps.all_roles.loading,
      });
    }
    if (nextProps.alerts && nextProps.alerts.alerts) {
      this.setState({ alert: nextProps.alerts.alerts });
    }
  }

  render() {
    let content = "",
      rightContent = "";

    content = (
      <div>
        <DataTable
          className="table-bordered"
          responsive={true}
          value={this.state.roles}
          style={{ textAlign: "center" }}
        >
          <Column
            filter={true}
            filterMatchMode="contains"
            sortable={true}
            field="roleCode"
            header="Role Code"
          />
          <Column
            filter={true}
            filterMatchMode="contains"
            sortable={true}
            field="roleDescr"
            header="Role Description"
          />
          <Column
            filter={true}
            filterMatchMode="contains"
            sortable={true}
            body={this.statusTemplate}
            field="status"
            header="Status"
          />
          <Column
            body={this.actionTemplate}
            field="actions"
            header="Actions"
            style={{ width: "150px" }}
          />
        </DataTable>
      </div>
    );
    rightContent = (
      <div>
        <DataTable
          className="table-bordered"
          responsive={true}
          value={this.state.rolerightlist}
          style={{ textAlign: "center" }}
          paginator={true}
          rows={10}
          rowsPerPageOptions={[10, 20, 30]}
          dataKey="rolerightid"
        >
          <Column
            filter={true}
            filterMatchMode="contains"
            sortable={true}
            field="tblRole.roleDescr"
            header="Role"
          />
          <Column
            filter={true}
            filterMatchMode="contains"
            sortable={true}
            field="tblMenu.menuDescr"
            header="Menu"
          />
          <Column
            filter={true}
            filterMatchMode="contains"
            sortable={true}
            body={this.minsertTemplate}
            field="minsert"
            header="Insert"
          />
          <Column
            filter={true}
            filterMatchMode="contains"
            sortable={true}
            body={this.mdeleteTemplate}
            field="mdelete"
            header="Delete"
          />
          <Column
            filter={true}
            filterMatchMode="contains"
            sortable={true}
            body={this.mselectTemplate}
            field="mselect"
            header="Select"
          />
          <Column
            filter={true}
            filterMatchMode="contains"
            sortable={true}
            body={this.mupdateemplate}
            field="mupdate"
            header="Update"
          />

          <Column
            body={this.actionTemplateRoleRight}
            field="actions"
            header="Actions"
            style={{ width: "150px" }}
          />
        </DataTable>
      </div>
    );

    return (
      <Row>
        <AddRole modelOpen={this.showModel} isOpen={this.state.modelOpen} />
        <UpdateRoleRight
          defaultvalue={this.state.rowRoleRight}
          modelOpen={this.showUpdateRoleRightModel}
          isOpen={this.state.UpdateRoleRightModel}
        />
        <UpdateRole
          defaultvalue={this.state.rowRole}
          modelOpen={this.updateModel}
          isOpen={this.state.updateRoleModel}
        />
        <AddRoleRight
          modelOpen={this.RoleDetailModel}
          isOpen={this.state.addRoleRightModel}
        />
        <Col>
          <Card>
            <CardHeader>
              <h5>Manage Roles & Role-Rights</h5>
            </CardHeader>
            <CardBody>
              {/* <div className="content-section introduction">
                <div className="feature-intro">
                  <h1>DataTable - Row Expansion</h1>
                  <p>
                    A row can be expanded to display extra content by enabling
                    expandableRows property and providing a row ng-template.
                  </p>
                </div>
              </div> */}
              <LoadingOverlay
                active={this.state.loading}
                spinner
                text="Please wait..."
              >
                <Accordion
                  activeIndex={this.state.activeIndex}
                  onTabChange={(e) => this.setState({ activeIndex: e.index })}
                >
                  <AccordionTab header="ROLES">
                    <Row>
                      <Col>
                        <Button
                          icon="pi pi-plus"
                          label="New Role"
                          tooltip="New Role"
                          onClick={this.showModel}
                          className=" float-right m-1 p-button-success"
                        ></Button>
                      </Col>
                    </Row>

                    {content}
                  </AccordionTab>
                  <AccordionTab header="ROLE RIGHTS">
                    <Row>
                      <Col>
                        <Button
                          icon="pi pi-plus"
                          label="Role Rights"
                          tooltip="New Role"
                          onClick={this.RoleDetailModel}
                          className=" float-right m-1 p-button-success"
                        ></Button>
                      </Col>
                    </Row>

                    {rightContent}
                  </AccordionTab>
                </Accordion>
              </LoadingOverlay>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

RoleManageModule.propTypes = {
  getAllRoles: PropTypes.func.isRequired,
  getAllRoleRights: PropTypes.func.isRequired,
  getRoleLov: PropTypes.func.isRequired,
  all_roles: PropTypes.object.isRequired,
  alerts: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  all_roles: state.role_module,
  alerts: state.alerts,
});

export default connect(mapStateToProps, {
  getAllRoles,
  getRoleLov,
  getAllRoleRights,
})(RoleManageModule);
