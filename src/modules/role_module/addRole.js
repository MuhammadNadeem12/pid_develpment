import React, { Component } from "react";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import RoleValidation from "./validation/RoleValidation";
import Loading from "../../utils/spinner";
import { createRole } from "../../redux/role_module/actions";
import FullScreen from "../../utils/maximize-dialog";
const initialState = {
  roleDescr: "",
  roleCode: "",
  status: "",
  errors: {},
};

class AddRole extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  cancelDialog = () => {
    this.props.modelOpen();
    this.setState(initialState);
  };

  formSubmit = (e) => {
    e.preventDefault();

    const data = {
      roleCode: this.state.roleCode.toUpperCase(),
      roleDescr: this.state.roleDescr.toUpperCase(),
      status: this.state.status.toUpperCase(),
    };
    const { haserror, errors } = RoleValidation(data);
    if (haserror) {
      this.setState({ errors });
    } else {
      this.props.createRole(data);
      this.setState(initialState);
      this.props.modelOpen();
    }
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps) this.setState(initialState);
  }
  render() {
    const { errors, loading } = this.state;
    let content = "";
    if (loading) {
      content = <Loading />;
    } else {
      content = (
        <Row>
          <Col md="4">
            <FormGroup>
              <Label>
                Role Code<span style={{ color: "red" }}> *</span>
              </Label>
              <Input
                type="text"
                value={this.state.roleCode}
                name="roleCode"
                onChange={this.onChangeInput}
                placeholder="Enter Role Code"
              />
              <p className="error">{errors.roleCode}</p>
            </FormGroup>
          </Col>
          <Col md="4">
            <FormGroup>
              <Label>
                Role Description<span style={{ color: "red" }}> *</span>
              </Label>
              <Input
                type="text"
                value={this.state.roleDescr}
                name="roleDescr"
                onChange={this.onChangeInput}
                placeholder="Enter Role Description"
              />
              <p className="error">{errors.roleDescr}</p>
            </FormGroup>
          </Col>

          <Col md="4">
            <FormGroup>
              <Label>
                Status<span style={{ color: "red" }}> *</span>
              </Label>
              <Input
                type="select"
                value={this.state.status}
                name="status"
                onChange={this.onChangeInput}
              >
                  <option value="">Select Status</option>
                <option value="I">IN-ACTIVE</option>
                <option value="A">ACTIVE</option>
              </Input>
              <p className="error">{errors.status}</p>
            </FormGroup>
          </Col>
        </Row>
      );
    }

    return (
      <div>
        <Modal
          id="addrolemodel"
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader>
            New Role{" "}
            <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("addrolemodel")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.cancelDialog}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button type="submit" color="success">
                Save
              </Button>
              <Button color="secondary" onClick={this.cancelDialog}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

AddRole.propTypes = {
  modelOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  createRole: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
});

export default connect(mapStateToProps, { createRole })(AddRole);
