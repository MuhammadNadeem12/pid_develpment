import validator from "validator";
import isEmptyCheck from "../../../utils/isEmpty";

const roleValidation = (data) => {
  let errors = {};
  const roleCode = isEmptyCheck(data.roleCode)
    ? ""
    : data.roleCode.trim().toString();
  const roleDescr = isEmptyCheck(data.roleDescr)
    ? ""
    : data.roleDescr.toString();
  const status = isEmptyCheck(data.status) ? "" : data.status.toString();

  if (validator.isEmpty(roleCode)) {
    errors.roleCode = "Role code is Required";
  }
  if (validator.isEmpty(roleDescr)) {
    errors.roleDescr = "Role Description is Required";
  }
  if (validator.isEmpty(status)) {
    errors.status = "Status is Required";
  }

  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors,
  };
  return feedback;
};
export default roleValidation;
