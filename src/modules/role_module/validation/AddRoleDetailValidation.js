import validator from "validator";
import isEmptyCheck from "../../../utils/isEmpty";

const AddRoleDetailValidation = (data) => {
  let errors = {};
  const menuId = isEmptyCheck(data.menuId) ? "" : data.menuId.toString();
  const roleId = isEmptyCheck(data.roleId) ? "" : data.roleId.toString();
  const hide = isEmptyCheck(data.hide) ? "" : data.hide.trim().toString();
  const select = isEmptyCheck(data.select) ? "" : data.select.trim().toString();
  const insert = isEmptyCheck(data.insert) ? "" : data.insert.trim().toString();
  const update = isEmptyCheck(data.update) ? "" : data.update.trim().toString();
  const status = isEmptyCheck(data.status) ? "" : data.status.trim().toString();

  if (validator.isEmpty(menuId)) {
    errors.menuId = "Module is Required";
  }
  if (validator.isEmpty(roleId)) {
    errors.roleId = "Role is Required";
  }
  if (validator.isEmpty(hide)) {
    errors.hide = "Hide is Required";
  }
  if (validator.isEmpty(insert)) {
    errors.insert = "Insert is Required";
  }
  if (validator.isEmpty(update)) {
    errors.update = "Update is Required";
  }
  if (validator.isEmpty(select)) {
    errors.select = "Select is Required";
  }
  if (validator.isEmpty(status)) {
    errors.status = "Status is Required";
  }

  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors,
  };
  return feedback;
};
export default AddRoleDetailValidation;
