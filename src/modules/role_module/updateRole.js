import React, { Component } from "react";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import PropTypes from "prop-types";
import RoleValidation from "./validation/RoleValidation";
import { updateRole } from "../../redux/role_module/actions";
import { connect } from "react-redux";
import Loading from "../../utils/spinner";

const initialState = {
  roleDescr: "",
  roleCode: "",
  status: "",
  roleId: "",
  disablefield: true,
  errors: {},
};
export class UpdateRole extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }
  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  formSubmit = (e) => {
    e.preventDefault();

    const data = {
      roleCode: this.state.roleCode.toUpperCase(),
      roleDescr: this.state.roleDescr.toUpperCase(),
      roleId: this.state.roleId,
      status: this.state.status.toUpperCase(),
    };

    const { haserror, errors } = RoleValidation(data);

    if (haserror) {
      this.setState({ errors });
    } else {
      this.props.updateRole(data, this.props.history);
      this.setState(initialState);
      this.props.modelOpen();
    }
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.defaultvalue) {
      if (nextProps.defaultvalue.roleId) {
        this.setState({ roleId: nextProps.defaultvalue.roleId });
      }
      if (nextProps.defaultvalue.roleDescr) {
        this.setState({ roleDescr: nextProps.defaultvalue.roleDescr });
      }
      if (nextProps.defaultvalue.roleCode) {
        this.setState({ roleCode: nextProps.defaultvalue.roleCode });
      }
      if (nextProps.defaultvalue.status) {
        this.setState({ status: nextProps.defaultvalue.status });
      }
    }
  }

  render() {
    const { errors, loading } = this.state;
    let content = "";
    if (loading) {
      content = <Loading />;
    } else {
      content = (
        <Row>
          <Col md="4">
            <FormGroup>
              <Label>Role Code</Label>
              <Input
                type="text"
                value={this.state.roleCode}
                name="roleCode"
                disabled={this.state.disablefield}
                onChange={this.onChangeInput}
                placeholder="Enter Module Code"
              />
              <p className="error">{errors.roleCode}</p>
            </FormGroup>
          </Col>
          <Col md="4">
            <FormGroup>
              <Label>Role Description</Label>
              <Input
                type="text"
                value={this.state.roleDescr}
                name="roleDescr"
                onChange={this.onChangeInput}
                placeholder="Enter Description"
              />
              <p className="error">{errors.roleDescr}</p>
            </FormGroup>
          </Col>
          <Col md="4">
            <FormGroup>
              <Label>Status</Label>
              <Input
                type="select"
                value={this.state.status}
                onChange={this.onChangeInput}
                name="status"
              >
                  <option value="">Select Status</option>
                <option value="I">IN-ACTIVE</option>
                <option value="A">ACTIVE</option>
              </Input>
              <p className="error">{errors.status}</p>
            </FormGroup>
          </Col>
        </Row>
      );
    }
    return (
      <div>
        <Modal
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader toggle={this.props.modelOpen}>Edit Role </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button type="submit" color="success">
                Update
              </Button>
              <Button color="secondary" onClick={this.props.modelOpen}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}
UpdateRole.propTypes = {
  modelOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
  lov: state.lov, //STORE
});

export default connect(mapStateToProps, {
  updateRole,
})(UpdateRole);
