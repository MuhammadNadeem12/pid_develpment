import React, { Component } from "react";
import { connect } from "react-redux";
import validator from "validator";

import {
  Card,
  CardBody,
  Row,
  Col,
  CardHeader,
  //Input,
  FormGroup,
  Label,
} from "reactstrap";
import { DataTable } from "primereact/datatable";
import { Button } from "primereact/button";
import { Column } from "primereact/column";
//import EditDetail from "./drEditDetail";
//import roleRight from "../../utils/checkRights";
import getAPI from "../../utils/getAPITemplate";
import postApi from "../../utils/postAPITemplate";
import LoadingOverlay from "react-loading-overlay";
import isEmpty from "../../utils/isEmpty";
//import config from "../../config";
//import { Calendar } from "primereact/calendar";
import moment from "moment";
import Select from "react-select";
const initialState = {
 
  TableRecords: [],
  getRecord: {},
  expandedRows: null,
  updateModel: false,
  modelOpen: false,
  globalFilter: null,
  row: {},
  empTypeList: [],
  wingList: [],
  secList: [],
  jobList: [],
  roleList: [],
  gradeList: [],
  posList: [],
  loading: false,
  errors: {},
  showMessage: "S",
};

class DREditSearch extends Component {
  constructor() {
    super();
    this.state = initialState;
    document.title = "Employee Detail::PID";
  }
  showModel = (item) => {
    this.setState({ modelOpen: !this.state.modelOpen, getRecord: item });
  };
  setEmpTypeValue = (e) => {
    this.setState({ empTypeId: e.value });
  };
  setmaritalValue = (e) => {
    this.setState({ maritalStatus: e.value });
  };
  setpostionValue = (e) => {
    this.setState({ lkpPositionId: e.value });
  };
  setDepartmentvalue = (e) => {
    this.setState({ departmentId: e.value });
  };
  setWingValue = async (e) => {
    this.setState({ lkpWingId: e.value });
  };
  setgradeValue = (e) => {
    this.setState({ lkpGradeId: e.value });
  };
  setsectionValue = (e) => {
    this.setState({ lkpSection: e.value });
  };
  setJobValue = async (e) => {
    this.setState({ lkpJobId: e.value });};
  componentDidMount = async () => {
    let emptypedata = {
      url: "pid/lovEmpType",
      showMessage: "",
    };
    const empResponse = await getAPI(emptypedata);
    if (empResponse && !isEmpty(empResponse)) {
      this.setState({
        empTypeList: empResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        empTypeList: [],
      });
    }

    let wingdata = {
      url: "pid/lovWing",
      showMessage: "",
    };
    const wingResponse = await getAPI(wingdata);
    if (wingResponse && !isEmpty(wingResponse)) {
      this.setState({
        wingList: wingResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        wingList: [],
      });
    }

    let jobdata = {
      url: "pid/lovJob",
      showMessage: "",
    };
    const jobResponse = await getAPI(jobdata);
    if (jobResponse && !isEmpty(jobResponse)) {
      this.setState({
        jobList: jobResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        jobList: [],
      });
    }

    let gradedata = {
      url: "pid/lovGrade",
      showMessage: "",
    };
    const gradeResponse = await getAPI(gradedata);
    if (gradeResponse && !isEmpty(gradeResponse)) {
      this.setState({
        gradeList: gradeResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        gradeList: [],
      });
    }
    let sectiondata = {
      url: "pid/lovSection",
      showMessage: "",
    };
    const secResponse = await getAPI(sectiondata);
    if (secResponse && !isEmpty(secResponse)) {
      this.setState({
        secList: secResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        secList: [],
      });
    }
    let positiondata = {
      url: "pid/lovPosition",
      showMessage: "",
    };
    const positionResponse = await getAPI(positiondata);
    if (positionResponse && !isEmpty(positionResponse)) {
      this.setState({
        posList: positionResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        posList: [],
      });
    }
    this.getDepartmentList();
  };

  UNSAFE_componentWillReceiveProps(nextProps) {}

  onChangeInput = (e) => {
    if (e.target && !isEmpty(e.target) && validator.isNumeric(e.target.value)) {
      this.setState({ [e.target.name]: e.target.value });
    } else if (e.target.value.length === 0) {
      this.setState({ [e.target.name]: "" });
    }
  };
  actionTemplate = (rowData, column) => {
    return (
      <div style={{ textAlign: "center" }}>
        <Button
          type="button"
          icon="pi pi-pencil"
          className="p-button-warning"
          tooltip="Employee Detail"
          tooltipOptions={{ position: "left" }}
          onClick={this.showModel.bind(this, rowData)}
        ></Button>
      </div>
    );
  };

  updateDetails = (id) => {};

  SearchTransaction = async (e) => {
    let errors = {};
    const data = {
      wingId: this.state.wingId,
      sectionId: this.state.sectionId,
      gradeId: this.state.gradeId,
      staff: this.state.staff,
      empId: this.state.empId,
      fdate: moment(this.state.fdate).format("YYYY-MM-DD HH:mm:ss"),
      tDate: moment(this.state.tDate).format("YYYY-MM-DD HH:mm:ss"),
      url: "pid/tasEmployessData",
    };

    // if (validator.isEmpty(data.fdate)) {
    //   errors.fdate = "From Date is Required";
    // }
    // if (validator.isEmpty(data.tdate)) {
    //   errors.tdate = "To Date is Required";
    // }
    
    // if (data.transRefNum != "") {
    this.setState({
      loading: true,
      empList: [],
      errors:errors,
    });
    const response = await postApi(data);
    //console.log(response);
    if (response && !isEmpty(response)) {
      this.setState({
        empList: response.map((item) => {
          return {
            name:
              item.tblEmployee.firstname +
              " " +
              (isEmpty(item.tblEmployee.middlename)
                ? ""
                : item.tblEmployee.middlename) +
              " " +
              (isEmpty(item.tblEmployee.lastname)
                ? ""
                : item.tblEmployee.lastname),
            department: item.tblEmployee.lkpDepartment.descr,
            section: item.tblEmployee.lkpSection.descr,
            wing: item.tblEmployee.lkpDesignation.descr,
            staff: item.tblEmployee.staffOfficer,
            gender: item.tblEmployee.gender,
            timein: moment(item.timeIn).format('HH:mm'),
          };
        }),
      });
      this.setState({errors});
    }
    this.setState({ loading: false });
  };

  getDepartmentList = async()=>{
    let sectiondata = {
      url: "pid/lovDepartment",
      showMessage: "",
    };
    const secResponse = await getAPI(sectiondata);
    if (secResponse && !isEmpty(secResponse)) {
      this.setState({
        departmentList: secResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
  }
}
  render() {
    let { errors } = this.state;
    const customStyles = {
        control: (base) => ({
          ...base,
          height: 30,
          minHeight: 30,
        }),
      };
    let content = "";
    if (this.state.TableRecords && !isEmpty(this.state.TableRecords)) {
      content = (
        <div>
          <DataTable
            className="table-bordered"
            responsive={true}
            emptyMessage="No records found"
            value={this.state.TableRecords}
            dataKey="transHeadId"
            style={{ textAlign: "center" }}
            paginator={true}
            rows={10}
            rowsPerPageOptions={[10, 20, 30]}
          >
            <Column
              field="transRefnum"
              header="Employee Name"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="senderCnic"
              header="Time In"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            <Column
              field="senderMobileno"
              header="Time Out"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="receiverCnic"
              header="Late"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="receiverMobileno"
              header="Absent"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
             <Column
              field="leave"
              header="Leave"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            {/* <Column
              body={this.actionTemplate}
              field="actions"
              header="Actions"
            /> */}
          </DataTable>
        </div>
      );
    }

    return (
      <Row>
        {/* <EditDetail
          modelOpen={this.showModel}
          isOpen={this.state.modelOpen}
          record={this.state.getRecord}
        /> */}
        <Col>
          <Card>
            <CardHeader>
              <h5>Employee Time Attendance</h5>
            </CardHeader>
            <CardBody>
              <LoadingOverlay
                active={this.state.loading}
                spinner
                text="Please wait..."
              >
                <Row>
                  <Col md="3">
                  <FormGroup>
                <Label>Wing</Label>
               
                <Select
                  onChange={this.setWingValue}
                  options={this.state.wingList}
                />
                <p className="error">{errors.lkpWingId}</p>
              </FormGroup>
                    </Col>
                    <Col md="3">
              <FormGroup>
                <Label>Cell/Section</Label>
                <Select
                  onChange={this.setsectionValue}
                  options={this.state.secList}
                />
                <p className="error">{errors.lkpSection}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Grade</Label>
                <Select
                  onChange={this.setgradeValue}
                  options={this.state.gradeList}
                />
                <p className="error">{errors.lkpGradeId}</p>
              </FormGroup>
            </Col>
                    {/* <Col md="3">
                    <FormGroup>
                      <Label>Employee #</Label>
                      <Input
                        minLength="9"
                        maxLength="9"
                        type="text"
                        value={this.state.transRefNum}
                        name="transRefNum"
                        onChange={this.onChangeInput}
                        placeholder="Enter Transaction ID "
                      />
                      <p className="error">{errors.transRefNum}</p>
                    </FormGroup>
                  </Col></Row> */}
                  </Row>
                  <Row>
                  <Col md="3">
              <FormGroup>
                <Label>Job</Label>
                <Select
                  onChange={this.setJobValue}
                  options={this.state.jobList}
                />
                <p className="error">{errors.lkpJobId}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Position</Label>
                <Select
                  onChange={this.setpostionValue}
                  options={this.state.posList}
                />
                <p className="error">{errors.lkpPositionId}</p>
              </FormGroup>
            </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>
                         Employee Type
                        </Label>
                        <Select
                          onChange={this.setEmpTypeValue}
                          options={this.state.empTypeList}
               
                        />

                        <p className="error">{errors.relCircleId}</p>
                      </FormGroup>
                    </Col>
                    {/* <Col md="3">
                      <FormGroup>
                        <Label>
                          From Date<span style={{ color: "red" }}> *</span>
                        </Label>
                        <br />
                        <Calendar
                          style={{
                            fontSize: "14px",
                            width: "20px",
                          }}
                          onFocus={() => {
                            setTimeout(() => {
                              const element = document.getElementsByClassName(
                                "p-datepicker"
                              );
                              element[0].style.top = null;
                            }, 200);
                          }}
                          hideOnDateTimeSelect={true}
                          value={this.state.fromDate}
                          onChange={(e) => this.setState({ fromDate: e.value })}
                          showTime={true}
                          maxDate={this.state.toDate}
                          showSeconds={true}
                        />
                        <p className="error">{errors.fromDate}</p>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>
                          To Date<span style={{ color: "red" }}> *</span>
                        </Label>
                        <br />
                        <Calendar
                          onFocus={() => {
                            setTimeout(() => {
                              const element = document.getElementsByClassName(
                                "p-datepicker"
                              );
                              element[1].style.top = null;
                            }, 200);
                          }}
                          hideOnDateTimeSelect={true}
                          value={this.state.toDate}
                          minDate={this.state.fromDate}
                          onChange={(e) => this.setState({ toDate: e.value })}
                          showTime={true}
                          showSeconds={true}
                        />
                        <p className="error">{errors.toDate}</p>
                      </FormGroup>
                    </Col>
                 */}

                  <Col md="3" style={{ marginTop: "28px" }}>
                    <Button
                      type="submit"
                      color="primary"
                      label="Search"
                      onClick={this.SearchTransaction}
                    />
                  </Col>
                </Row>

                <div className="content-section implementation">{content}</div>
              </LoadingOverlay>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

DREditSearch.propTypes = {};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {})(DREditSearch);
