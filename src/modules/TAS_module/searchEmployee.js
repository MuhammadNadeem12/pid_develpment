import React, { Component } from "react";
import { connect } from "react-redux";
import validator from "validator";
import moment from "moment";
import getAPI from "../../utils/getAPITemplate";
import {
  Card,
  CardBody,
  Row,
  Col,
  CardHeader,
  //Input,
  FormGroup,
  Label,
} from "reactstrap";
import { DataTable } from "primereact/datatable";
import { Button } from "primereact/button";
import { Column } from "primereact/column";
//import EditDetail from "./drEditDetail";
//import roleRight from "../../utils/checkRights";
import postApi from "../../utils/postAPITemplate";
import LoadingOverlay from "react-loading-overlay";
import isEmpty from "../../utils/isEmpty";
//import config from "../../config";
import { Calendar } from "primereact/calendar";
import Select from "react-select";
const initialState = {
  wingId: "",
  sectionId: "",
  gradeId: "",
  staff: "",
  empId: "",
  fdate: "",
  tDate: "",
  empList: [],
  wingList: [],
  secList: [],
  gradeList: [],
  staffList: [
    { value: "", label: "Select an Option" },
    { value: "S", label: "Staff" },
    { value: "O", label: "Officer" },
  ],
  getRecord: {},
  expandedRows: null,
  updateModel: false,
  modelOpen: false,
  globalFilter: null,

  row: {},
  loading: false,
  errors: {},
  showMessage: "S",
};

class SearchEmployee extends Component {
  constructor() {
    super();
    this.state = initialState;
    document.title = "Search Employee::PID";
  }
  showModel = (item) => {
    this.setState({ modelOpen: !this.state.modelOpen, getRecord: item });
  };

  componentDidMount = async () => {
    let wingdata = {
      url: "pid/lovWing",
      showMessage: "",
    };
    const wingResponse = await getAPI(wingdata);
    if (wingResponse && !isEmpty(wingResponse)) {
      this.setState({
        wingList: wingResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        wingList: [],
      });
    }

    let gradedata = {
      url: "pid/lovGrade",
      showMessage: "",
    };
    const gradeResponse = await getAPI(gradedata);
    if (gradeResponse && !isEmpty(gradeResponse)) {
      this.setState({
        gradeList: gradeResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        gradeList: [],
      });
    }
    let sectiondata = {
      url: "pid/lovSection",
      showMessage: "",
    };
    const secResponse = await getAPI(sectiondata);
    if (secResponse && !isEmpty(secResponse)) {
      this.setState({
        secList: secResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        secList: [],
      });
    }
  };

  setWingValue = async (e) => {
    this.setState({ wingId: e.value });
    
  };

  setsectionValue = (e) => {
    this.setState({ sectionId: e.value });
  };

  setgradeValue = (e) => {
    this.setState({ gradeId: e.value });
  };

  setstaffValue = (e) => {
    this.setState({ staff: e.value });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {}

  onChangeInput = (e) => {
    if (e.target && !isEmpty(e.target) && validator.isNumeric(e.target.value)) {
      this.setState({ [e.target.name]: e.target.value });
    } else if (e.target.value.length === 0) {
      this.setState({ [e.target.name]: "" });
    }
  };
  actionTemplate = (rowData, column) => {
    return (
      <div style={{ textAlign: "center" }}>
        <Button
          type="button"
          icon="pi pi-pencil"
          className="p-button-warning"
          tooltip="Employee Detail"
          tooltipOptions={{ position: "left" }}
          onClick={this.showModel.bind(this, rowData)}
        ></Button>
      </div>
    );
  };

  statusTemplate = (rowData, column) => {
    return (
      <div>
        {rowData.staff && rowData.staff === "S" ? (
          <div> Staff </div>
        ) : (
          <div>Officer</div>
        )}
      </div>
    );
  };
  genderTemplate = (rowData, column) => {
    return (
      <div>
        {rowData.gender && rowData.gender === "M" ? (
          <div> Male </div>
        ) : (
          <div>Female</div>
        )}
      </div>
    );
  };

  updateDetails = (id) => {};

  SearchTransaction = async (e) => {
    let errors = {};
    const data = {
      wingId: this.state.wingId,
      sectionId: this.state.sectionId,
      gradeId: this.state.gradeId,
      staff: this.state.staff,
      empId: this.state.empId,
      fdate: this.state.fdate===""?"":moment(this.state.fdate).format("YYYY-MM-DD HH:mm:ss"),
      tDate: this.state.tDate===""?"":moment(this.state.tDate).format("YYYY-MM-DD HH:mm:ss"),
      url: "pid/tasEmployessData",
    };
    const fdate = isEmpty(data.fdate.toString()) ? "": data.fdate.toString();
    const tDate = isEmpty(data.tDate.toString()) ? "": data.tDate.toString();
    if (validator.isEmpty(fdate)) {
      errors.fdate = "From Date is Required";
    }
    if (validator.isEmpty(tDate)) {
      errors.tDate = "To Date is Required";
    }
    if(validator.isEmpty(data.staff)){
      errors.staff = "Please Select an Option";
    }
    
    this.setState({
      loading: true,
      empList: [],
      errors:errors,
    });
    if(errors && isEmpty(errors)){
    const response = await postApi(data);
    //console.log(response);
    if (response && !isEmpty(response)) {
      this.setState({
        empList: response.map((item) => {
          return {
            name:
              item.tblEmployee.firstname +
              " " +
              (isEmpty(item.tblEmployee.middlename)
                ? ""
                : item.tblEmployee.middlename) +
              " " +
              (isEmpty(item.tblEmployee.lastname)
                ? ""
                : item.tblEmployee.lastname),
            department: item.tblEmployee.lkpDepartment.descr,
            section: item.tblEmployee.lkpSection.descr,
            wing: item.tblEmployee.lkpDesignation.descr,
            staff: item.tblEmployee.staffOfficer,
            gender: item.tblEmployee.gender,
            timein: item.timeIn!==null?moment(item.timeIn).format('HH:mm'):"ABSENT",
          };
        }),
      });
      this.setState({errors});
    }
  }
    this.setState({errors:errors, loading: false });
  };

  render() {
    let { errors } = this.state;
    const customStyles = {
      control: (base) => ({
        ...base,
        height: 30,
        minHeight: 30,
      }),
    };
    let content = "";
    if (this.state.empList && !isEmpty(this.state.empList)) {
      content = (
        <div>
          <DataTable
            className="table-bordered"
            responsive={true}
            emptyMessage="No records found"
            value={this.state.empList}
            dataKey="empAttId"
            style={{ textAlign: "center" }}
            paginator={true}
            rows={10}
            rowsPerPageOptions={[10, 20, 30]}
          >
            <Column
              field="name"
              header="Employee Name"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="timein"
              header="Time In"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            <Column
              field="department"
              header="Department"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="section"
              header="Section"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="wing"
              header="Wing"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            <Column
              field="staff"
              header="Employee Type"
              body={this.statusTemplate}
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            <Column
              field="gender"
              header="Gender"
              body={this.genderTemplate}
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            {/* <Column
              body={this.actionTemplate}
              field="actions"
              header="Actions"
            /> */}
          </DataTable>
        </div>
      );
    }

    return (
      <Row>
        {/* <EditDetail
          modelOpen={this.showModel}
          isOpen={this.state.modelOpen}
          record={this.state.getRecord}
        /> */}
        <Col>
          <Card>
            <CardHeader>
              <h5>Search Time Attendance</h5>
            </CardHeader>
            <CardBody>
              <LoadingOverlay
                active={this.state.loading}
                spinner
                text="Please wait..."
              >
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>Wing</Label>

                      <Select
                        onChange={this.setWingValue}
                        options={this.state.wingList}
                      />

                      <p className="error">{errors.relCircleId}</p>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Cell/Selection</Label>
                      <Select
                        onChange={this.setsectionValue}
                        options={this.state.secList}
                      />
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Grade</Label>
                      <Select
                        onChange={this.setgradeValue}
                        options={this.state.gradeList}
                      />

                      <p className="error">{errors.relCircleId}</p>
                    </FormGroup>
                  </Col>
                  {/* <Col md="3">
                    <FormGroup>
                      <Label>Employee #</Label>
                      <Input
                        minLength="9"
                        maxLength="9"
                        type="text"
                        value={this.state.transRefNum}
                        name="transRefNum"
                        onChange={this.onChangeInput}
                        placeholder="Enter Transaction ID "
                      />
                      <p className="error">{errors.transRefNum}</p>
                    </FormGroup>
                  </Col></Row> */}
                </Row>
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>Staff/Officer <span style={{ color: "red" }}> *</span></Label>
                      <Select
                        styles={customStyles}
                        onChange={this.setstaffValue}
                        options={this.state.staffList}
                        //value={this.state.relCircle_obj}
                      />

                      <p className="error">{errors.staff}</p>
                    </FormGroup>
                  </Col>
                  {/* <Col md="3">
                      <FormGroup>
                        <Label>
                         Position
                          <span style={{ color: "red" }}> *</span>
                        </Label>
                        <Select
                          styles={customStyles}
                          onChange={this.setRelCircleValue}
                          options={this.state.relCirclelist}
                          value={this.state.relCircle_obj}
                        />

                        <p className="error">{errors.relCircleId}</p>
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label>
                         Contract Type
                          <span style={{ color: "red" }}> *</span>
                        </Label>
                        <Select
                          styles={customStyles}
                          onChange={this.setRelCircleValue}
                          options={this.state.relCirclelist}
                          value={this.state.relCircle_obj}
                        />

                        <p className="error">{errors.relCircleId}</p>
                      </FormGroup>
                    </Col>
                     */}
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        From Date<span style={{ color: "red" }}> *</span>
                      </Label>
                      <br />
                      <Calendar
                        style={{
                          fontSize: "14px",
                          width: "20px",
                        }}
                        onFocus={() => {
                          setTimeout(() => {
                            const element = document.getElementsByClassName(
                              "p-datepicker"
                            );
                            element[0].style.top = null;
                          }, 200);
                        }}
                        hideOnDateTimeSelect={true}
                        value={this.state.fdate}
                        onChange={(e) => this.setState({ fdate: e.value })}
                        showTime={true}
                        maxDate={this.state.toDate}
                        showSeconds={true}
                      />
                      <p className="error">{errors.fdate}</p>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        To Date<span style={{ color: "red" }}> *</span>
                      </Label>
                      <br />
                      <Calendar
                        onFocus={() => {
                          setTimeout(() => {
                            const element = document.getElementsByClassName(
                              "p-datepicker"
                            );
                            element[1].style.top = null;
                          }, 200);
                        }}
                        hideOnDateTimeSelect={true}
                        value={this.state.tDate}
                        minDate={this.state.fromDate}
                        onChange={(e) => this.setState({ tDate: e.value })}
                        showTime={true}
                        showSeconds={true}
                      />
                      <p className="error">{errors.tDate}</p>
                    </FormGroup>
                  </Col>

                  <Col md="3" style={{ marginTop: "28px" }}>
                    <Button
                      type="submit"
                      color="primary"
                      label="Search"
                      onClick={this.SearchTransaction}
                    />
                  </Col>
                </Row>

                <div className="content-section implementation">{content}</div>
              </LoadingOverlay>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

SearchEmployee.propTypes = {};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {})(SearchEmployee);
