import React, { Component } from "react";
import {
  Row,
//  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
//  Badge
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
//import RoleValidation from "./validation/RoleValidation";
//import Loading from "../../utils/spinner";
import { createRole } from "../../redux/role_module/actions";
import FullScreen from "../../utils/maximize-dialog";
import isEmpty from "../../utils/isEmpty";
import moment from "moment";
const initialState = {
  title:"",
  errors: {},
  empList:[],
};

class AddRole extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  cancelDialog = () => {
    this.props.modelOpen();
    this.setState(initialState);
  };
  statusTemplate = (rowData, column) => {
    return (
      <div>
        {rowData.staff && rowData.staff === "S" ? (
        <div> Staff </div>
        ) : (
          <div>Officer</div>
        )}
      </div>
    );
  };
  genderTemplate = (rowData, column) => {
    return (
      <div>
        {rowData.gender && rowData.gender === "M" ? (
        <div> Male </div>
        ) : (
          <div>Female</div>
        )}
      </div>
    );
  };


  UNSAFE_componentWillReceiveProps(nextProps) {
    
      if (nextProps && nextProps.defaultvalue) {
        this.setState({
          empList: nextProps.defaultvalue.map((item) => {
            return {
              name:  item.tblEmployee.firstname+" "+
              (isEmpty(item.tblEmployee.middlename) ? "" :item.tblEmployee.middlename)+" "+
              (isEmpty(item.tblEmployee.lastname) ? "" :item.tblEmployee.lastname),
              department:item.tblEmployee.lkpDepartment.descr,
              section:item.tblEmployee.lkpSection.descr,
              wing:item.tblEmployee.lkpDesignation.descr,
              staff:item.tblEmployee.staffOfficer,
              gender:item.tblEmployee.gender,
              timein: item.timeIn!==null?moment(item.timeIn).format('HH:mm'):"ABSENT",
              
             // moment(item.timeIn, "MM-DD-YYYY", true).format("YYYY-MM-DD"),
            };
          }),
          title:nextProps.defaultTitle,
        });

    }
  }
  render() {
    const { title } = this.state;
    let content = "";

      content = (
        <Row>
              <DataTable
            className="table-bordered"
            responsive={true}
            emptyMessage="No records found"
            value={this.state.empList}
            dataKey="empAttId"
            style={{ textAlign: "center" }}
            paginator={true}
            rows={10}
            rowsPerPageOptions={[10, 20, 30]}
          >
            <Column
              field="name"
              header="Employee Name"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="timein"
              header="Time In"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            <Column
              field="department"
              header="Department"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="section"
              header="Section"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="wing"
              header="Wing"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
             <Column
              field="staff"
              header="Employee Type"
              body={this.statusTemplate}
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
               <Column
              field="gender"
              header="Gender"
              body={this.genderTemplate}
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            {/* <Column
              body={this.actionTemplate}
              field="actions"
              header="Actions"
            /> */}
          </DataTable>
       
    </Row>
      );
    

    return (
      <div>
        <Modal
          id="addrolemodel"
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader>
           {title}
            <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("addrolemodel")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.cancelDialog}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              {/* <Button type="submit" color="success">
                Save
              </Button> */}
              <Button color="secondary" onClick={this.cancelDialog}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

AddRole.propTypes = {
  modelOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  createRole: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
});

export default connect(mapStateToProps, { createRole })(AddRole);
