import React, { Component, 
  //lazy,
   //Suspense
   } from "react";
import Chart from "react-apexcharts";
import { connect } from "react-redux";
//import { Bar, Line } from "react-chartjs-2";
import { Card, CardBody, Col, Row } from "reactstrap";
//import { CustomTooltips } from "@coreui/coreui-plugin-chartjs-custom-tooltips";
//import { getStyle, hexToRgba } from "@coreui/coreui/dist/js/coreui-utilities";
import TotalEmployee from "./barchartDetail";
import totalImage from "../../assets/total_Attendance.png";
import PresentImage from "../../assets/present3.png";
import LateImage from "../../assets/late.png";
import AbsentImage from "../../assets/Absent.png";
//import postAPI from "../../utils/postAPITemplate";
import getAPI from "../../utils/getAPITemplate";
import isEmpty from "../../utils/isEmpty";
//const Widget03 = lazy(() => import("../../views/Widgets/Widget03"));

export class ChartABC extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      detail_show_list: [],
      employee_List: [],
      present_list: [],
      late_list: [],
      absent_list: [],
      leave_list: [],
      staff_list: [],
      officer_list: [],
      total_employee: "",
      present_emp: "",
      late_emp: "",
      absent_emp: "",
      staff_emp: "",
      officer_emp: "",
      modelOpen: false,
      sectionWiseOptions: {
        colors: ["#4dbd74", "#ffc107", "#f86c6b"//, "#20a8d8"
      ],
        chart: {
          id: "bar",
          events: {
            //selection: true,
            //  click: this.barChart,
            // => {
            //   console.log(config);
            // },
          },
        },
        xaxis: {
          categories: [],
        },
      },
      sectionWiseSeries: [],
      wingWiseSeries: [],
      wingWiseOptions: {
        //  colors: ["#4dbd74", "#ffc107", "#f86c6b"],
        chart: {
          width: 300,
          type: "pie",
          events: {
            //selection: true,
            // click: this.barChart,
            // => {
            //   console.log(config);
            // },
          },
        },
        labels: ["WEB","DESKTOP","TEST"],
        responsive: [
          {
            //  breakpoint: 480,
            options: {
              chart: {
                width: 200,
              },
              legend: {
                position: "top",
              },
            },
          },
        ],
      },
      todaysAttSeries: [],
      todaysAttOptions: {
        colors: ["#4dbd74", "#ffc107", "#f86c6b"],//, "#20a8d8"],
        chart: {
          width: 300,
          type: "pie",
          events: {
            dataPointSelection: this.todaysDetailView,
          },
        },
        labels: ["Present", "Late", "Absent"],//, "Leave"],
        responsive: [
          {
            breakpoint: 480,
            options: {
              chart: {
                width: 200,
              },
              legend: {
                position: "bottom",
              },
            },
          },
        ],
      },
      staffSeries: [],
      staffOptions: {
        //  colors: ["#4dbd74", "#ffc107", "#f86c6b"],
        chart: {
          width: 300,
          type: "pie",
          events: {
            //selection: true,
            dataPointSelection: this.staffOfficerDetail,
            // => {
            //   console.log(config);
            // },
          },
        },
        labels: ["Officer", "Staff"],
        responsive: [
          {
            //  breakpoint: 480,
            options: {
              chart: {
                width: 200,
              },
              legend: {
                position: "top",
              },
            },
          },
        ],
      },
    };
  }

  componentWillMount() {
    this.todays_attendance_chart();
    this.sectionWise_chart();
    this.wingWise_chart();
  }

  todays_attendance_chart = async () => {
    let emptypedata = {
      url: "pid/getTasDashboard",
      showMessage: "",
    };

    const empResponse = await getAPI(emptypedata);
    if (empResponse && !isEmpty(empResponse)) {
      this.setState({ employee_List: empResponse });
      let present_emps = [],
        late_emps = [],
        absent_emps = [],
        leave_emps = [],
        staff_emps = [],
        officer_emps = [];
      let staff_emp = 0,
        officer_emp = 0,
        total_employee = 0,
        present_emp = 0,
        late_emp = 0,
        absent_emp = 0,
        leave_emp = 0;
      empResponse.forEach((item) => {
        total_employee++;

        if (item.status && item.status === "P") {
          present_emp++;
          present_emps.push(item);
        }
        if (item.status && item.status === "L") {
          late_emp++;
          late_emps.push(item);
        }

        if (item.status && item.status === "A") {
          absent_emp++;
          absent_emps.push(item);
        }

        // if (item.status && item.status === "V") {
        //   leave_emp++;
        //   leave_emps.push(item);
        // }

        if (!isEmpty(item.tblEmployee)) {
          if (
            item.tblEmployee &&
            item.tblEmployee.staffOfficer &&
            item.tblEmployee.staffOfficer === "S"
          ) {
            staff_emp++;
            staff_emps.push(item);
          } else if (
            item.tblEmployee &&
            item.tblEmployee.staffOfficer &&
            item.tblEmployee.staffOfficer === "O"
          ) {
            officer_emp++;
            officer_emps.push(item);
          }
          if (!isEmpty(item.tblEmployee.lkpsection)) {
          }
        }
      });
      let todaysList = [];
      todaysList.push(present_emp);
      todaysList.push(late_emp);
      todaysList.push(absent_emp);
     // todaysList.push(leave_emp);

      this.setState({ todaysAttSeries: todaysList });

      this.setState({ staff_emp, officer_emp });

      let staffcountList = [];
      staffcountList.push(officer_emp);
      staffcountList.push(staff_emp);
      this.setState({ staffSeries: staffcountList });

      this.setState({
        total_employee,
        present_emp,
        late_emp,
        absent_emp,
        present_list: present_emps,
        late_list: late_emps,
        absent_list: absent_emps,
        leave_list: leave_emps,
        staff_list: staff_emps,
        officer_list: officer_emps,
      });
    }
  };

  sectionWise_chart = async () => {
    let data = {
      url: "pid/getTodaySectionWiseBarChart",
      showMessage: "",
    };

    const response = await getAPI(data);
     //console.log(response);
    // const response = {
    //   categories: ["Software", "Hr"],
    //   series: [
    //     { name: "Present", data: [4, 0] },
    //     { name: "Late", data: [1, 0] },
    //     { name: "Absent", data: [1, 0] },
    //     //{ name: "Leave", data: [1, 0] },
    //   ],
    // };
    if (response && !isEmpty(response)) {
      const { sectionWiseOptions } = this.state;
      sectionWiseOptions.xaxis.categories = response.categories;

      this.setState({ sectionWiseOptions });
      this.setState({ sectionWiseSeries: response.series });
    }
  };

  wingWise_chart = async () => {
    let wingdata = {
      url: "pid/getTodayWingWiseBarChart",
      showMessage: "",
    };

    const wingresponse = await getAPI(wingdata);
    //     const wingresponse = {
    //   present_values: [4, 2],
    //   labels: ["Web developer", "Desktop application developer"],
    // };
    if (wingresponse && !isEmpty(wingresponse)) {
    console.log(wingresponse);
    //console.log(wingresponse);
    this.setState({ wingWiseSeries: wingresponse.present_values });
    const { wingWiseOptions } = this.state;
    wingWiseOptions.labels = wingresponse.labels;
    //wingWiseOptions.labels=["WEB","DESKTOP","TEST"];
    this.setState({ wingWiseOptions });
  }
  };

  staffOfficerDetail = (event, chartContext, config) => {
    const getValue = config.w.config.labels[config.dataPointIndex];
    if (getValue === "Officer") this.officer_employee_show();
    else if (getValue === "Staff") this.staff_employee_show();
  };
  todaysDetailView = (event, chartContext, config) => {
    const getValue = config.w.config.labels[config.dataPointIndex];
    if (getValue === "Present") this.present_employee_show();
    else if (getValue === "Late") this.late_employee_show();
    else if (getValue === "Absent") this.absent_employee_show();
    //else if (getValue === "Leave") this.leave_employee_show();
  };

  total_employee_show = () => {
    this.setState({
      detail_show_list: this.state.employee_List,
      title: "Total Employee Detail",
    });
    this.setState({ modelOpen: !this.state.modelOpen });
  };
  present_employee_show = () => {
    this.setState({
      detail_show_list: this.state.present_list,
      title: "Present Employee Detail",
    });
    this.setState({ modelOpen: !this.state.modelOpen });
  };
  late_employee_show = () => {
    this.setState({
      detail_show_list: this.state.late_list,
      title: "Late Employee Detail",
    });
    this.setState({ modelOpen: !this.state.modelOpen });
  };
  absent_employee_show = () => {
    this.setState({
      detail_show_list: this.state.absent_list,
      title: "Absent Employee Detail",
    });
    this.setState({ modelOpen: !this.state.modelOpen });
  };
  leave_employee_show = () => {
    this.setState({
      detail_show_list: this.state.leave_list,
      title: "Leave Employee Detail",
    });
    this.setState({ modelOpen: !this.state.modelOpen });
  };
  staff_employee_show = () => {
    this.setState({
      detail_show_list: this.state.staff_list,
      title: "Staff Detail",
    });
    this.setState({ modelOpen: !this.state.modelOpen });
  };
  officer_employee_show = () => {
    this.setState({
      detail_show_list: this.state.officer_list,
      title: "Officer Detail",
    });
    this.setState({ modelOpen: !this.state.modelOpen });
  };
  showModel = () => {
    this.setState({ modelOpen: !this.state.modelOpen });
  };
  render() {
    let sectionWiseChartContent = "";
    let todayAttendenceContent = "";
    let stafOfficerConetnt = "";
    let wingWiseOption = "";
    if (
      !isEmpty(this.state.sectionWiseOptions) &&
      !isEmpty(this.state.sectionWiseSeries)
    ) {
      sectionWiseChartContent = (
        <Chart
          options={this.state.sectionWiseOptions}
          series={this.state.sectionWiseSeries}
          type="bar"
          height="400"
          width="1200"
        />
      );
    }

    if (
      !isEmpty(this.state.todaysAttOptions) &&
      !isEmpty(this.state.todaysAttSeries)
    ) {
      todayAttendenceContent = (
        <Chart
          options={this.state.todaysAttOptions}
          series={this.state.todaysAttSeries}
          type="pie"
          width="380"
        />
      );
    }

    if (!isEmpty(this.state.staffOptions) && !isEmpty(this.state.staffSeries)) {
      stafOfficerConetnt = (
        <Chart
          options={this.state.staffOptions}
          series={this.state.staffSeries}
          type="pie"
          width="380"
        />
      );
    }

    if (
      !isEmpty(this.state.wingWiseOptions) &&
      !isEmpty(this.state.wingWiseSeries)
    ) {
      wingWiseOption = (
        <Chart
          options={this.state.wingWiseOptions}
          series={this.state.wingWiseSeries}
          type="pie"
          width="400"
        />
      );
    }

    const { total_employee, present_emp, late_emp, absent_emp } = this.state;
    //console.log(this.state.options ,this.state.series);

    return (
      <Row>
        <TotalEmployee
          modelOpen={this.showModel}
          isOpen={this.state.modelOpen}
          defaultvalue={this.state.detail_show_list}
          defaultTitle={this.state.title}
        />
        <Row>
          <Col md="3">
            <Card className="text-white bg-primary" style={{ width: "300px" }}>
              <CardBody onClick={this.total_employee_show}>
                <img
                  src={totalImage}
                  alt="Total"
                  height={50}
                  width={50}
                  style={{ float: "right" }}
                />
                <h1>Total</h1>

                <div className="text-value">{total_employee}</div>
              </CardBody>
            </Card>
          </Col>

          <Col md="3">
            <Card className="text-white bg-success" style={{ width: "300px" }}>
              <CardBody onClick={this.present_employee_show}>
                <img
                  src={PresentImage}
                  alt="Total"
                  height={50}
                  width={50}
                  style={{ float: "right" }}
                />
                <h1>Present</h1>
                <div className="text-value">{present_emp}</div>
              </CardBody>
            </Card>
          </Col>

          <Col md="3">
            <Card className="text-white bg-warning" style={{ width: "300px" }}>
              <CardBody onClick={this.late_employee_show}>
                <img
                  src={LateImage}
                  alt="Total"
                  height={50}
                  width={50}
                  style={{ float: "right" }}
                />
                <h1>Late</h1>
                <div className="text-value">{late_emp}</div>
              </CardBody>
            </Card>
          </Col>

          <Col md="3">
            <Card className="text-white bg-danger" style={{ width: "300px" }}>
              <CardBody onClick={this.absent_employee_show}>
                <img
                  src={AbsentImage}
                  alt="Total"
                  height={50}
                  width={50}
                  style={{ float: "right" }}
                />
                <h1>Absent</h1>
                <div className="text-value">{absent_emp}</div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Card>
            <CardBody>
              <h3>Staff/Officer Attendance</h3>
              {stafOfficerConetnt}
            </CardBody>
          </Card>
          <Card>
            <CardBody>
              <h3>Today's Attendance</h3>
              {todayAttendenceContent}
            </CardBody>
          </Card>
          <Card>
            <CardBody>
              <h3>Wing-Wise Attendance</h3>
              {wingWiseOption}
            </CardBody>
          </Card>
        </Row>
        <Row>
       
          <Card>
            <CardBody>
              <h3>Section-Wise Attendance</h3>
              {sectionWiseChartContent}
            </CardBody>
          </Card>
        </Row>
      </Row>
    );
  }
}
ChartABC.propTypes = {};
const mapStateToProps = (state) => ({});
export default connect(mapStateToProps, {})(ChartABC);
