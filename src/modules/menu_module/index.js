import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { Card, Badge, CardBody, CardHeader, Row, Col } from "reactstrap";
import AddModule from "./addMenu";
import UpdateModule from "./updateMenu";
//import roleRight from "../../utils/checkRights";
//import isEmpty from "../../utils/isEmpty";
import { DataTable } from "primereact/datatable";
import { getAllMenus } from "../../redux/menu_module/actions";
import LoadingOverlay from "react-loading-overlay";
export class BussinessFunctionModule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu_Items: [],
      alerts: {},
      loading: false,
      modelOpen: false,
      updateModelOpen: false,
      businessrow: {},
      row: {},
      modelBusinessOpen: false,
      viewBusinessFunctionModel: false,
    };
    document.title = "Manage Menu :: DFS";
    //this.actionTemplate = this.actionTemplate.bind(this);
  }

  componentDidMount = async () => {
    this.props.getAllMenus();
  };
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.menuRed && nextProps.menuRed.allMenus) {
      this.setState({
        menu_Items: nextProps.menuRed.allMenus,
        loading: nextProps.menuRed.loading,
      });
    }
    if (nextProps.alerts && nextProps.alerts.alerts) {
      this.setState({ alert: nextProps.alerts.alerts });
    }
  }

  showModel = () => {
    this.setState(() => {
      return {
        modelOpen: !this.state.modelOpen,
      };
    });
  };
  updateModel = (item) => {
    this.setState(() => {
      return {
        updateModelOpen: !this.state.updateModelOpen,
        row: item,
      };
    });
  };

  showBusinessModel = (item) => {
    this.setState({
      modelBusinessOpen: !this.state.modelBusinessOpen,
      businessrow: item,
    });
  };

  actionTemplate = (rowData, column) => {
    return (
      <div style={{ textAlign: "center" }}>
        <Button
          type="button"
          icon="pi pi-pencil"
          tooltip="Edit Menu"
          tooltipOptions={{ position: "left" }}
          className="p-button-warning"
          onClick={this.updateModel.bind(this, rowData)}
        ></Button>
      </div>
    );
  };

  statusTemplate = (rowData, column) => {
    return (
      <div>
        {rowData.status && rowData.status === "A" ? (
          <Badge color="info">Active</Badge>
        ) : (
          <Badge color="secondary">InActive</Badge>
        )}
      </div>
    );
  };
  menuTypeTemplate = (rowData, column) => {
    return (
      <div>{rowData.status && rowData.status === "P" ? "PARENT" : "FORM"}</div>
    );
  };

  render() {
    let content = "";

    content = (
      <LoadingOverlay active={this.state.loading} spinner text="Please wait...">
        <DataTable
          className="table-bordered"
          responsive={true}
          value={this.state.menu_Items}
          dataKey="menuid"
          style={{ textAlign: "center" }}
          paginator={true}
          rows={10}
          rowsPerPageOptions={[10, 20, 30]}
        >
          <Column
            filter={true}
            sortable={true}
            field="menuCode"
            header="Menu Code"
          />
          <Column
            filter={true}
            sortable={true}
            field="menuDescr"
            header="Menu Descr."
          />
          <Column
            filter={true}
            sortable={true}
            body={this.menuTypeTemplate}
            field="menuType"
            header="Type"
          />
          <Column
            filter={true}
            sortable={true}
            field="menuPath"
            header="Path"
          />

          <Column
            filter={true}
            sortable={true}
            body={this.statusTemplate}
            field="status"
            header="Status"
          />
          <Column body={this.actionTemplate} field="actions" header="Actions" />
        </DataTable>
      </LoadingOverlay>
    );

    return (
      <Row>
        <UpdateModule
          defaultvalue={this.state.row}
          modelOpen={this.updateModel}
          isOpen={this.state.updateModelOpen}
        />
        <AddModule modelOpen={this.showModel} isOpen={this.state.modelOpen} />

        <Col>
          <Card>
            <CardHeader>
              <h5>
                Manage Menu
                <Button
                  icon="pi pi-plus"
                  className="p-button-success float-right m-1"
                  tooltip="New Menu"
                  tooltipOptions={{ position: "left" }}
                  onClick={this.showModel}
                ></Button>
              </h5>
            </CardHeader>
            <CardBody>{content}</CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

BussinessFunctionModule.propTypes = {
  getAllMenus: PropTypes.func.isRequired,
  alerts: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  menuRed: state.menu_Reducer,
  alerts: state.alerts,
});

export default connect(mapStateToProps, {
  getAllMenus,
})(BussinessFunctionModule);
