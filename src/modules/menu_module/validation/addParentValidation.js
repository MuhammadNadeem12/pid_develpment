import validator from "validator";
import isEmptyCheck from "../../../utils/isEmpty";

const AddParentValidation = (data) => {
  let errors = {};
  const menuCode = isEmptyCheck(data.menuCode)
    ? ""
    : data.menuCode.trim().toString();
  const menuDescr = isEmptyCheck(data.menuDescr)
    ? ""
    : data.menuDescr.toString();
  const menuType = isEmptyCheck(data.menuType) ? "" : data.menuType.toString();

  const sortSeq = isEmptyCheck(data.sortSeq) ? "" : data.sortSeq.toString();
  const status = isEmptyCheck(data.status) ? "" : data.status.toString();

  if (validator.isEmpty(menuCode)) {
    errors.menuCode = "Module  code is Required";
  }
  if (validator.isEmpty(menuDescr)) {
    errors.menuDescr = "Menu Description is Required";
  }
  if (validator.isEmpty(menuType)) {
    errors.menuType = "Menu Type is Required";
  }
  if (validator.isEmpty(status)) {
    errors.status = "Status is Required";
  }

  if (validator.isEmpty(sortSeq)) {
    errors.sortSeq = "sort Sequence is Required";
  }

  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors,
  };
  return feedback;
};
export default AddParentValidation;
