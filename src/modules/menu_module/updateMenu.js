import React, { Component } from "react";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import menuValidation from "./validation/menuValidation";
import ParentValidation from "./validation/addParentValidation";
import Loading from "../../utils/spinner";
import { updateMenuRow } from "../../redux/menu_module/actions";
import FullScreen from "../../utils/maximize-dialog";
import Select from "react-select";
import { getParentMenuLov } from "../../redux/lov/actions";
import { InputMask } from "primereact/inputmask";
import IsEmpty from "../../utils/isEmpty";
import isEmpty from "../../utils/isEmpty";
const initialState = {
  menuType: "",
  menuTypeList: [
    { value: "", label: "Select Status" },
    { value: "P", label: "PARENT" },
    { value: "F", label: "FORM" },
  ],
  menuTypeObj: {},
  menuCheck: true,
  menuCode: "",
  menuDescr: "",
  menuPath: "",
  menuId: "",
  status: "",
  sortSeq: "",
  statusList: [
    { value: "", label: "Select Status" },
    { value: "A", label: "ACTIVE" },
    { value: "I", label: "IN-ACTIVE" },
  ],
  statusObj: {},
  parentMenu: "",
  parentMenuList: [],
  parentMenuObj: { value: "", label: "Select Parent" },
  errors: {},
  disable: true,
  disableSubmitButton: false,
};

class UpdateMenu extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    this.props.getParentMenuLov();
  }

  formSubmit = (e) => {
    e.preventDefault();

    const data = {
      menuId: this.state.menuId,
      menuCode: this.state.menuCode.toUpperCase(),
      menuDescr: this.state.menuDescr.toUpperCase(),
      menuType: this.state.menuType,
      parentMenu: this.state.parentMenu,
      status: this.state.status,
      sortSeq: this.state.sortSeq,
      menuPath: this.state.menuPath,
    };

    if (data.menuType === "F") {
      const { haserror, errors } = menuValidation(data);
      if (haserror) {
        this.setState({ errors });
      } else {
        this.setState({ disableSubmitButton: true });
        this.props.updateMenuRow(data);
        this.setState(initialState);
        this.props.modelOpen();
      }
    } else {
      const { haserror, errors } = ParentValidation(data);
      if (haserror) {
        this.setState({ errors });
      } else {
        this.props.updateMenuRow(data);
        this.setState(initialState);
        this.props.modelOpen();
      }
    }
  };
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.lov && nextProps.lov.parentmanu_lov) {
      this.setState({
        parentMenuList: nextProps.lov.parentmanu_lov.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    }
    if (nextProps.defaultvalue) {
      let selectedmenupath = "",
        selectedmenuObj = { value: "", label: "Select Menu" },
        selectedParent = "",
        selectedParentObj = { value: "", label: "Select Parent" },
        selectedStatus = "",
        selectedStatusObj = { value: "", label: "Select Status" },
        selectedSortSeq = "";
      if (nextProps.defaultvalue.menuType) {
        selectedmenuObj = this.state.menuTypeList.filter(
          (item) => nextProps.defaultvalue.menuType === item.value
        );

        if (nextProps.defaultvalue.menuType === "F") {
          this.setState({ menuCheck: false });
        }
      }

      if (
        !IsEmpty(nextProps.defaultvalue.parentMenu) &&
        !isEmpty(this.state.parentMenuList)
      ) {
        selectedParentObj = this.state.parentMenuList.filter(
          (item) =>
            parseInt(item.value) === parseInt(nextProps.defaultvalue.parentMenu)
        );
        selectedParent = nextProps.defaultvalue.parentMenu;
      }

      if (nextProps.defaultvalue.status) {
        selectedStatusObj = this.state.statusList.filter(
          (item) => nextProps.defaultvalue.status === item.value
        );
        selectedStatus = nextProps.defaultvalue.status;
      }

      if (nextProps.defaultvalue.menuPath !== null) {
        selectedmenupath = nextProps.defaultvalue.menuPath;
      }
      if (nextProps.defaultvalue.sortSeq !== null) {
        selectedSortSeq = nextProps.defaultvalue.sortSeq;
      }

      this.setState({
        menuCode: nextProps.defaultvalue.menuCode,
        menuDescr: nextProps.defaultvalue.menuDescr,
        menuId: nextProps.defaultvalue.menuId,
        menuPath: selectedmenupath,
        menuType: nextProps.defaultvalue.menuType,
        menuTypeObj: selectedmenuObj,
        parentMenu: selectedParent,
        sortSeq: selectedSortSeq,
        status: selectedStatus,
        statusObj: selectedStatusObj,
      });
      if (selectedParentObj && !isEmpty(selectedParentObj)) {
        this.setState({ parentMenuObj: selectedParentObj[0] });
      }
    }
  }
  setmenuType = (e) => {
    if (e.value === "P") {
      this.setState({
        menuType: e.value,
        menuTypeObj: e,
        menuCheck: true,
        parentMenu: "",
        menuPath: "",
        parentMenuObj: { value: "", label: "Select Parent" },
      });
    } else {
      this.setState({ menuType: e.value, menuCheck: false, menuTypeObj: e });
    }
  };

  setStatus = (e) => {
    this.setState({ status: e.value });
  };
  setParent = (e) => {
    this.setState({ parentMenu: e.value, parentMenuObj: e });
  };

  render() {
    console.log(this.state);
    const { errors, loading } = this.state;
    let content = "";
    if (loading) {
      content = <Loading />;
    } else {
      content = (
        <Row>
          <Col md="6">
            <FormGroup>
              <Label>
                Menu Type<span style={{ color: "red" }}> *</span>
              </Label>
              <Select
                isDisabled={this.state.disable}
                onChange={this.setmenuType}
                options={this.state.menuTypeList}
                value={this.state.menuTypeObj}
              />
              <p className="error">{errors.menuType}</p>
            </FormGroup>
          </Col>
          <Col md="6">
            <FormGroup>
              <Label>
                Parent<span style={{ color: "red" }}> *</span>
              </Label>
              <Select
                isDisabled={this.state.menuCheck}
                onChange={this.setParent}
                options={this.state.parentMenuList}
                value={this.state.parentMenuObj}
              />
              <p className="error">{errors.parentMenu}</p>
            </FormGroup>
          </Col>
          <Col md="6">
            <FormGroup>
              <Label>
                Menu Code<span style={{ color: "red" }}> *</span>
              </Label>
              <Input
                disabled={this.state.disable}
                type="text"
                value={this.state.menuCode}
                name="menuCode"
                onChange={this.onChangeInput}
                placeholder="Enter Menu Code"
              />
              <p className="error">{errors.menuCode}</p>
            </FormGroup>
          </Col>
          <Col md="6">
            <FormGroup>
              <Label>
                Menu Description<span style={{ color: "red" }}> *</span>
              </Label>
              <Input
                type="text"
                value={this.state.menuDescr}
                name="menuDescr"
                onChange={this.onChangeInput}
                placeholder="Enter Menu Description"
              />
              <p className="error">{errors.menuDescr}</p>
            </FormGroup>
          </Col>
          <Col md="6">
            <FormGroup>
              <Label>
                Path<span style={{ color: "red" }}> *</span>
              </Label>
              <Input
                disabled={this.state.disable}
                type="text"
                value={this.state.menuPath}
                name="menuPath"
                onChange={this.onChangeInput}
                placeholder="Enter Path"
              />
              <p className="error">{errors.menuPath}</p>
            </FormGroup>
          </Col>

          <Col md="6">
            <FormGroup>
              <Label>
                Status<span style={{ color: "red" }}> *</span>
              </Label>
              <Select
                onChange={this.setStatus}
                options={this.state.statusList}
                value={this.state.statusObj}
              />
              <p className="error">{errors.status}</p>
            </FormGroup>
          </Col>
          <Col md="6">
            <FormGroup>
              <Label>
                Sort Sequence<span style={{ color: "red" }}> *</span>
              </Label>
              <br />
              <InputMask
                disabled={this.state.disable}
                mask="999"
                value={this.state.sortSeq}
                onChange={(e) => this.setState({ sortSeq: e.value })}
              ></InputMask>

              <p className="error">{errors.sortSeq}</p>
            </FormGroup>
          </Col>
        </Row>
      );
    }
    return (
      <div>
        <Modal
          id="UpdateMenumodel"
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader>
            Update Menu Item
            <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("UpdateMenumodel")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.props.modelOpen}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button
                disabled={this.state.disableSubmitButton}
                type="submit"
                color="success"
              >
                Update
              </Button>
              <Button color="secondary" onClick={this.props.modelOpen}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

UpdateMenu.propTypes = {
  modelOpen: PropTypes.func.isRequired,
  updateMenuRow: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  getParentMenuLov: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
  lov: state.lov, //STORE
});

export default connect(mapStateToProps, {
  updateMenuRow,
  getParentMenuLov,
})(UpdateMenu);
