import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
//import { getCityLov } from "../../redux/lov/actions";
//import FileBase64 from "react-file-base64";
import ViewStation from "./viewStation";
//import docValidation from "./validation/document";
//import newsValidation from "./validation/newspapper";
import postAPI from "../../utils/postAPITemplate";
import Base64Downloader from 'react-base64-downloader';
import {
Card,
Badge,
CardBody,
Row,
Col,
CardHeader,
//Button as Btn,
//Form,
FormGroup,
Label,
Input,
CardFooter,
// Input
} from "reactstrap";
import { DataTable } from "primereact/datatable";
import { Button } from "primereact/button";
import { Column } from "primereact/column";
//import { getEmployes } from "../../redux/user_module/actions";
//import LoadingOverlay from "react-loading-overlay";
import isEmpty from "../../utils/isEmpty";
//import moment from "moment";
//import Select from "react-select";
import { withRouter } from "react-router-dom";
//import { Calendar } from "primereact/calendar";
import getAPI from "../../utils/getAPITemplate";
class ViewDpcument extends Component {
constructor(props) {

super();
this.state = { errors: {},document:[],row:{}}
this.viewDocs(props.match.params.id);
document.title = "View Documents :: PID";
}
onChangeInput = (e) => {
this.setState({ [e.target.name]: e.target.value });
};
showUpdateModel = (item) => {
this.setState({ updateModel: !this.state.updateModel, row:item });
};
actionTemplate = (rowData, column) => {
return (
<div style={{ textAlign: "center" }}>
<Button
type="button"
icon="pi pi-pencil"
className="p-button-eye"
tooltip="View Station"
tooltipOptions={{ position: "left" }}
onClick={this.showUpdateModel.bind(this, rowData)}
></Button>
</div>
);
};
SUBMITED = async(e) => {
   e.preventDefault();
   const data={
      docId:this.state.document[0].documentId,
      statusId:this.state.document[0].tblDocWorkflow.tblDocStatus3.statusId,
      url:"pid/documentAction",
      showMessage:"S"
   }
   const response = await postAPI(data);
   this.apiResponce(response);
 
}
SENDBACK = async(e) => {
   e.preventDefault();
   const data={
      docId:this.state.document[0].documentId,
      statusId:this.state.document[0].tblDocWorkflow.tblDocStatus2.statusId,
      url:"pid/documentAction",
      showMessage:"S"
   }
   const response = await postAPI(data);
   this.apiResponce(response);
}
apiResponce = (response) => {
   if (response && !isEmpty(response)) {
     this.props.history.push("/pendingdoc");
   } 
 };
viewDocs= async(id)=>{
  const data = {
    url: "pid/getDocumentById/"+id
    }
    const response = await getAPI(data);
  
    if (response && !isEmpty(response)) {
    this.setState({
    document: response,
    });
    } else {
    this.setState({ document: [] });
    }
}

downloadFile = (rowData, column) => {
 
   const BASE66 ="data:image/png;base64,"+rowData.fileBase64
   return (
   <div style={{ textAlign: "center" }}>
  
  <Base64Downloader base64={BASE66} downloadName={rowData.name}>
 Click to download
</Base64Downloader>
   </div>
   );
   };

formateUploadType = (rowData) => {
if(rowData.uploadType==="L"){
return "Letter";
}else{
return "Matter";
}
};
formateAdditonalCharges = (rowData) => {
if(rowData.additionalCharges==="M"){
return "Multiple";
}else{
return "Selection";
}
};
formateStaionType = (rowData) => {
if(rowData.additionalCharges==="C"){
return "Combined";
}else{
return "Individual";
}
};
statusTemplate = (rowData, column) => {
return (
<div>
   {rowData &&
   !isEmpty(rowData) &&
   rowData.status &&
   !isEmpty(rowData.status) &&
   rowData.status === "Y" ? (
   <Badge color="info"> Active </Badge>
   ) : (
   <Badge color="secondary"> InActive </Badge>
   )}
</div>
);
};
UNSAFE_componentWillReceiveProps(nextProps) {}
render() {
  
let content="";
const { errors } = this.state;
if(!isEmpty (this.state.document)){
content=
<Row>
   <ViewStation
      modelOpen={this.showUpdateModel}
      stationlist={this.state.row}
      isOpen={this.state.updateModel}
      getStation={this.getStationLanguage}
      />
   <Col>
   <Card>
      <CardHeader>
         <h3> View (R&I)</h3>
      </CardHeader>
      <CardBody>
         <div className="content-section implementation">
            <Row>
               <Col md="6">
               <FormGroup>
                  <Label>
                  Letter No :<span style={{ color: "red" }}> *</span>
                  </Label>
                  <Input
                     disabled={true}
                     type="text"
                     value={this.state.document[0].letterNo}
                     name="letterNo"
                     />
               </FormGroup>
               </Col>
               <Col md="3">
               <FormGroup>
                  <Label>
                  Rec Letter Date :
                  <span style={{ color: "red" }}> *</span>
                  </Label>
                  <br></br>
                  <Input
                     disabled={true}
                     type="text"
                     value={this.state.document[0].receiveDate}
                     name="letterNo"
                     />
                  <p className="error">{errors.receiveDate}</p>
               </FormGroup>
               </Col>
               <Col md="3">
               <FormGroup>
                  <Label>
                  Date Of Publication :
                  <span style={{ color: "red" }}> *</span>
                  </Label>
                  <br></br>
                  <Input
                     disabled={true}
                     type="text"
                     value={this.state.document[0].publishDate}
                     name="letterNo"
                     />
                  <p className="error">{errors.publishDate}</p>
               </FormGroup>
               </Col>
               <Col md="6">
               <FormGroup>
                  <Label>
                  External Department Name :
                  <span style={{ color: "red" }}> *</span>
                  </Label>
                  <Input
                     disabled={true}
                     type="text"
                     value={this.state.document[0].tblExternalDep.descr}
                     name="letterNo"
                     />
                  <p className="error">{errors.tblExternalDepId}</p>
               </FormGroup>
               </Col>
               <Col md="6">
               <FormGroup>
                  <Label>
                  Contact No :<span style={{ color: "red" }}> *</span>
                  </Label>
                  <Input
                     disabled={true}
                     type="text"
                     value={this.state.empNo}
                     name="empNo"
                     invalid={!isEmpty(errors.empNo)}
                     onChange={this.onChangeInput}
                     placeholder="Enter Contact No "
                     />
                  <p className="error">{errors.empNo}</p>
               </FormGroup>
               </Col>
               <Col md="6">
               <FormGroup>
                  <Label>
                  Advertisement Type :
                  <span style={{ color: "red" }}> *</span>
                  </Label>
                  <Input
                     disabled={true}
                     type="text"
                     value={this.state.document[0].tblDocType.descr}
                     name="letterNo"
                     />
                  <p className="error">{errors.tblDocTypeId}</p>
               </FormGroup>
               </Col>
               <Col md="6">
               <FormGroup>
                  <Label>
                  Advertisement Caption :
                  <span style={{ color: "red" }}> *</span>
                  </Label>
                  <Input
                     disabled={true}
                     type="text"
                     value={this.state.document[0].adCaption}
                     name="letterNo"
                     />
                  <p className="error">{errors.adCaption}</p>
               </FormGroup>
               </Col>
            </Row>
         </div>
      </CardBody>
   </Card>
   <hr>
   </hr>
   <Card>
      <CardHeader>
         <h3>Newspapers</h3>
      </CardHeader>
      <CardBody>
         <div className="content-section implementation">
            <Row>
               <Col md="12" style={{ marginTop: "15px" }}>
               <DataTable
               className="table-bordered"
               responsive={true}
               emptyMessage="No records found"
               value={this.state.document[0].tblDocumentDetails}
               // header={header}
               rowExpansionTemplate={this.rowExpansionTemplate}
               dataKey="documentDetailId"
               style={{ textAlign: "center" }}
               paginator={true}
               rows={10}
               rowsPerPageOptions={[10, 20, 30]}
               >
               <Column
                  sortable={true}
                  filter={true}
                  field="tblNewspaper.descr"
                  header="Newspaper"
                  />
               <Column
                  sortable={true}
                  filter={true}
                  body={this.formateStaionType}
                  field="stationType"
                  header="Station Type"
                  />
               <Column
                  sortable={true}
                  filter={true}
                  field="lkpSize.descr"
                  header="Size"
                  />
               <Column
                  sortable={true}
                  filter={true}
                  body={this.formateAdditonalCharges}
                  field="additionalCharges"
                  header="Additional Charges "
                  />
               <Column
                  body={this.actionTemplate}
                  header="Action "
                  />
               </DataTable>
               <p className="error">{errors.newsPapers}</p>
               </Col>
            </Row>
         </div>
      </CardBody>
   </Card>
   <hr>
   </hr>
   <Card>
      <CardHeader>
         <h3> Files</h3>
      </CardHeader>
      <CardBody>
         <div className="content-section implementation">
            <Row>
               <Col md="12" style={{ marginTop: "15px" }}>
               <DataTable
               className="table-bordered"
               responsive={true}
               emptyMessage="No records found"
               value={this.state.document[0].tblDocumentUploads}
               // header={header}
               rowExpansionTemplate={this.rowExpansionTemplate}
               dataKey="userId"
               style={{ textAlign: "center" }}
               paginator={true}
               rows={10}
               rowsPerPageOptions={[10, 20, 30]}
               >
               <Column
                  sortable={true}
                  filter={true}
                  field="uploadType"
                  body={this.formateUploadType}
                  header="Upload Type"
                  />
               <Column
                  sortable={true}
                  filter={true}
                  field="name"
                  header="Name"
                  />
               <Column
                  sortable={true}
                  filter={true}
                  body={this.statusTemplate}
                  field="status"
                  header="Status"
                  />
              
               <Column
                   body={this.downloadFile}
                  field="actions"
                  header="Actions"
                  />
               
               </DataTable>
               <p className="error">{errors.docFileUpoads}</p>
               </Col>
            </Row>
         </div>
      </CardBody>
      <CardFooter> <Button
         icon=""
       
         label={this.state.document[0].tblDocWorkflow.tblDocStatus3.descr}
         className="p-button-success p-button-lg"
         tooltipOptions={{ position: "left" }}
         onClick={this.SUBMITED}
         />
         <Button
         
         label={this.state.document[0].tblDocWorkflow.tblDocStatus2.descr}
         tooltipOptions={{ position: "right" }}
         style={{ marginLeft: "10px" }}
         className="p-button-info"
         onClick={this.SENDBACK}
         />
      </CardFooter>
   </Card>
   </Col>
</Row>
}else{
content=   
<Card>
   <CardBody>
      <div className="content-section implementation">
         <h2>Loading.....!!!</h2>
      </div>
   </CardBody>
</Card>
}
return (
<div>  {content}</div>
);
}
}
ViewDpcument.propTypes = {
employees: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
employees: state.users,
auth: state.auth,
});
export default connect(mapStateToProps, {  })(
withRouter(ViewDpcument)
);
