import React, { Component } from "react";
import { connect } from "react-redux";
import validator from "validator";

import {
  Card,
  CardBody,
  Row,
  Col,
  CardHeader,
  Input,
  FormGroup,
  Label,
} from "reactstrap";
import { DataTable } from "primereact/datatable";
import { Button } from "primereact/button";
import { Column } from "primereact/column";
//import EditDetail from "./drEditDetail";
//import roleRight from "../../utils/checkRights";
import postApi from "../../utils/postAPITemplate";
import getApi from "../../utils/getAPITemplate";
import LoadingOverlay from "react-loading-overlay";
import isEmpty from "../../utils/isEmpty";
//import config from "../../config";
import { Calendar } from "primereact/calendar";
import Select from "react-select";
const initialState = {
  pidNo: "",
  letterNo: "",
  adTypeId:'',
  adTypelist:[],
  adType_obj:{},
  newsPaper:"",
  newsPaperlist:[],
  newsPaper_obj:{},
  departmentId:"",
  department_obj:{},
  departmentlist:[],
  TableRecords: [],
  getRecord: {},
  expandedRows: null,
  updateModel: false,
  modelOpen: false,
  globalFilter: null,
  row: {},
  loading: false,
  errors: {},
  showMessage: "S",
};

class DREditSearch extends Component {
  constructor() {
    super();
    this.state = initialState;
    document.title = "Media Cell::PID";
  }
  showModel = (item) => {
    console.log(item);
    this.setState({ modelOpen: !this.state.modelOpen, getRecord: item });
  };

  componentDidMount = async () => {
    const data = { url: "pid/lovDoctype" }
    let response = await getApi(data);
    if(response && !isEmpty(response)){
      this.setState({adTypelist: response.map((item) => {
        return {
          value: item.code,
          label: item.descr,
        };
      })});}
      const data2 = { url: "pid/lovNewsPaper" }
    let response2= await getApi(data2);
    if(response2 && !isEmpty(response2)){
      this.setState({newsPaperlist: response2.map((item) => {
        return {
          value: item.code,
          label: item.descr,
        };
      })});}
      const data3 = { url: "pid/lovExternalDept" }
      let response3= await getApi(data3);
      if(response3 && !isEmpty(response3)){
        this.setState({departmentlist: response3.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        })});}
    // let responce = await roleRight(config.API_URL + "/dredit");
    // if (!isEmpty(responce)) {
    //   if (responce < 0) {
    //     this.props.history.push("/");
    //   } else {
    //     this.setState(initialState);
    //   }
    // }
  };

  UNSAFE_componentWillReceiveProps(nextProps) { }

  onChangeInput = (e) => {
    if (e.target && !isEmpty(e.target) && validator.isNumeric(e.target.value)) {
      this.setState({ [e.target.name]: e.target.value });
    } else if (e.target.value.length === 0) {
      this.setState({ [e.target.name]: "" });
    }
  };
  actionTemplate = (rowData, column) => {
    return (
      <div style={{ textAlign: "center" }}>
        <Button
          type="button"
          icon="pi pi-eye"
          className="p-button-info"
          tooltip="Document Detail"
          tooltipOptions={{ position: "left" }}
          onClick={this.showModel.bind(this, rowData)}
        ></Button>
      </div>
    );
  };

  updateDetails = (id) => { };
  setAdTypeValue= (e)=>{this.setState({adTypeId:e.value,adType_obj:e})}
  setDepartmentValue= (e)=>{this.setState({departmentId:e.value,department_obj:e})}
  setNewspaperValue= (e)=>{this.setState({newsPaper:e.value,newsPaper_obj:e})}
  SearchDocument = async (e) => {
    const data = {
     pidNo:this.state.pidNo,
       letterNo:this.state.letterNo,
       extDeptName:this.state.departmentId,
       newsPaper:this.state.newsPaper,
       docType:this.state.adTypeId,
       fromDate:this.state.fromDate,
       toDate:this.state.toDate,
      url: "pid/searchDocument"
    }

    // if (data.pidNo != "") {
    // this.setState({
    //   loading: true,
    //   TableRecords: [],
    //   errors: {},
    // });
    const response = await postApi(data);
    console.log(response);
    if (response && !isEmpty(response)) {
      this.setState({
        TableRecords: response,
        loading: false,
      });
    } else {
      this.setState({ loading: false });
    }
    // } else {
    //   this.setState({ errors: { pidNo: "Please Enter Transaction ID" } });
    // }
  };

  render() {
    let { errors } = this.state;
    const customStyles = {
      control: (base) => ({
        ...base,
        height: 30,
        minHeight: 30,
      }),
    };
    let content = "";
    if (this.state.TableRecords && !isEmpty(this.state.TableRecords)) {
      content = (
        <div>
          <DataTable
            className="table-bordered"
            responsive={true}
            emptyMessage="No records found"
            value={this.state.TableRecords}
            dataKey="transHeadId"
            style={{ textAlign: "center" }}
            paginator={true}
            rows={10}
            rowsPerPageOptions={[10, 20, 30]}
          >
            <Column
              field="pidNo"
              header="PID No."
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="letterNo"
              header="Letter No."
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            <Column
              field="tblExternalDep.descr"
              header="Department Name"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="tblDocType.descr"
              header="Ad Type"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="receiveDate"
              header="Receive Date"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
             <Column
              field="tblDocStatus.descr"
              header="Status"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            <Column
              body={this.actionTemplate}
              field="actions"
              header="Actions"
            />
          </DataTable>
        </div>
      );
    }

    return (
      <Row>
        {/* <EditDetail
          modelOpen={this.showModel}
          isOpen={this.state.modelOpen}
          record={this.state.getRecord}
        /> */}
        <Col>
          <Card>
            <CardHeader>
              <h5>Media Release Cell</h5>
            </CardHeader>
            <CardBody>
              <LoadingOverlay
                active={this.state.loading}
                spinner
                text="Please wait..."
              >
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>PID #</Label>
                      <Input
                        minLength="9"
                        maxLength="9"
                        type="text"
                        value={this.state.pidNo}
                        name="pidNo"
                        onChange={this.onChangeInput}
                        placeholder="Enter PID No. "
                      />
                      <p className="error">{errors.pidNo}</p>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>Letter #</Label>
                      <Input
                        minLength="13"
                        maxLength="13"
                        pattern="\d*"
                        type="text"
                        value={this.state.letterNo}
                        name="letterNo"
                        onChange={this.onChangeInput}
                        placeholder="Enter Letter No."
                      />
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        Department Name
                          <span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                        styles={customStyles}
                        onChange={this.setDepartmentValue}
                        options={this.state.departmentlist}
                        value={this.state.department_obj}
                      />

                      <p className="error">{errors.departmentId}</p>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        Newspaper
                          <span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                        styles={customStyles}
                        onChange={this.setNewspaperValue}
                        options={this.state.newsPaperlist}
                        value={this.state.newsPaper_obj}
                      />

                      <p className="error">{errors.newsPaper}</p>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        Ad Type
                          <span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                        styles={customStyles}
                        onChange={this.setAdTypeValue}
                        options={this.state.adTypelist}
                        value={this.state.adType_obj}
                      />

                      <p className="error">{errors.adTypeId}</p>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        From Date<span style={{ color: "red" }}> *</span>
                      </Label>
                      <br />
                      <Calendar
                        style={{
                          fontSize: "14px",
                          width: "20px",
                        }}
                        onFocus={() => {
                          setTimeout(() => {
                            const element = document.getElementsByClassName(
                              "p-datepicker"
                            );
                            element[0].style.top = null;
                          }, 200);
                        }}
                        hideOnDateTimeSelect={true}
                        value={this.state.fromDate}
                        onChange={(e) => this.setState({ fromDate: e.value })}
                        showTime={true}
                        maxDate={this.state.toDate}
                        showSeconds={true}
                      />
                      <p className="error">{errors.fromDate}</p>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        To Date<span style={{ color: "red" }}> *</span>
                      </Label>
                      <br />
                      <Calendar
                        onFocus={() => {
                          setTimeout(() => {
                            const element = document.getElementsByClassName(
                              "p-datepicker"
                            );
                            element[1].style.top = null;
                          }, 200);
                        }}
                        hideOnDateTimeSelect={true}
                        value={this.state.toDate}
                        minDate={this.state.fromDate}
                        onChange={(e) => this.setState({ toDate: e.value })}
                        showTime={true}
                        showSeconds={true}
                      />
                      <p className="error">{errors.toDate}</p>
                    </FormGroup>
                  </Col>


                  <Col md="3" style={{ marginTop: "28px" }}>
                    <Button
                      type="submit"
                      color="primary"
                      label="Search"
                      onClick={this.SearchDocument}
                    />
                  </Col>
                </Row>

                <div className="content-section implementation">{content}</div>
              </LoadingOverlay>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

DREditSearch.propTypes = {};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {})(DREditSearch);
