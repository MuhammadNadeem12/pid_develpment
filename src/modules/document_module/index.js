import React, { Component } from "react";
import { connect } from "react-redux";
//import PropTypes from "prop-types";
import FileBase64 from "react-file-base64";
import AddStation from "../../modules/document_module/addStationDialog";
import docValidation from "./validation/document";
import newsValidation from "./validation/newspapper";
import uploadFileValidation from "./validation/uploadFiles";
import postAPI from "../../utils/postAPITemplate";
import {
  Card,
  Badge,
  CardBody,
  Row,
  Col,
  CardHeader,
  //Button as Btn,
  //Form,
  FormGroup,
  Label,
  Input,
  CardFooter,
  // Input
} from "reactstrap";
import { DataTable } from "primereact/datatable";
import { Button } from "primereact/button";
import { Column } from "primereact/column";
//import LoadingOverlay from "react-loading-overlay";
import isEmpty from "../../utils/isEmpty";
import moment from "moment";
import Select from "react-select";
import { withRouter } from "react-router-dom";
import { Calendar } from "primereact/calendar";
import getAPI from "../../utils/getAPITemplate";
class AddDocument extends Component {
  constructor() {
    super();
    this.state = { errors: {},
    depnamesList:[],
    advertismentList:[],
    uploadtypeList: [
      { value: "", label: "Select an Option" },
      { value: "L", label: "Letter" },
      { value: "M", label: "Matter" },
    ],
     statusList: [
      { value: "", label: "Select an Option" },
      { value: "Y", label: "Activate" },
      { value: "N", label: "De-activate" },
    ],
    displayList: [
      { value: "", label: "Select an Option" },
      { value: "Y", label: "YES" },
      { value: "N", label: "NO" },
    ],
    StationTypeList: [
      { value: "", label: "Select an Option" },
      { value: "C", label: "Combined" },
      { value: "I", label: "Individaul" },
    ],
    addChargesList: [
      { value: "", label: "Select an Option" },
      { value: "M", label: "Multiple" },
      { value: "S", label: "Selection" },
    ],
    file:{},
    docFileUpoads:[],
    uploadType:"",
    status:"",
    newpapersList:[],
    sizeList:[],
    updateModel:false,
    tblNewspaperId:"",
    sectionlist:[],
    stations:[],
    newsPapers:[],
    lkpSizeId:"",
    displayStatus:"",
    letterNo:"",
    tblExternalDepId:"",
    tblDocTypeId:"",
    receiveDate:"",
    adCaption:"",
    publishDate:"",
    stationType:"",
    addCharges:"",
    newspaperObj:{ value: "", label: "Select an Option" },
    stationtypeObj:{ value: "", label: "Select an Option" },
    sizeObj:{ value: "", label: "Select an Option" },
    addchargObj:{ value: "", label: "Select an Option" },
    displaystatusObj:{ value: "", label: "Select an Option" },
    statusObj:{ value: "", label: "Select an Option" },
    uploadTypeObj:{ value: "", label: "Select an Option" }}
    document.title = "Manage Documents :: PID";
  }
  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  formateUploadType = (rowData) => {
    if(rowData.uploadType==="L"){
      return "Letter";
    }else{
      return "Matter";
    }
  };
  statusTemplate = (rowData, column) => {
    return (
      <div>
        {rowData &&
        !isEmpty(rowData) &&
        rowData.status &&
        !isEmpty(rowData.status) &&
        rowData.status === "Y" ? (
          <Badge color="info"> Active </Badge>
        ) : (
          <Badge color="secondary"> InActive </Badge>
        )}
      </div>
    );
  };

  

  componentDidMount = async () => {
    let depNamedata = {
      url: "pid/lovExternalDept",
      showMessage: "",
    };
    const depResponce = await getAPI(depNamedata);
    if (depResponce && !isEmpty(depResponce)) {
      this.setState({
        depnamesList: depResponce.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        depnamesList: [],
      });
    }

    let advertisementdata = {
      url: "pid/lovDoctype",
      showMessage: "",
    };
    const advertisementResponce = await getAPI(advertisementdata);
    if (advertisementResponce && !isEmpty(advertisementResponce)) {
      this.setState({
        advertismentList: advertisementResponce.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        advertismentList: [],
      });
    }


    let newpaperdata = {
      url: "pid/lovNewsPaper",
      showMessage: "",
    };
    const newsPapperResponce = await getAPI(newpaperdata);
    if (newsPapperResponce && !isEmpty(newsPapperResponce)) {
      this.setState({
        newpapersList: newsPapperResponce.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        advertismentList: [],
      });
    }

    let sizedata = {
      url: "pid/lovSize",
      showMessage: "",
    };
    const sizeResponce = await getAPI(sizedata);
    if (sizeResponce && !isEmpty(sizeResponce)) {
      this.setState({
        sizeList: sizeResponce.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        advertismentList: [],
      });
    }
  }

  onFileChange(files) {
   
    this.setState({ file:files});
  }
  setStatus = (e) => {
    this.setState({ status: e.value , statusObj:e});
  };
  setSize=(e)=>{
    this.setState({ lkpSizeId: e.value,sizeObj:e });
  }
  setStationType=(e)=>{
    this.setState({ stationType: e.value,stationtypeObj:e });
  }
  setAddCharges=(e)=>{
    this.setState({ addCharges: e.value,addchargObj:e });
  }
  setDisplayStatus=(e)=>{
    this.setState({ displayStatus: e.value,displaystatusObj:e });
  }
  setDepartmentName=(e)=>{
    this.setState({ tblExternalDepId: e.value });
  }
  setAdvType=(e)=>{
    this.setState({ tblDocTypeId: e.value });
  }
  setNewsPapper = async (e) => {
    this.setState({ tblNewspaperId: e.value, newspaperObj:e });

    let sectiondata = {
      url: "pid/lovNewsPaperStation/"+e.value ,
      showMessage: "",
    };
    const sectionResponce = await getAPI(sectiondata);
    if (sectionResponce && !isEmpty(sectionResponce)) {
      this.setState({
        sectionlist: sectionResponce.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        sectionlist: [],
      });
    }
  };
  setUploadType = (e) => {
    this.setState({ uploadType: e.value , uploadTypeObj:e});
  };

  uploadFiles = (e) => {
    e.preventDefault();
    this.setState({ errors: {} });
    if(!isEmpty(this.state.file)){
      const data={
        uploadType:this.state.uploadType,
        name:this.state.file.file.name,
        status:this.state.status,
        fileBase64:this.state.file.base64,
        file:this.state.file
      }
  
      const { haserror, errors } = uploadFileValidation(data);
      if (haserror) {
        this.setState({ errors });
      } else {
        this.setState({ docFileUpoads: [...this.state.docFileUpoads, data],  statusObj:{ value: "", label: "Select an Option" },
        uploadTypeObj:{ value: "", label: "Select an Option" },file:{}});
      }
    }


   

  }

  saveDocuments =async (e) => {
    e.preventDefault();
    this.setState({ errors: {} });
    const receiveDate_date = moment(this.state.receiveDate, "MM-DD-YYYY", true).format(
      "YYYY-MM-DD"
    );
    const publishDate_date = moment(this.state.publishDate, "MM-DD-YYYY", true).format(
      "YYYY-MM-DD"
    );
    const data ={
    letterNo: this.state.letterNo,
    tblExternalDepId: this.state.tblExternalDepId,
    tblDocTypeId: this.state.tblDocTypeId,
    receiveDate: receiveDate_date,
    adCaption: this.state.adCaption,
    publishDate: publishDate_date,
    newsPapers:this.state.newsPapers,
    docFileUpoads:this.state.docFileUpoads,
    showMessage: "S",
    url: "pid/createLetter",
    }

    const { haserror, errors } = docValidation(data);
    if (haserror) {
      this.setState({ errors });
    } else {
      this.setState({ disableSubmitButton: true });
      const response = await postAPI(data);
      this.userResponse(response);
    }
  
   
  }

  userResponse = (response) => {
    if (response && !isEmpty(response)) {
      this.props.history.push("/document");
    } else this.setState({ loading: false });
  };

  addNewsPapper = (e) => {
    e.preventDefault();
    this.setState({ errors: {} });
    let SectionDescr = "";
    if (this.state.stations) {
     this.state.stations.map((item) => {
        SectionDescr=item.lkpStationDescr+","+SectionDescr;
      });

    }
    const data={
     tblNewspaperId:this.state.tblNewspaperId,
     tblNewspaperDescr:this.state.newspaperObj.label,
     lkpSizeId:this.state.lkpSizeId,
     lkpSizeDescr:this.state.sizeObj.label,
     displayStatus:this.state.displayStatus,
     displayStatusDescr:this.state.displaystatusObj.label,
     stationType:this.state.stationType,
     stationTypeDescr:this.state.stationtypeObj.label,
     addCharges:this.state.addCharges,
     addChargesDescr:this.state.addchargObj.label,
     SectionDescr:SectionDescr,
     stations: this.state.stations
    }
    const { haserror, errors } = newsValidation(data);
    if (haserror) {
      this.setState({ errors });
    } else {
      this.setState({ newsPapers: [...this.state.newsPapers, data], newspaperObj:{ value: "", label: "Select an Option" },
      stationtypeObj:{ value: "", label: "Select an Option" },
      sizeObj:{ value: "", label: "Select an Option" },
      addchargObj:{ value: "", label: "Select an Option" },
      displaystatusObj:{ value: "", label: "Select an Option" } });
    }

   

  }


  showUpdateModel = () => {
    this.setState({ updateModel: !this.state.updateModel, row:this.state.sectionlist });
  };

  getStationLanguage = (item) => {
  
   this.setState({stations:item})
  };


  UNSAFE_componentWillReceiveProps(nextProps) {}

  render() {
    const { errors } = this.state;
    return (
      <Row>
        <AddStation
            modelOpen={this.showUpdateModel}
            stationlist={this.state.row}
            isOpen={this.state.updateModel}
            getStation={this.getStationLanguage}
          />
        <Col>
          <Card>
            <CardHeader>
              <h3>R&I Form</h3>
            </CardHeader>
            <CardBody>
              <div className="content-section implementation">
                <Row>
                  <Col md="6">
                    <FormGroup>
                      <Label>
                        Letter No :<span style={{ color: "red" }}> *</span>
                      </Label>
                      <Input
                        type="text"
                        value={this.state.letterNo}
                        name="letterNo"
                        invalid={!isEmpty(errors.letterNo)}
                        onChange={this.onChangeInput}
                        placeholder="Enter  Letter No "
                      />

                      <p className="error">{errors.letterNo}</p>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        Rec Letter Date :
                        <span style={{ color: "red" }}> *</span>
                      </Label>
                      <br></br>
                      <Calendar
                        yearNavigator={true}
                        yearRange="1980:2030"
                        value={this.state.receiveDate}
                        name="receiveDate"
                        hideOnDateTimeSelect={true}
                        maxDate={new Date()}
                        dateFormat="dd/mm/yy"
                        onChange={this.onChangeInput}
                      />

                      <p className="error">{errors.receiveDate}</p>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        Date Of Publication :
                        <span style={{ color: "red" }}> *</span>
                      </Label>
                      <br></br>
                      <Calendar
                        yearNavigator={true}
                        yearRange="1980:2030"
                        value={this.state.publishDate}
                        name="publishDate"
                        hideOnDateTimeSelect={true}
                        maxDate={new Date()}
                        dateFormat="dd/mm/yy"
                        onChange={this.onChangeInput}
                      />

                      <p className="error">{errors.publishDate}</p>
                    </FormGroup>
                  </Col>
                  <Col md="6">
                    <FormGroup>
                      <Label>
                        External Department Name :
                        <span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                        onChange={this.setDepartmentName}
                        options={this.state.depnamesList}
                      />

                      <p className="error">{errors.tblExternalDepId}</p>
                    </FormGroup>
                  </Col>
                  <Col md="5">
                    <FormGroup>
                      <Label>
                        Contact No :<span style={{ color: "red" }}> *</span>
                      </Label>
                      <Input
                        disabled={true}
                        type="text"
                        value={this.state.empNo}
                        name="empNo"
                        invalid={!isEmpty(errors.empNo)}
                        onChange={this.onChangeInput}
                        placeholder="Enter Contact No "
                      />

                      <p className="error">{errors.empNo}</p>
                    </FormGroup>
                  </Col>
                  <Col md="6">
                    <FormGroup>
                      <Label>
                        Advertisement Type :
                        <span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                        onChange={this.setAdvType}
                        options={this.state.advertismentList}
                      />

                      <p className="error">{errors.tblDocTypeId}</p>
                    </FormGroup>
                  </Col>
                  <Col md="5">
                    <FormGroup>
                      <Label>
                        Advertisement Caption :
                        <span style={{ color: "red" }}> *</span>
                      </Label>
                      <Input
                        type="text"
                        value={this.state.adCaption}
                        name="adCaption"
                        invalid={!isEmpty(errors.adCaption)}
                        onChange={this.onChangeInput}
                        placeholder="Enter  Advertisement Caption "
                      />

                      <p className="error">{errors.adCaption}</p>
                    </FormGroup>
                  </Col>
                </Row>
              </div>
            </CardBody>
          </Card>
          <hr></hr>

          <Card>
            <CardHeader>
              <h3>Add Newspaper</h3>
            </CardHeader>
            <CardBody>
              <div className="content-section implementation">
                <Row>
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        Newspaper :<span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                      value={this.state.newspaperObj}
                        onChange={this.setNewsPapper}
                        options={this.state.newpapersList}
                      />

                      <p className="error">{errors.tblNewspaperId}</p>
                    </FormGroup>
                  </Col>
               
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        Station Type:<span style={{ color: "red" }}> *</span>
                      </Label>
                   
                      <Select
                      value={this.state.stationtypeObj}
                        onChange={this.setStationType}
                        options={this.state.StationTypeList}
                      />

                      <p className="error">{errors.stationType}</p>
                    </FormGroup>
                  </Col>
              
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        Size :<span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                      value={this.state.sizeObj}
                        onChange={this.setSize}
                        options={this.state.sizeList}
                      />

                      <p className="error">{errors.lkpSizeId}</p>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>
                        Additional Charges :<span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                      value={this.state.addchargObj}
                        onChange={this.setAddCharges}
                        options={this.state.addChargesList}
                      />
                      <p className="error">{errors.addCharges}</p>
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label>
                      Status:<span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                      value={this.state.displaystatusObj}
                        onChange={this.setDisplayStatus}
                        options={this.state.displayList}
                      />


                      <p className="error">{errors.displayStatus}</p>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                
                  <Col md="4">
                  <Button
                      icon="pi pi-plus"
                      tooltip="Add"
                      label="Add Station"
                      className="p-button-success p-button-lg"
                      tooltipOptions={{ position: "left" }}
                      onClick={this.showUpdateModel.bind(this)}
                    />
                    <Button
                      icon="pi pi-plus"
                      tooltip="Add"
                      label="Add"
                      style={{ marginLeft: "10px" }}
                      className="p-button-success p-button-lg"
                      tooltipOptions={{ position: "left" }}
                      onClick={this.addNewsPapper}
                    />
                    <Button
                      tooltip="Clear"
                      label="Clear"
                      tooltipOptions={{ position: "right" }}
                      style={{ marginLeft: "10px" }}
                      className="p-button-danger"
                      //onClick={this.clearValuesofLineEntery}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md="12" style={{ marginTop: "15px" }}>
                    <DataTable
                      className="table-bordered"
                      responsive={true}
                      emptyMessage="No records found"
                      value={this.state.newsPapers}
                      // header={header}
                      rowExpansionTemplate={this.rowExpansionTemplate}
                      dataKey="userId"
                      style={{ textAlign: "center" }}
                      paginator={true}
                      rows={10}
                      rowsPerPageOptions={[10, 20, 30]}
                    >
                      <Column
                        sortable={true}
                        filter={true}
                        field="tblNewspaperDescr"
                        header="Newspaper"
                      />
                      <Column
                        sortable={true}
                        filter={true}
                        field="stationTypeDescr"
                        header="Station Type"
                      />
                      <Column
                        sortable={true}
                        filter={true}
                        field="lkpSizeDescr"
                        header="Size"
                      />
                      <Column
                        sortable={true}
                        filter={true}
                        field="addChargesDescr"
                        header="Additional Charges "
                      />
                        <Column
                        sortable={true}
                        filter={true}
                        field="SectionDescr"
                        header="Station "
                      />
                     

                   
                    </DataTable>
                    <p className="error">{errors.newsPapers}</p>
                  </Col>
                </Row>
              </div>
            </CardBody>
          </Card>
          <hr></hr>
          <Card>
            <CardHeader>
              <h3>Upload File</h3>
            </CardHeader>
            <CardBody>
              <div className="content-section implementation">
                <Row>
                  <Col md="4">
                    <FormGroup>
                      <Label>
                        Upload Type :<span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                        onChange={this.setUploadType}
                        options={this.state.uploadtypeList}
                        value={this.state.uploadTypeObj}
                      />

                      <p className="error">{errors.uploadType}</p>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>
                        Status :<span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                        onChange={this.setStatus}
                        options={this.state.statusList}
                        value={this.state.statusObj}
                      />

                      <p className="error">{errors.status}</p>
                    </FormGroup>
                  </Col>
                  <Col md="4" style={{ marginTop: "25px" }}>
                    <FileBase64
                    
                     onDone={this.onFileChange.bind(this)}
                    />
                  </Col>
                </Row>
                <Row>
                  {" "}
                  <Col md="4">
                    <Button
                      icon="pi pi-plus"
                      tooltip="Add"
                      label="Add"
                      className="p-button-success p-button-lg"
                      tooltipOptions={{ position: "left" }}
                      onClick={this.uploadFiles}
                    />
                    <Button
                      tooltip="Clear"
                      label="Clear"
                      tooltipOptions={{ position: "right" }}
                      style={{ marginLeft: "10px" }}
                      className="p-button-danger"
                      //onClick={this.clearValuesofLineEntery}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md="12" style={{ marginTop: "15px" }}>
                    <DataTable
                      className="table-bordered"
                      responsive={true}
                      emptyMessage="No records found"
                      value={this.state.docFileUpoads}
                      // header={header}
                      rowExpansionTemplate={this.rowExpansionTemplate}
                      dataKey="userId"
                      style={{ textAlign: "center" }}
                      paginator={true}
                      rows={10}
                      rowsPerPageOptions={[10, 20, 30]}
                    >
                      <Column
                        sortable={true}
                        filter={true}
                        field="uploadType"
                        body={this.formateUploadType}
                        header="Upload Type"
                      />
                       <Column
                        sortable={true}
                        filter={true}
                        field="name"
                        header="Name"
                      />
                           <Column
                        sortable={true}
                        filter={true}
                        body={this.statusTemplate}
                        field="status"
                        header="Status"
                      />
                     

                      {/* <Column
                body={this.actionTemplate}
                field="actions"
                header="Actions"
              /> */}
                    </DataTable>
                    <p className="error">{errors.docFileUpoads}</p>
                  </Col>
                </Row>
              </div>
            </CardBody>
            <CardFooter> <Button
                      icon="pi pi-plus"
                      tooltip="Save"
                      label="Save Document"
                      className="p-button-success p-button-lg"
                      tooltipOptions={{ position: "left" }}
                      onClick={this.saveDocuments}
                    />
                    <Button
                      tooltip="Clear"
                      label="Clear"
                      tooltipOptions={{ position: "right" }}
                      style={{ marginLeft: "10px" }}
                      className="p-button-danger"
                      //onClick={this.clearValuesofLineEntery}
                    /></CardFooter>
          </Card>
        </Col>
      </Row>
    );
  }
}

AddDocument.propTypes = {
 
};

const mapStateToProps = (state) => ({
  employees: state.users,
  auth: state.auth,
});

export default connect(mapStateToProps, { })(
  withRouter(AddDocument)
);
