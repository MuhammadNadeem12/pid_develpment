import validator from "validator";
import isEmptyCheck from "../../../utils/isEmpty";

const userValidation = (data) => {
  let errors = {};
  const tblNewspaperId = isEmptyCheck(data.tblNewspaperId)
    ? ""
    : data.tblNewspaperId.trim().toString();
    const lkpSizeId = isEmptyCheck(data.lkpSizeId)
    ? ""
    : data.lkpSizeId.trim().toString();
    const displayStatus = isEmptyCheck(data.displayStatus)
    ? ""
    : data.displayStatus.trim().toString();
    const stationType = isEmptyCheck(data.stationType)
    ? ""
    : data.stationType.trim().toString();
    const addCharges = isEmptyCheck(data.addCharges)
    ? ""
    : data.addCharges.trim().toString();
    
  if (validator.isEmpty(tblNewspaperId)) {
    errors.tblNewspaperId = "Newspaper is Required";
  }
  if (validator.isEmpty(lkpSizeId)) {
    errors.lkpSizeId = "Size is Required";
  }
  if (validator.isEmpty(displayStatus)) {
    errors.displayStatus = "Status is Required";
  }
  if (validator.isEmpty(stationType)) {
    errors.stationType = "Station Type is Required";
  }
  if (validator.isEmpty(addCharges)) {
    errors.addCharges = "Additional Charges  is Required";
  }

  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors,
  };
  return feedback;
};
export default userValidation;
