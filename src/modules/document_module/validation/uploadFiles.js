import validator from "validator";
import isEmptyCheck from "../../../utils/isEmpty";

const userValidation = (data) => {
  let errors = {};
  const uploadType = isEmptyCheck(data.uploadType)
    ? ""
    : data.uploadType.trim().toString();
    const status = isEmptyCheck(data.status)
    ? ""
    : data.status.trim().toString();
 ;

    
  if (validator.isEmpty(uploadType)) {
    errors.uploadType = "upload Type is Required";
  }
  if (validator.isEmpty(status)) {
    errors.status = "Status is Required";
  }
  
 
  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors,
  };
  return feedback;
};
export default userValidation;
