import validator from "validator";
import isEmptyCheck from "../../../utils/isEmpty";

const userValidation = (data) => {
  let errors = {};
  const letterNo = isEmptyCheck(data.letterNo)
    ? ""
    : data.letterNo.trim().toString();
    const tblExternalDepId = isEmptyCheck(data.tblExternalDepId)
    ? ""
    : data.tblExternalDepId.trim().toString();
    const tblDocTypeId = isEmptyCheck(data.tblDocTypeId)
    ? ""
    : data.tblDocTypeId.trim().toString();
    const adCaption = isEmptyCheck(data.adCaption)
    ? ""
    : data.adCaption.trim().toString();
    const receiveDate = isEmptyCheck(data.receiveDate.toString()) ? "" : data.receiveDate.toString();
    const publishDate = isEmptyCheck(data.publishDate.toString()) ? "" : data.publishDate.toString();
  if (validator.isEmpty(letterNo)) {
    errors.letterNo = "Letter No is Required";
  }
  if (validator.isEmpty(tblExternalDepId)) {
    errors.tblExternalDepId = "External Department is Required";
  }
  if (validator.isEmpty(tblDocTypeId)) {
    errors.tblDocTypeId = "Advertisement Type is Required";
  }
  if (validator.isEmpty(adCaption)) {
    errors.adCaption = "Advertisement Caption is Required";
  }
  if (!validator.isEmpty(receiveDate) && receiveDate.toString() === "Invalid date") {
    errors.receiveDate = "Received Date is required";
  }
  if (!validator.isEmpty(publishDate) && publishDate.toString() === "Invalid date") {
    errors.publishDate = "Publish Date is required";
  }

  if (isEmptyCheck(data.newsPapers)) {
    errors.newsPapers = "Please Add Lines For Newspapper";
  }
  if (isEmptyCheck(data.docFileUpoads)) {
    errors.docFileUpoads = "Please Add Lines For Upload File";
  }
 

  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors,
  };
  return feedback;
};
export default userValidation;
