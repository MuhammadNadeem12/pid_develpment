import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getEmployes } from "../../redux/user_module/actions";
//import userValidation from "./validation/createUser";
import isEmpty from "../../utils/isEmpty";
//import Select from "react-select";
import Loading from "../../utils/spinner";
import FullScreen from "../../utils/maximize-dialog";
//import { Calendar } from "primereact/calendar";
//import validator from "validator";
//import postAPI from "../../utils/postAPITemplate";
//import getAPI from "../../utils/getAPITemplate";
//import moment from "moment";
import { DataTable } from "primereact/datatable";

import { Column } from "primereact/column";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
//  FormGroup,
//  Label,
 // Input,
 // Badge
} from "reactstrap";
//import { Button as Btn } from "primereact/button";
const initialState = {
errors:{},
lkpStationId:"",
stationObj: { value: "", label: "Select an Option" },
langObj: { value: "", label: "Select an Option" },
lkplanguages:null,
stationList:[],
languagelist:[],
stations:[]
};

class ViewStation extends Component {
  constructor(props) {
   
    super(props);
    this.state = initialState;
  }
  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount = async () => {
  };


  formSubmit = async (e) => {
    e.preventDefault();
   this.props.getStation(this.state.stations);
   this.cancelDialog();
   
  };

  userResponse = (response) => {
 
  };
  cancelDialog = () => {
    this.props.modelOpen();
    this.setState(initialState);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    console.log(nextProps);
   
   if(nextProps && !isEmpty(nextProps.stationlist)){
   this.setState({stationList:nextProps.stationlist})
  }
  
  }

  rowExpansionTemplate = (data) => {
    console.log(data);
    const docs_arr = data.tblDocStationLanguages;
    return (
      <div className="p-grid p-fluid" style={{ padding: "2em 1em 1em 1em" }}>
        <div className="p-col-12 p-md-12">
          <div className="">
            {docs_arr &&
              docs_arr.length > 0 &&
              docs_arr.map((docs, index) => {
                return (
                  <Row key={index}>
                    <Col col="3">
                      <Row>
                        <Col md="4" sm="12">
                          Code:
                        </Col>
                        <Col md="8" sm="12" style={{ fontWeight: "bold" }}>
                          {docs.lkpLanguage.code}
                        </Col>
                      </Row>
                    </Col>
                    <Col col="5">
                      <Row>
                        <Col md="5" sm="12">
                          Description:
                        </Col>
                        <Col md="7" sm="12" style={{ fontWeight: "bold" }}>
                          {docs.lkpLanguage.descr}
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                );
              })}
          </div>
        </div>
      </div>
    );
  };
  render() {
   
   
    const {  loading } = this.state;
    let content = "";
    if (loading) {
      content = <Loading />;
    } else {
      content = (
        <div>
        
          <Row>
          <DataTable
                      className="table-bordered"
                      responsive={true}
                      emptyMessage="No records found"
                      value={this.state.stationList.tblDocumentDetailStations}    
                      dataKey="userId"
                      expandedRows={this.state.expandedRows}
                      onRowToggle={(e) => this.setState({ expandedRows: e.data })}
                      rowExpansionTemplate={this.rowExpansionTemplate}
                      style={{ textAlign: "center" }}
                      paginator={true}
                      rows={10}
                      rowsPerPageOptions={[10, 20, 30]}
                    >
                    <Column expander={true} style={{ width: "3em" }} />
                      <Column
                        sortable={true}
                        filter={true}
                        field="lkpStation.descr"
                        header="Station"
                      />
                      
                       
                     

                    </DataTable>
          </Row>
        </div>
      );
    }

    return (
      <div>
        <Modal
          id="addusermodel"
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader>
           Add Station
            <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("addusermodel")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.cancelDialog}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button
               
                type="submit"
                color="success"
              >
                Add
              </Button>
              <Button color="secondary" onClick={this.cancelDialog}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

ViewStation.propType = {
  errors: PropTypes.object.isRequired, //STORE
  modelOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.object.isRequired,
  getEmployes: PropTypes.func.isRequired, //STORE
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
});

export default connect(mapStateToProps, {
  getEmployes,
})(ViewStation);
