import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getEmployes } from "../../redux/user_module/actions";
//import userValidation from "./validation/createUser";
import isEmpty from "../../utils/isEmpty";
import Select from "react-select";
import Loading from "../../utils/spinner";
import FullScreen from "../../utils/maximize-dialog";
//import { Calendar } from "primereact/calendar";
//import validator from "validator";
//import postAPI from "../../utils/postAPITemplate";
import getAPI from "../../utils/getAPITemplate";
//import moment from "moment";
import { DataTable } from "primereact/datatable";

import { Column } from "primereact/column";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
//  Input,
} from "reactstrap";
import { Button as Btn } from "primereact/button";
const initialState = {
errors:{},
lkpStationId:"",
stationObj: { value: "", label: "Select an Option" },
langObj: { value: "", label: "Select an Option" },
lkplanguages:null,
stationList:[],
languagelist:[],
stations:[]
};

class AddStation extends Component {
  constructor(props) {
   
    super(props);
    this.state = initialState;
  }
  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount = async () => {
  };


  formSubmit = async (e) => {
    e.preventDefault();
   this.props.getStation(this.state.stations);
   this.cancelDialog();
   
  };

  userResponse = (response) => {
 
  };
  cancelDialog = () => {
    this.props.modelOpen();
    this.setState(initialState);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
   
   if(nextProps && !isEmpty(nextProps.stationlist)){
   this.setState({stationList:nextProps.stationlist})
  }
  
  }

  setStation =async (e) => {
    this.setState({ lkpStationId: e.value,stationObj:e});

    let langdata = {
      url: "pid/lovNewsPaperStationLanguage/"+e.value ,
      showMessage: "",
    };
    const langResponce = await getAPI(langdata);
    if (langResponce && !isEmpty(langResponce)) {
      this.setState({
        languagelist: langResponce.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        languagelist: [],
      });
    }
  };

  setLanguage =async (value) => {
   
   this.setState({lkplanguages:value})
  };

  addStationAndLang = (e) => {
    e.preventDefault();
   
    let languages = [];
    let languagesdescr = "";
    if (this.state.lkplanguages) {
      languages = this.state.lkplanguages.map((item) => {
        languagesdescr=item.label+","+languagesdescr;
        return { lkpLanguageId: item.value };
      });

    }
   
    const data={
      languages:languages,
      lkpStationId:this.state.lkpStationId,
      lkpStationDescr:this.state.stationObj.label,
      languagesdescr:languagesdescr

    }
    this.setState({ stations: [...this.state.stations, data] });
    this.setState({ errors: {} ,
      stationObj: { value: "", label: "Select an Option",},lkplanguages:null});
  }
 


  render() {
   
   
    const { errors, loading } = this.state;
    let content = "";
    if (loading) {
      content = <Loading />;
    } else {
      content = (
        <div>
          <Row>
          <Col md="4">
                    <FormGroup>
                      <Label>
                        Station :<span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                        onChange={this.setStation}
                        options={this.state.stationList}
                        value={this.state.stationObj}
                      />

                      <p className="error">{errors.empNo}</p>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                    <FormGroup>
                      <Label>
                        Language :<span style={{ color: "red" }}> *</span>
                      </Label>
                      <Select
                       isMulti={true}
                        onChange={this.setLanguage}
                        options={this.state.languagelist}
                        value={this.state.lkplanguages}
                      
                      />

                      <p className="error">{errors.empNo}</p>
                    </FormGroup>
                  </Col>
                  <Col md="4">
                  <Btn
                  style={{marginTop:"27px"}}
                      icon="pi pi-plus"
                      tooltip="Add"
                      label="Add"
                      className="p-button-success p-button-lg"
                      tooltipOptions={{ position: "left" }}
                      onClick={this.addStationAndLang}
                    />
                  </Col>
          </Row>
          <hr></hr>
          <Row>
          <DataTable
                      className="table-bordered"
                      responsive={true}
                      emptyMessage="No records found"
                      value={this.state.stations}    
                      dataKey="userId"
                      style={{ textAlign: "center" }}
                      paginator={true}
                      rows={10}
                      rowsPerPageOptions={[10, 20, 30]}
                    >
                      <Column
                        sortable={true}
                        filter={true}
                        field="lkpStationDescr"
                        header="Station"
                      />
                       <Column
                        sortable={true}
                        filter={true}
                        field="languagesdescr"
                        header="languagesdescr"
                      />
                       
                     

                    </DataTable>
          </Row>
        </div>
      );
    }

    return (
      <div>
        <Modal
          id="addusermodel"
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader>
           Add Station
            <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("addusermodel")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.cancelDialog}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button
               
                type="submit"
                color="success"
              >
                Add
              </Button>
              <Button color="secondary" onClick={this.cancelDialog}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

AddStation.propType = {
  errors: PropTypes.object.isRequired, //STORE
  modelOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.object.isRequired,
  getEmployes: PropTypes.func.isRequired, //STORE
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
});

export default connect(mapStateToProps, {
  getEmployes,
})(AddStation);
