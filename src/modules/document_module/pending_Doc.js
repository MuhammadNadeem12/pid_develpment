import React, { Component } from "react";
import { connect } from "react-redux";
import validator from "validator";

import {
  Card,
  CardBody,
  Row,
  Col,
  CardHeader,
  //Input,
  //FormGroup,
  //Label,
} from "reactstrap";
import { DataTable } from "primereact/datatable";
import { Button } from "primereact/button";
import { Column } from "primereact/column";
//import EditDetail from "./drEditDetail";
//import roleRight from "../../utils/checkRights";
//import postApi from "../../utils/postAPITemplate";
import getApi from "../../utils/getAPITemplate";
import LoadingOverlay from "react-loading-overlay";
import isEmpty from "../../utils/isEmpty";
//import config from "../../config";
//import { Calendar } from "primereact/calendar";
//import Select from "react-select";
const initialState = {
  pidNo: "",
  letterNo: "",
  adTypeId:'',
  adTypelist:[],
  adType_obj:{},
  newsPaper:"",
  newsPaperlist:[],
  newsPaper_obj:{},
  departmentId:"",
  department_obj:{},
  departmentlist:[],
  TableRecords: [],
  getRecord: {},
  expandedRows: null,
  updateModel: false,
  modelOpen: false,
  globalFilter: null,
  row: {},
  loading: false,
  errors: {},
  showMessage: "S",
};

class PendingDoc extends Component {
  constructor() {
    super();
    this.state = initialState;
    document.title = "Document::PID";
  }
  viewDocs = (item) => {
    console.log(item);
     this.props.history.push("/viewdoc/"+item.documentId);
  };

  componentDidMount = async () => {
    const data = {
         url: "pid/getPendingDocuments"
       }
   
       const response = await getApi(data);
       console.log(response);
       if (response && !isEmpty(response)) {
         this.setState({
           TableRecords: response,
           loading: false,
         });
       } else {
         this.setState({ loading: false });
       }
     
  };

  UNSAFE_componentWillReceiveProps(nextProps) { }

  onChangeInput = (e) => {
    if (e.target && !isEmpty(e.target) && validator.isNumeric(e.target.value)) {
      this.setState({ [e.target.name]: e.target.value });
    } else if (e.target.value.length === 0) {
      this.setState({ [e.target.name]: "" });
    }
  };
  actionTemplate = (rowData, column) => {
    return (
      <div style={{ textAlign: "center" }}>
        <Button
          type="button"
          icon="pi pi-eye"
          className="p-button-info"
          tooltip="Document Detail"
          tooltipOptions={{ position: "left" }}
          onClick={this.viewDocs.bind(this, rowData)}
        ></Button>
      </div>
    );
  };

  

  render() {
    let content = "";
    //if (this.state.TableRecords && !isEmpty(this.state.TableRecords)) {
      content = (
        <div>
          <DataTable
            className="table-bordered"
            responsive={true}
            emptyMessage="No records found"
            value={this.state.TableRecords}
            dataKey="transHeadId"
            style={{ textAlign: "center" }}
            paginator={true}
            rows={10}
            rowsPerPageOptions={[10, 20, 30]}
          >
            <Column
              field="pidNo"
              header="PID No."
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="letterNo"
              header="Letter No."
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            <Column
              field="tblExternalDep.descr"
              header="Department Name"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="tblDocType.descr"
              header="Ad Type"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />

            <Column
              field="receiveDate"
              header="Receive Date"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
             <Column
              field="tblDocStatus.descr"
              header="Status"
              filter={true}
              filterMatchMode="contains"
              sortable={true}
            />
            <Column
              body={this.actionTemplate}
              field="actions"
              header="Actions"
            />
          </DataTable>
        </div>
      );
   // }

    return (
      <Row>
    
        <Col>
          <Card>
            <CardHeader>
              <h5>Document</h5>
            </CardHeader>
            <CardBody>
              <LoadingOverlay
                active={this.state.loading}
                spinner
                text="Please wait..."
              >
                <div className="content-section implementation">{content}</div>
              </LoadingOverlay>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

PendingDoc.propTypes = {};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {})(PendingDoc);
