import validator from "validator";
import isEmptyCheck from "../../../utils/isEmpty";

const userValidation = (data) => {
  let errors = {};
  const fileNo = isEmptyCheck(data.fileNo) ? "" : data.fileNo.trim().toString();
  const empNo = isEmptyCheck(data.empNo) ? "" : data.empNo.toString();
  const lkpEmpTypeId = isEmptyCheck(data.lkpEmpTypeId)
    ? ""
    : data.lkpEmpTypeId.toString();
  const staffOfficer = isEmptyCheck(data.staffOfficer)
    ? ""
    : data.staffOfficer.toString();
  const cnic = isEmptyCheck(data.cnic) ? "" : data.cnic.toString();
  const dob = isEmptyCheck(data.dob.toString()) ? "" : data.dob.toString();
  const dateOfEntry = isEmptyCheck(data.dateOfEntry.toString())
    ? ""
    : data.dateOfEntry.toString();
  const dateOfRetirement = isEmptyCheck(data.dateOfRetirement.toString())
    ? ""
    : data.dateOfRetirement.toString();
  const status = isEmptyCheck(data.status) ? "" : data.status.toString();
  const lkpWingId = isEmptyCheck(data.lkpWingId)
    ? ""
    : data.lkpWingId.toString();

  const roleId = isEmptyCheck(data.roleId) ? "" : data.roleId.toString();
  const domicile = isEmptyCheck(data.domicile) ? "" : data.domicile.toString();
  const maritalStatus = isEmptyCheck(data.maritalStatus)
    ? ""
    : data.maritalStatus.toString();
  const firstName = isEmptyCheck(data.firstName)
    ? ""
    : data.firstName.toString();
  const lastName = isEmptyCheck(data.lastName) ? "" : data.lastName.toString();
  const gender = isEmptyCheck(data.gender) ? "" : data.gender.toString();

  const lkpSection = isEmptyCheck(data.lkpSection)
    ? ""
    : data.lkpSection.toString();

  const lkpPositionId = isEmptyCheck(data.lkpPositionId)
    ? ""
    : data.lkpPositionId.toString();

  const lkpJobId = isEmptyCheck(data.lkpJobId) ? "" : data.lkpJobId.toString();
  const lkpGradeId = isEmptyCheck(data.lkpGradeId)
    ? ""
    : data.lkpGradeId.toString();

  const createUser = isEmptyCheck(data.createUser)
    ? ""
    : data.createUser.toString();

  if (validator.isEmpty(gender)) {
    errors.gender = "Gender is required";
  }
  if (validator.isEmpty(lkpJobId)) {
    errors.lkpJobId = "Job is required";
  }
  if (validator.isEmpty(lkpSection)) {
    errors.lkpSection = "Cell/Section is required";
  }
  if (validator.isEmpty(lkpPositionId)) {
    errors.lkpPositionId = "Position  is required";
  }
  if (validator.isEmpty(lkpGradeId)) {
    errors.lkpGradeId = "Grade  is required";
  }
  if (validator.isEmpty(fileNo)) {
    errors.fileNo = "File No is required";
  }
  if (validator.isEmpty(empNo)) {
    errors.empNo = "Employee No is required";
  }

  if (validator.isEmpty(staffOfficer)) {
    errors.staffOfficer = "Staff/Officer is required";
  }
  if (validator.isEmpty(cnic)) {
    errors.cnic = "CNIC is required";
  }

  if (validator.isEmpty(lkpEmpTypeId)) {
    errors.lkpEmpTypeId = "File Type is required";
  }

  if (!validator.isEmpty(dob) && dob.toString() === "Invalid date") {
    errors.dob = "Date Of Birth is required";
  }
  if (
    !validator.isEmpty(dateOfEntry) &&
    dateOfEntry.toString() === "Invalid date"
  ) {
    errors.dateOfEntry = "Date OF Entry is required";
  }
  if (
    !validator.isEmpty(dateOfRetirement) &&
    dateOfRetirement.toString() === "Invalid date"
  ) {
    errors.dateOfRetirement = "Date Of Retirement is required";
  }
  if (validator.isEmpty(status)) {
    errors.status = "Status Is required";
  }
  if (validator.isEmpty(createUser)) {
    errors.createUser = "Create User Is required";
  }
  if (validator.isEmpty(lkpWingId)) {
    errors.lkpWingId = "lkpWingId  is required";
  }

  if (validator.isEmpty(roleId)) {
    errors.roleId = "Please Select Role";
  }

  if (validator.isEmpty(domicile)) {
    errors.domicile = "Domicile is required";
  }
  if (validator.isEmpty(maritalStatus)) {
    errors.maritalStatus = "Marital Status is required";
  }
  if (validator.isEmpty(firstName)) {
    errors.firstName = "First Name is required";
  }
  if (validator.isEmpty(lastName)) {
    errors.lastName = "Last Name is required";
  }

  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors,
  };
  return feedback;
};
export default userValidation;
