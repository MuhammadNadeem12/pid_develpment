import validator from "validator";
import isEmptyCheck from "../../../utils/isEmpty";

const updateUserValidation = (data) => {
  let errors = {};
  const roleId = isEmptyCheck(data.roleId) ? "" : data.roleId.toString();
  const department = isEmptyCheck(data.department)
    ? ""
    : data.department.toString();
  const cityId = isEmptyCheck(data.cityId) ? "" : data.cityId.toString();
  const countryId = isEmptyCheck(data.countryId)
    ? ""
    : data.countryId.toString();
  const email = isEmptyCheck(data.email) ? "" : data.email.toString();
  const mobileNo = isEmptyCheck(data.mobileNo) ? "" : data.mobileNo.toString();
  const phoneExt = isEmptyCheck(data.phoneExt) ? "" : data.phoneExt.toString();
  const designation = isEmptyCheck(data.designation)
    ? ""
    : data.designation.toString();
  const status = isEmptyCheck(data.status) ? "" : data.status.toString();

  if (validator.isEmpty(roleId)) {
    errors.roleId = "Role is required";
  }
  if (validator.isEmpty(status)) {
    errors.status = "Status is required";
  }
  if (validator.isEmpty(cityId)) {
    errors.cityId = "City is required";
  }
  if (validator.isEmpty(countryId)) {
    errors.countryId = "Country is required";
  }
  if (validator.isEmpty(designation)) {
    errors.designation = "Designation is required";
  }
  if (validator.isEmpty(department)) {
    errors.department = "department is required";
  }
  if (validator.isEmpty(mobileNo)) {
    errors.mobileNo = "Mobile No is required";
  }
  if (validator.isEmpty(email)) {
    errors.email = "Email No is required";
  }
  if (validator.isEmpty(phoneExt)) {
    errors.phoneExt = "Phone Ext No is required";
  }

  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors,
  };
  return feedback;
};
export default updateUserValidation;
