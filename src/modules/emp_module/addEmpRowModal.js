import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getEmployes } from "../../redux/user_module/actions";
import FileBase64 from "react-file-base64";
import isEmpty from "../../utils/isEmpty";
import Select from "react-select";
import Loading from "../../utils/spinner";
import FullScreen from "../../utils/maximize-dialog";
import { Calendar } from "primereact/calendar";
import validator from "validator";
import postAPI from "../../utils/postAPITemplate";
import getAPI from "../../utils/getAPITemplate";
import moment from "moment";
import profileAvatar from "../../assets/Profileavatar.jpg";
import EmpValidation from "./validation/createUser";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

const initialState = {
  status: "",
  statusList: [
    { value: "", label: "Select an Option" },
    { value: "A", label: "Activate" },
    { value: "I", label: "De-activate" },
  ],
  createduserList: [
    { value: "", label: "Select an Option" },
    { value: "Y", label: "YES" },
    { value: "N", label: "NO" },
  ],
  staffList: [
    { value: "", label: "Select an Option" },
    { value: "S", label: "Staff" },
    { value: "O", label: "Officer" },
  ],
  MaritalList: [
    { value: "", label: "Select an Option" },
    { value: "S", label: "Single" },
    { value: "M", label: "Married" },
    { value: "W", label: "Widow" },
    { value: "D", label: "Divorced" },
  ],
  genderList: [
    { value: "", label: "Select an Option" },
    { value: "M", label: "Male" },
    { value: "F", label: "Female" },
  ],
  domicelList: [
    { value: "", label: "Select an Option" },
    { value: "Punjab", label: "Punjab" },
    { value: "Gilgit-Baltistan", label: "Gilgit-Baltistan" },
    {
      value: "Islamabad",
      label: "Islamabad Capital Territory",
    },
    { value: "KPK", label: "Khyber Pakhtunkhwa" },
    { value: "Sindh", label: "Sindh" },
    { value: "Balochistan", label: "Balochistan" },
  ],
  empNo: "",
  cellNo: "",
  cnic: "",
  dateOfEntry: "",
  dateOfRetirement: "",
  dob: "",
  domicile: "",
  fatherspousename: "",
  maritalStatus: "",
  lkpEmpTypeId: "",
  lkpWingId: "",
  lkpSection: "",
  createUser: "",
  roleId: "",
  firstName: "",
  middleName: "",
  lastName: "",
  fileNo: "",
  lkpJobId: "",
  lkpPositionId: "",
  lkpGradeId: "",
  staffOfficer: "",
  errors: {},
  empTypeList: [],
  wingList: [],
  secList: [],
  jobList: [],
  roleList: [],
  gradeList: [],
  posList: [],
  camerapicture: profileAvatar,
  disableRole: true,
  departmentList:[],
  departmentId:"",
};

class AddUserRowModal extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }
  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount = async () => {
    let emptypedata = {
      url: "pid/lovEmpType",
      showMessage: "",
    };
    const empResponse = await getAPI(emptypedata);
    if (empResponse && !isEmpty(empResponse)) {
      this.setState({
        empTypeList: empResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        empTypeList: [],
      });
    }

    let wingdata = {
      url: "pid/lovWing",
      showMessage: "",
    };
    const wingResponse = await getAPI(wingdata);
    if (wingResponse && !isEmpty(wingResponse)) {
      this.setState({
        wingList: wingResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        wingList: [],
      });
    }

    let jobdata = {
      url: "pid/lovJob",
      showMessage: "",
    };
    const jobResponse = await getAPI(jobdata);
    if (jobResponse && !isEmpty(jobResponse)) {
      this.setState({
        jobList: jobResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        jobList: [],
      });
    }

    let gradedata = {
      url: "pid/lovGrade",
      showMessage: "",
    };
    const gradeResponse = await getAPI(gradedata);
    if (gradeResponse && !isEmpty(gradeResponse)) {
      this.setState({
        gradeList: gradeResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        gradeList: [],
      });
    }
    let sectiondata = {
      url: "pid/lovSection",
      showMessage: "",
    };
    const secResponse = await getAPI(sectiondata);
    if (secResponse && !isEmpty(secResponse)) {
      this.setState({
        secList: secResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        secList: [],
      });
    }
    let positiondata = {
      url: "pid/lovPosition",
      showMessage: "",
    };
    const positionResponse = await getAPI(positiondata);
    if (positionResponse && !isEmpty(positionResponse)) {
      this.setState({
        posList: positionResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    } else {
      this.setState({
        posList: [],
      });
    }
    this.getDepartmentList();
  };
  onChangeInputNumber = (e) => {
    if (e.target && !isEmpty(e.target) && validator.isNumeric(e.target.value)) {
      this.setState({ [e.target.name]: e.target.value });
    } else if (e.target.value.length === 0) {
      this.setState({ [e.target.name]: "" });
    }
  };

  formSubmit = async (e) => {
    e.preventDefault();
    this.setState({ errors: {} });
    const dob_date = moment(this.state.dob, "MM-DD-YYYY", true).format(
      "YYYY-MM-DD HH:mm:ss"
    );
    const retirement_date = moment(
      this.state.dateOfRetirement,
      "MM-DD-YYYY",
      true
    ).format("YYYY-MM-DD HH:mm:ss");
    const entry_date = moment(
      this.state.dateOfEntry,
      "MM-DD-YYYY",
      true
    ).format("YYYY-MM-DD HH:mm:ss");
    const data = {
      emppicture: this.state.camerapicture,
      empNo: this.state.empNo,
      cellNo: this.state.cellNo,
      cnic: this.state.cnic,
      dateOfEntry: entry_date,
      dateOfRetirement: retirement_date,
      dob: dob_date,
      domicile: this.state.domicile,
      fatherspousename: this.state.fatherspousename,
      maritalStatus: this.state.maritalStatus,
      status: this.state.status,
      lkpEmpTypeId: this.state.lkpEmpTypeId,
      lkpWingId: this.state.lkpWingId,
      lkpSection: this.state.lkpSection,
      createUser: this.state.createUser,
      roleId: this.state.roleId,
      firstName: this.state.firstName,
      middleName: this.state.middleName,
      lastName: this.state.lastName,
      fileNo: this.state.fileNo,
      lkpJobId: this.state.lkpJobId,
      lkpPositionId: this.state.lkpPositionId,
      lkpGradeId: this.state.lkpGradeId,
      staffOfficer: this.state.staffOfficer,
      gender: this.state.gender,
      lkpDepartmentId:this.state.departmentId,

      url: "pid/createEmployee",
      showMessage: "S",
    };

    const { haserror, errors } = EmpValidation(data);
    if (haserror) {
      console.log("test" + errors);
      this.setState({ errors });
    } else {
      this.setState({ disableSubmitButton: true });
      const response = await postAPI(data);
      this.userResponse(response);
    }
  };

  userResponse = (response) => {
    if (response && !isEmpty(response)) {
      this.props.getEmployes();
      // this.setState({ errors: {} });
      this.setState(initialState);
      this.props.modelOpen();
    } else this.setState({ loading: false });
  };
  cancelDialog = () => {
    this.props.modelOpen();
    this.setState(initialState);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {}

  setRoleValue = (e) => {
    this.setState({ roleId: e.value });
  };
  setstaffValue = (e) => {
    this.setState({ staffOfficer: e.value });
  };
  setgradeValue = (e) => {
    this.setState({ lkpGradeId: e.value });
  };
  setsectionValue = (e) => {
    this.setState({ lkpSection: e.value });
  };
  empTypeValue = (e) => {
    this.setState({ lkpEmpTypeId: e.value });
  };
  setstatusValue = (e) => {
    this.setState({ status: e.value });
  };
  setUserValue = async (e) => {
    this.setState({ createUser: e.value });
    if (e.value === "Y") {
      this.setState({ disableRole: false });
      let roledata = {
        url: "pid/lovRole",
        showMessage: "",
      };
      const roleResponse = await getAPI(roledata);
      if (roleResponse && !isEmpty(roleResponse)) {
        console.log(roleResponse);
        this.setState({
          roleList: roleResponse.map((item) => {
            return {
              value: item.code,
              label: item.descr,
            };
          }),
        });
      } else {
        this.setState({
          roleList: [],
        });
      }
    } else {
      this.setState({
        roleList: [],
        roleId: "",
        disableRole: true,
      });
    }
  };
  setgenderValue = (e) => {
    this.setState({ gender: e.value });
  };
  setdomicelValue = (e) => {
    this.setState({ domicile: e.value });
  };
  setmaritalValue = (e) => {
    this.setState({ maritalStatus: e.value });
  };
  setpostionValue = (e) => {
    this.setState({ lkpPositionId: e.value });
  };
  setDepartmentvalue = (e) => {
    this.setState({ departmentId: e.value });
  };
  setWingValue = async (e) => {
    this.setState({ lkpWingId: e.value });
  };

  setJobValue = async (e) => {
    this.setState({ lkpJobId: e.value });};

  onFileChange(files) {
    this.setState({ camerapicture: files.base64 });
  }
  getDepartmentList = async()=>{
    let sectiondata = {
      url: "pid/lovDepartment",
      showMessage: "",
    };
    const secResponse = await getAPI(sectiondata);
    if (secResponse && !isEmpty(secResponse)) {
      this.setState({
        departmentList: secResponse.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
  }
}

  render() {
    const { errors, loading } = this.state;
    let content = "";
    if (loading) {
      content = <Loading />;
    } else {
      content = (
        <div>
          <Row>
            <Col md="3">
              <img
                src={this.state.camerapicture}
                alt={this.state.camerapicture}
                style={{ width: "100%" }}
                className="shadow-sm"
              />
              <p className="error">{errors.picture}</p>
            </Col>
            <Col md="2" className="p-0 m-0">
              <FileBase64 onDone={this.onFileChange.bind(this)} />
            </Col>
          </Row>

          <Row>
            <Col md="3">
              <FormGroup>
                <Label>
                  Employee ID :<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="text"
                  value={this.state.empNo}
                  name="empNo"
                  invalid={!isEmpty(errors.empNo)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Employee ID"
                />

                <p className="error">{errors.empNo}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>
                  File NO<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="text"
                  value={this.state.fileNo}
                  name="fileNo"
                  invalid={!isEmpty(errors.fileNo)}
                  onChange={this.onChangeInput}
                  placeholder="Enter User ID"
                />
                <p className="error">{errors.fileNo}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>
                  File Type<span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  onChange={this.empTypeValue}
                  options={this.state.empTypeList}
                />
                <p className="error">{errors.lkpEmpTypeId}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>
                  Department <span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  onChange={this.setDepartmentvalue}
                  options={this.state.departmentList}
                />
                <p className="error">{errors.lkpDepartmentId}</p>
              </FormGroup>
            </Col>
           
            <Col md="3">
              <FormGroup>
                <Label>
                  Staff/Officer <span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  onChange={this.setstaffValue}
                  options={this.state.staffList}
                />
                <p className="error">{errors.staffOfficer}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>
                  First Name<span style={{ color: "red" }}> *</span>
                </Label>

                <Input
                  type="text"
                  value={this.state.firstName}
                  name="firstName"
                  invalid={!isEmpty(errors.firstName)}
                  onChange={this.onChangeInput}
                  placeholder="Enter First Name"
                />
                <p className="error">{errors.firstName}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Middle Name</Label>

                <Input
                  type="text"
                  value={this.state.middleName}
                  name="middleName"
                  invalid={!isEmpty(errors.middleName)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Middle Name"
                />
                <p className="error">{errors.middleName}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>
                  Last Name<span style={{ color: "red" }}> *</span>
                </Label>

                <Input
                  type="text"
                  value={this.state.lastName}
                  name="lastName"
                  invalid={!isEmpty(errors.lastName)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Last Name"
                />
                <p className="error">{errors.lastName}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>
                  CNIC<span style={{ color: "red" }}> *</span>
                </Label>

                <Input
                  minLength="13"
                  maxLength="13"
                  type="text"
                  value={this.state.cnic}
                  name="cnic"
                  invalid={!isEmpty(errors.cnic)}
                  onChange={this.onChangeInputNumber}
                  placeholder="Enter CNIC"
                />
                <p className="error">{errors.cnic}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Father/Husband Name</Label>

                <Input
                  type="text"
                  value={this.state.fatherspousename}
                  name="fatherspousename"
                  invalid={!isEmpty(errors.fatherspousename)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Last Name"
                />
                <p className="error">{errors.fatherspousename}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>
                  Date of Birth:<span style={{ color: "red" }}> *</span>
                </Label>
                <br />
                <Calendar
                  yearNavigator={true}
                  yearRange="1980:2030"
                  value={this.state.dob}
                  name="dob"
                  hideOnDateTimeSelect={true}
                  maxDate={new Date()}
                  dateFormat="dd/mm/yy"
                  onChange={this.onChangeInput}
                />

                <p className="error">{errors.dob}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>
                  Marital Status<span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  onChange={this.setmaritalValue}
                  options={this.state.MaritalList}
                />

                <p className="error">{errors.maritalStatus}</p>
              </FormGroup>
            </Col>

            <Col md="3">
              <FormGroup>
                <Label>
                  Gender<span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  onChange={this.setgenderValue}
                  options={this.state.genderList}
                />

                <p className="error">{errors.gender}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Cell NO:</Label>
                <Input
                  type="text"
                  maxLength="11"
                  minLength="11"
                  value={this.state.cellNo}
                  name="cellNo"
                  invalid={!isEmpty(errors.cellNo)}
                  onChange={this.onChangeInputNumber}
                  placeholder="03005885165"
                />
                <p className="error">{errors.cellNo}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>
                  Domicile<span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  onChange={this.setdomicelValue}
                  options={this.state.domicelList}
                />

                <p className="error">{errors.domicile}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Wing</Label>
                <span style={{ color: "red" }}> *</span>
                <Select
                  onChange={this.setWingValue}
                  options={this.state.wingList}
                />
                <p className="error">{errors.lkpWingId}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Cell/Section</Label>
                <span style={{ color: "red" }}> *</span>
                <Select
                  onChange={this.setsectionValue}
                  options={this.state.secList}
                />
                <p className="error">{errors.lkpSection}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Job</Label>
                <span style={{ color: "red" }}> *</span>
                <Select
                  onChange={this.setJobValue}
                  options={this.state.jobList}
                />
                <p className="error">{errors.lkpJobId}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Position</Label>
                <span style={{ color: "red" }}> *</span>
                <Select
                  onChange={this.setpostionValue}
                  options={this.state.posList}
                />
                <p className="error">{errors.lkpPositionId}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Grade</Label>
                <span style={{ color: "red" }}> *</span>
                <Select
                  onChange={this.setgradeValue}
                  options={this.state.gradeList}
                />
                <p className="error">{errors.lkpGradeId}</p>
              </FormGroup>
            </Col>

            <Col md="3">
              <FormGroup>
                <Label>
                  Date Of Entry:<span style={{ color: "red" }}> *</span>
                </Label>
                <br />
                <Calendar
                  yearNavigator={true}
                  yearRange="1980:2030"
                  value={this.state.dateOfEntry}
                  name="dateOfEntry"
                  hideOnDateTimeSelect={true}
                  maxDate={new Date()}
                  dateFormat="dd/mm/yy"
                  onChange={this.onChangeInput}
                />

                <p className="error">{errors.dateOfEntry}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>
                  Date Of Retirement:<span style={{ color: "red" }}> *</span>
                </Label>
                <br />
                <Calendar
                  yearNavigator={true}
                  yearRange="1980:2030"
                  value={this.state.dateOfRetirement}
                  name="dateOfRetirement"
                  hideOnDateTimeSelect={true}
                  dateFormat="dd/mm/yy"
                  onChange={this.onChangeInput}
                />

                <p className="error">{errors.dateOfRetirement}</p>
              </FormGroup>
            </Col>
            {/* <Col md="3">
              <FormGroup>
                <Label>
                  Date Of Transfer:<span style={{ color: "red" }}> *</span>
                </Label>
                <br />
                <Calendar
                  yearNavigator={true}
                  yearRange="1980:2030"
                  value={this.state.dateOfRetirement}
                  name="dateOfRetirement"
                  hideOnDateTimeSelect={true}
                  dateFormat="dd/mm/yy"
                  onChange={this.onChangeInput}
                />

                <p className="error">{errors.dateOfRetirement}</p>
              </FormGroup>
            </Col> */}
            <Col md="3">
              <FormGroup>
                <Label>
                  Status<span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  onChange={this.setstatusValue}
                  options={this.state.statusList}
                />
                <p className="error">{errors.status}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>
                  Created User<span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  onChange={this.setUserValue}
                  options={this.state.createduserList}
                />
                <p className="error">{errors.createUser}</p>
              </FormGroup>
            </Col>
            <Col md="3">
              <FormGroup>
                <Label>Role</Label>
                <Select
                  isDisabled={this.state.disableRole}
                  onChange={this.setRoleValue}
                  options={this.state.roleList}
                />
                <p className="error">{errors.roleId}</p>
              </FormGroup>
            </Col>
          </Row>
        </div>
      );
    }

    return (
      <div>
        <Modal
          id="addusermodel"
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader>
            Register Employee
            <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("addusermodel")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.cancelDialog}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button
               // disabled={this.state.disableSubmitButton}
                type="submit"
                color="success"
              >
                Register
              </Button>
              <Button color="secondary" onClick={this.cancelDialog}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

AddUserRowModal.propType = {
  errors: PropTypes.object.isRequired, //STORE
  modelOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.object.isRequired,
  getEmployes: PropTypes.func.isRequired, //STORE
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
});

export default connect(mapStateToProps, {
  getEmployes,
})(AddUserRowModal);
