import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getRoleLov, getCityLov, getCountryLov } from "../../redux/lov/actions";
import { updateUser } from "../../redux/user_module/actions";
import userUpdateValidation from "./validation/updateUser";
import isEmpty from "../../utils/isEmpty";
import Select from "react-select";
import Loading from "../../utils/spinner";
import FullScreen from "../../utils/maximize-dialog";
import validator from "validator";
import moment from "moment";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

const initialState = {
  status: "",
  statusList: [
    { value: "", label: "Select an Option" },
    { value: "A", label: "Activate" },
    { value: "I", label: "De-activate" },
  ],
  statusObject: {},
  roleid: null,
  cityId: "",
  countryId: "",
  errors: {},
  role: [],
  roleObject: {},
  email: "",
  userid: "",
  employeeName: "",
  cnic: "",
  dob: "",
  motherName: "",
  empNo: "",
  department: "",
  phone_ext: "",
  mobileNo: "",
  designation: "",
  cityList: [],
  cityObject: {},
  countryList: [],
  countryObject: {},
  glLimitClass: "",
  glLimitClassList: [],
  disable: true,
  username: "",
  disableSubmitButton: false,
};

class AddUserRowModal extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }
  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    this.props.getRoleLov();
    this.props.getCountryLov();
  }
  onChangeInputNumber = (e) => {
    if (e.target && !isEmpty(e.target) && validator.isNumeric(e.target.value)) {
      this.setState({ [e.target.name]: e.target.value });
    } else if (e.target.value.length === 0) {
      this.setState({ [e.target.name]: "" });
    }
  };
  formSubmit = (e) => {
    e.preventDefault();
    this.setState({ errors: {} });
    console.log(this.state);
    const data = {
      userId: this.state.userid,
      department:
        this.state.department === null
          ? ""
          : this.state.department.toUpperCase(),
      roleId: this.state.roleObject.value,
      cityId: this.state.cityObject.value,
      countryId: this.state.countryObject.value,
      email: this.state.email,
      mobileNo: this.state.mobileNo,
      phoneExt: this.state.phone_ext,
      designation:
        this.state.designation === null
          ? ""
          : this.state.designation.toUpperCase(),
      status: this.state.statusObject.value,
    };

    const { haserror, errors } = userUpdateValidation(data);
    if (haserror) {
      this.setState({ errors });
    } else {
      this.setState({ disableSubmitButton: true });
      this.props.updateUser(data);
      this.setState({ errors: {} });
      this.setState(initialState);
      this.props.modelOpen();
    }
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.lov && nextProps.lov.role_lov) {
      this.setState({
        role: nextProps.lov.role_lov.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    }
    if (nextProps && nextProps.defaultValue) {
      if (!isEmpty(nextProps.defaultValue.cnic)) {
        this.setState({ cnic: nextProps.defaultValue.cnic });
      } else {
        this.setState({ cnic: "" });
      }
      if (!isEmpty(nextProps.defaultValue.userId)) {
        this.setState({ userid: nextProps.defaultValue.userId });
      } else {
        this.setState({ userid: "" });
      }
      if (
        !isEmpty(nextProps.defaultValue.department) &&
        nextProps.defaultValue.department !== null
      ) {
        this.setState({ department: nextProps.defaultValue.department });
      } else {
        this.setState({ department: nextProps.defaultValue.department });
      }
      if (!isEmpty(nextProps.defaultValue.designation)) {
        this.setState({ designation: nextProps.defaultValue.designation });
      } else {
        this.setState({ designation: "" });
      }
      if (!isEmpty(nextProps.defaultValue.email)) {
        this.setState({ email: nextProps.defaultValue.email });
      } else {
        this.setState({ email: "" });
      }
      if (!isEmpty(nextProps.defaultValue.employeeName)) {
        this.setState({ employeeName: nextProps.defaultValue.employeeName });
      } else {
        this.setState({ employeeName: nextProps.defaultValue.employeeName });
      }
      if (!isEmpty(nextProps.defaultValue.employeeNo)) {
        this.setState({ empNo: nextProps.defaultValue.employeeNo });
      } else {
        this.setState({ empNo: "" });
      }
      if (!isEmpty(nextProps.defaultValue.mobileNo)) {
        this.setState({ mobileNo: nextProps.defaultValue.mobileNo });
      } else {
        this.setState({ mobileNo: "" });
      }

      if (!isEmpty(nextProps.defaultValue.dob)) {
        const dob_user = moment(nextProps.defaultValue.dob).format(
          "YYYY-MM-DD"
        );
        this.setState({ dob: dob_user });
      } else {
        this.setState({ dob: "" });
      }

      if (!isEmpty(nextProps.defaultValue.motherName)) {
        this.setState({ motherName: nextProps.defaultValue.motherName });
      } else {
        this.setState({ motherName: "" });
      }
      if (!isEmpty(nextProps.defaultValue.phoneExt)) {
        this.setState({ phoneExt: nextProps.defaultValue.phoneExt });
      } else {
        this.setState({ phoneExt: nextProps.defaultValue.phoneExt });
      }
      if (!isEmpty(nextProps.defaultValue.username)) {
        this.setState({ username: nextProps.defaultValue.username });
      } else {
        this.setState({ username: nextProps.defaultValue.username });
      }
      if (nextProps.defaultValue.phoneExt) {
        this.setState({ phone_ext: nextProps.defaultValue.phoneExt });
      }
      if (nextProps.defaultValue.tblRole) {
        let selectedrole = {
          label: nextProps.defaultValue.tblRole.roleDescr,
          value: nextProps.defaultValue.tblRole.roleId,
        };
        this.setState({ roleObject: selectedrole });
      }
      if (nextProps.defaultValue.status) {
        if (nextProps.defaultValue.status === "A") {
          let selectedrole = {
            label: "Active",
            value: nextProps.defaultValue.status,
          };
          this.setState({ statusObject: selectedrole });
        } else {
          let selectedrole = {
            label: "In-Active",
            value: nextProps.defaultValue.status,
          };
          this.setState({ statusObject: selectedrole });
        }
      }
      if (nextProps.defaultValue.tblRole) {
        let selectedrole = {
          label: nextProps.defaultValue.tblRole.roleDescr,
          value: nextProps.defaultValue.tblRole.roleId,
        };
        this.setState({ roleObject: selectedrole });
      }
      if (nextProps.defaultValue.lkpCity) {
        let selectedcity = {
          label: nextProps.defaultValue.lkpCity.cityDescr,
          value: nextProps.defaultValue.lkpCity.cityId,
        };
        this.setState({ cityObject: selectedcity });
      }
      if (nextProps.defaultValue.lkpCountry) {
        let selectedcountry = {
          label: nextProps.defaultValue.lkpCountry.countryDescr,
          value: nextProps.defaultValue.lkpCountry.countryId,
        };
        this.setState({ countryObject: selectedcountry });
      }
      if (nextProps && nextProps.lov && nextProps.lov.city_lov) {
        this.setState({
          cityList: nextProps.lov.city_lov.map((item) => {
            return {
              value: item.code,
              label: item.descr,
            };
          }),
        });
      }
      if (nextProps && nextProps.lov && nextProps.lov.country_lov) {
        this.setState({
          countryList: nextProps.lov.country_lov.map((item) => {
            return {
              value: item.code,
              label: item.descr,
            };
          }),
        });
      }
    }
  }

  setRoleValue = (e) => {
    this.setState({ roleid: e.value, roleObject: e });
  };
  setcityValue = (e) => {
    this.setState({ cityId: e.value, cityObject: e });
  };
  setcountryValue = (e) => {
    this.props.getCityLov(e.value);
    this.setState({ countryId: e.value, countryObject: e });
  };
  setglLimitClassValue = (e) => {
    this.setState({ glLimitClass: e.value });
  };
  setstatusValue = (e) => {
    this.setState({ status: e.value, statusObject: e });
  };
  render() {
    const { errors, loading } = this.state;
    let content = "";
    if (loading) {
      content = <Loading />;
    } else {
      content = (
        <div>
          <Row>
            <Col md="6">
              <FormGroup>
                <Label>
                  Registered User as :<span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  value={this.state.roleObject}
                  onChange={this.setRoleValue}
                  options={this.state.role}
                />
                <p className="error">{errors.roleId}</p>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md="4">
              <FormGroup>
                <Label>
                  User ID<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  disabled={this.state.disable}
                  type="text"
                  value={this.state.username}
                  name="userid"
                  invalid={!isEmpty(errors.username)}
                  onChange={this.onChangeInput}
                />
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Employee Name<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  disabled={this.state.disable}
                  type="text"
                  value={this.state.employeeName}
                  name="employeeName"
                  invalid={!isEmpty(errors.employeeName)}
                  onChange={this.onChangeInput}
                />
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  CNIC<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  disabled={this.state.disable}
                  type="text"
                  value={this.state.cnic}
                  name="cnic"
                  invalid={!isEmpty(errors.cnic)}
                  onChange={this.onChangeInput}
                />
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Date of Birth:<span style={{ color: "red" }}> *</span>
                </Label>
                <br />
                <Input
                  disabled={this.state.disable}
                  type="text"
                  value={this.state.dob}
                />
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Mother's Name<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  disabled={this.state.disable}
                  type="text"
                  value={this.state.motherName}
                  name="motherName"
                  invalid={!isEmpty(errors.motherName)}
                  onChange={this.onChangeInput}
                />
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Employee No:<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  disabled={this.state.disable}
                  type="text"
                  value={this.state.empNo}
                  name="empNo"
                  invalid={!isEmpty(errors.employeeNo)}
                  onChange={this.onChangeInput}
                />
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Department<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="text"
                  value={this.state.department}
                  name="department"
                  invalid={!isEmpty(errors.department)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Department"
                />
                <p className="error">{errors.department}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Phone/Ext<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="text"
                  maxLength="11"
                  value={this.state.phone_ext}
                  name="phone_ext"
                  invalid={!isEmpty(errors.phoneExt)}
                  onChange={this.onChangeInputNumber}
                  placeholder="Enter Phone No."
                />
                <p className="error">{errors.phoneExt}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Mobile No:<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  minLength="11"
                  maxLength="11"
                  pattern="\d*"
                  type="text"
                  value={this.state.mobileNo}
                  name="mobileNo"
                  invalid={!isEmpty(errors.mobileNo)}
                  onChange={this.onChangeInputNumber}
                  placeholder="03351574180"
                />
                <p className="error">{errors.mobileNo}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Designation:<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="text"
                  value={this.state.designation}
                  name="designation"
                  invalid={!isEmpty(errors.designation)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Designation"
                />
                <p className="error">{errors.designation}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Email<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="email"
                  value={this.state.email}
                  name="email"
                  invalid={!isEmpty(errors.email)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Email"
                />
                <p className="error">{errors.email}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>Country</Label>
                <Select
                  value={this.state.countryObject}
                  onChange={this.setcountryValue}
                  options={this.state.countryList}
                />
                <p className="error">{errors.countryId}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>City</Label>
                <Select
                  value={this.state.cityObject}
                  onChange={this.setcityValue}
                  options={this.state.cityList}
                />
                <p className="error">{errors.cityId}</p>
              </FormGroup>
            </Col>

            <Col md="4">
              <FormGroup>
                <Label>GL Limit Class:</Label>
                <Select
                  isDisabled={this.state.disable}
                  onChange={this.setglLimitClassValue}
                  options={this.state.glLimitClassList}
                />
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Status<span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  value={this.state.statusObject}
                  onChange={this.setstatusValue}
                  options={this.state.statusList}
                />
                <p className="error">{errors.status}</p>
              </FormGroup>
            </Col>
          </Row>
        </div>
      );
    }

    return (
      <div>
        <Modal
          id="addusermodel"
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader>
            Update User
            <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("addusermodel")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.props.modelOpen}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button
                disabled={this.state.disableSubmitButton}
                type="submit"
                color="success"
              >
                Update Register User
              </Button>
              <Button color="secondary" onClick={this.props.modelOpen}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

AddUserRowModal.propType = {
  getRoleLov: PropTypes.func.isRequired, //STORE
  errors: PropTypes.object.isRequired, //STORE
  lov: PropTypes.array.isRequired, //STORE
  modelOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.object.isRequired,
  updateUser: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
  lov: state.lov, //STORE
});

export default connect(mapStateToProps, {
  getRoleLov,
  updateUser,
  getCountryLov,
  getCityLov,
})(AddUserRowModal);
