import validator from "validator";
import isEmptyCheck from "../../../../utils/isEmpty";

const userValidation = (data) => {
  let errors = {};
  const username = isEmptyCheck(data.username)
    ? ""
    : data.username.trim().toString();
  const employeeNo = isEmptyCheck(data.employeeNo)
    ? ""
    : data.employeeNo.toString();
  const email = isEmptyCheck(data.email) ? "" : data.email.toString();
  const employeeName = isEmptyCheck(data.employeeName)
    ? ""
    : data.employeeName.toString();
  const cnic = isEmptyCheck(data.cnic) ? "" : data.cnic.toString();
  const dob = isEmptyCheck(data.dob.toString()) ? "" : data.dob.toString();
  const status = isEmptyCheck(data.status) ? "" : data.status.toString();
  const motherName = isEmptyCheck(data.motherName)
    ? ""
    : data.motherName.toString();
  const department = isEmptyCheck(data.department)
    ? ""
    : data.department.toString();
  const roleId = isEmptyCheck(data.roleId) ? "" : data.roleId.toString();
  const mobileNo = isEmptyCheck(data.mobileNo) ? "" : data.mobileNo.toString();
  const designation = isEmptyCheck(data.designation)
    ? ""
    : data.designation.toString();
  const cityId = isEmptyCheck(data.cityId) ? "" : data.cityId.toString();
  const countryId = isEmptyCheck(data.countryId)
    ? ""
    : data.countryId.toString();
  if (validator.isEmpty(username)) {
    errors.username = "User Name is required";
  }
  if (validator.isEmpty(employeeNo)) {
    errors.employeeNo = "Employee No is required";
  }

  if (validator.isEmpty(employeeName)) {
    errors.employeeName = "Employee Name is required";
  }
  if (validator.isEmpty(cnic)) {
    errors.cnic = "CNIC is required";
  }

  if (!validator.isEmail(email)) {
    errors.email = "Email is invalid";
  }
  if (validator.isEmpty(email)) {
    errors.email = "Email is required";
  }

  if (!validator.isEmpty(dob) && dob.toString() === "Invalid date") {
    errors.dob = "Date Of Birth is required";
  }
  if (validator.isEmpty(status)) {
    errors.status = "Status Is required";
  }
  if (validator.isEmpty(motherName)) {
    errors.motherName = "Mother name is required";
  }
  if (validator.isEmpty(department)) {
    errors.department = "Department  is required";
  }
  if (validator.isEmpty(roleId)) {
    errors.roleId = "Please Select Role";
  }

  if (validator.isEmpty(mobileNo)) {
    errors.mobileNo = "Mobile Number is required";
  }
  if (validator.isEmpty(designation)) {
    errors.designation = "Designation is required";
  }
  if (validator.isEmpty(cityId)) {
    errors.cityId = "City is required";
  }
  if (validator.isEmpty(countryId)) {
    errors.countryId = "Country is required";
  }

  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors,
  };
  return feedback;
};
export default userValidation;
