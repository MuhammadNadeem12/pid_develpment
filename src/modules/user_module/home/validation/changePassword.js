import validator from "validator";
import isEmptyCheck from "../../../../utils/isEmpty";

const changePassword = (data) => {
  let errors = {};
  const oldPass = isEmptyCheck(data.oldPass)
    ? ""
    : data.oldPass.trim().toString();
  const newPass = isEmptyCheck(data.newPass)
    ? ""
    : data.newPass.trim().toString();
  const confirmPass = isEmptyCheck(data.confirmPass)
    ? ""
    : data.confirmPass.trim().toString();

  if (!validator.equals(newPass, confirmPass)) {
    errors.confirmPass = "Both passwords must be same";
  }
  if (validator.equals(oldPass, newPass)) {
    errors.newPass =
      "Current And New Passwords are same, Please Change Password";
  }
  if (validator.isEmpty(newPass)) {
    errors.newPass = "New Password is required";
  } else {
    var decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    if (newPass.match(decimal)) {
      console.log("Done");
    } else {
      errors.newPass = "Please Follow The Password Policy";
    }
  }
  if (validator.isEmpty(oldPass)) {
    errors.oldPass = "Old password is required";
  }
  if (validator.isEmpty(confirmPass)) {
    errors.confirmPass = "Re-new password  is required";
  }

  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors,
  };
  return feedback;
};
export default changePassword;
