import validator from "validator";
import isEmptyCheck from "../../../../utils/isEmpty";

const changePassword = (data) => {
  console.log(data);
  let errors = {};
  const userName = isEmptyCheck(data.userName)
    ? ""
    : data.userName.trim().toString();

  if (validator.isEmpty(userName)) {
    errors.userName = "User Name is required";
  }

  const feedback = {
    haserror: !isEmptyCheck(errors),
    errors,
  };
  return feedback;
};
export default changePassword;
