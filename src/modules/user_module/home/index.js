import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getCityLov } from "../../../redux/lov/actions";
import {
  Card,
  Badge,
  CardBody,
  Row,
  Col,
  CardHeader,
  // Input
} from "reactstrap";
import { DataTable } from "primereact/datatable";
import { Button } from "primereact/button";
import { Column } from "primereact/column";
import { getUsers } from "../../../redux/user_module/actions";
import UpdateUserModel from "../../user_module/home/updateUserRowModal";
import AddUserModel from "../../user_module/home/addUserRowModal";
import LoadingOverlay from "react-loading-overlay";
import isEmpty from "../../../utils/isEmpty";
import moment from "moment";
//import roleRight from "../../../utils/checkRights";
import { withRouter } from "react-router-dom";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      expandedRows: null,
      updateModel: false,
      modelOpen: false,
      globalFilter: null,
      row: {},
      loading: true,
    };
    document.title = "Manage Users :: PID";
  }
  showModel = () => {
    this.setState({ modelOpen: !this.state.modelOpen });
  };
  showUpdateModel = (item) => {
    if (!isEmpty(item)) {
      if (!isEmpty(item.lkpCountry)) {
        if (!isEmpty(item.lkpCountry.countryId)) {
          this.props.getCityLov(1);
        }
      }
    }

    this.setState({ updateModel: !this.state.updateModel, row: item });
  };

  componentDidMount = async () => {
    this.props.getUsers();
  };

  formateDate = (rowData) => {
    if (!isEmpty(rowData.dob)) {
      let dob_date = moment(rowData.dob).format("YYYY-MM-DD");
      return dob_date;
    }
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.users && nextProps.users.users) {
      this.setState({
        users: nextProps.users.users,
        loading: nextProps.users.loading,
      });
    }
  }

  actionTemplate = (rowData, column) => {
    return (
      <div style={{ textAlign: "center" }}>
        <Button
          type="button"
          icon="pi pi-pencil"
          className="p-button-warning"
          tooltip="Edit User"
          tooltipOptions={{ position: "left" }}
          onClick={this.showUpdateModel.bind(this, rowData)}
        ></Button>
      </div>
    );
  };

  updateDetails = (id) => {};

  showDetails = (id) => {
    alert(id);
  };

  statusTemplate = (rowData, column) => {
    return (
      <div>
        {rowData &&
        !isEmpty(rowData) &&
        rowData.status &&
        !isEmpty(rowData.status) &&
        rowData.status === "A" ? (
          <Badge color="info"> Active </Badge>
        ) : (
          <Badge color="secondary"> InActive </Badge>
        )}
      </div>
    );
  };

  render() {
    // const header = (
    //   <div style={{ textAlign: "left" }}>
    //     <i className="pi pi-search" style={{ margin: "4px 4px 0 0" }}></i>
    //     <Input
    //       style={{ width: "320px" }}
    //       type="search"
    //       onInput={(e) => this.setState({ globalFilter: e.target.value })}
    //       placeholder="Global Search"
    //     />
    //   </div>
    // );

    let content = "";

    content = (
      <LoadingOverlay active={this.state.loading} spinner text="Please wait...">
        <Row>
          {/* <Col md="4"></Col>
          <Col md="4">
            {" "}
            <InputGroup>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Username"
              />
              <InputGroupAddon addonType="prepend"></InputGroupAddon>
              <BButton color="info">
                <i className="fa fa-search"> Search</i>
              </BButton>
            </InputGroup>
          </Col>
          <Col md="4"></Col> */}

          <Col md="12" style={{ marginTop: "15px" }}>
            <DataTable
              className="table-bordered"
              responsive={true}
              emptyMessage="No records found"
              value={this.state.users}
              // header={header}
              rowExpansionTemplate={this.rowExpansionTemplate}
              dataKey="userId"
              style={{ textAlign: "center" }}
              paginator={true}
              rows={10}
              rowsPerPageOptions={[10, 20, 30]}
            >
              <Column
                sortable={true}
                filter={true}
                field="username"
                header="User Name"
              />
              <Column
                sortable={true}
                filter={true}
                field="tblRole.roleDescr"
                header="Role"
              />
              <Column
                sortable={true}
                filter={true}
                field="employeeNo"
                header="Emp No"
              />
              <Column
                sortable={true}
                filter={true}
                field="employeeName"
                header="Emp Name"
              />
              <Column
                sortable={true}
                filter={true}
                field="cnic"
                header="CNIC"
              />
              <Column
                field="dob"
                sortable={true}
                filter={true}
                body={this.formateDate}
                header="Date Of Birth"
              />
              <Column
                field="designation"
                header="Designation"
                sortable={true}
                filter={true}
              />
              <Column
                field="department"
                header="Department"
                sortable={true}
                filter={true}
              />
              <Column
                field="mobileNo"
                header="Mobile No"
                sortable={true}
                filter={true}
              />
              <Column
                body={this.statusTemplate}
                sortable={true}
                filter={true}
                field="status"
                header="Status"
              />
              <Column
                body={this.actionTemplate}
                field="actions"
                header="Actions"
              />
            </DataTable>
          </Col>
        </Row>
      </LoadingOverlay>
    );

    return (
      <Row>
        <AddUserModel
          modelOpen={this.showModel}
          isOpen={this.state.modelOpen}
        />
        {
          <UpdateUserModel
            modelOpen={this.showUpdateModel}
            defaultValue={this.state.row}
            isOpen={this.state.updateModel}
          />
        }
        <Col>
          <Card>
            <CardHeader>
              <h5>
                Manage Users
                <Button
                  icon="pi pi-plus"
                  tooltip="Register User"
                  tooltipOptions={{ position: "left" }}
                  onClick={this.showModel}
                  className=" float-right p-button-success"
                ></Button>
              </h5>
            </CardHeader>
            <CardBody>
              <div className="content-section implementation">{content}</div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

Home.propTypes = {
  getUsers: PropTypes.func.isRequired,
  getCityLov: PropTypes.func.isRequired,
  users: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.users,
  auth: state.auth,
});

export default connect(mapStateToProps, { getUsers, getCityLov })(
  withRouter(Home)
);
