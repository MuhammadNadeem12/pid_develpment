import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  changeSpecificUserPassword,
  getUserAgaintUserName,
} from "../../../redux/user_module/actions";

import {
  Row,
  Col,
  Card,
  CardBody,
  CardHeader,
  FormGroup,
  Label,
  Button as ButtonBoot,
  Input,
} from "reactstrap";
import changePasswordValidation from "./validation/changeUserPassword";
import LoadingOverlay from "react-loading-overlay";
const initialState = {
  userName: "",
  user: "",
  errors: {},
  userflag: false,
  loading: false,
};
class ChangeSpecificUserPassword extends Component {
  constructor() {
    super();
    this.state = initialState;
    this.onBlurUserName = this.onBlurUserName.bind(this);
    document.title = "Change Specific User Password";
  }

  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onBlurUserName = (e) => {
    let name = e.target.value;
    this.props.getUserAgaintUserName(name.toUpperCase());
  };
  changeUserPassword = (e) => {
    e.preventDefault();
    this.setState({ errors: {} });

    const data = {
      userName: this.state.userName.toUpperCase(),
    };
    const { haserror, errors } = changePasswordValidation(data);

    if (haserror) {
      this.setState({ errors });
    } else {
      this.props.changeSpecificUserPassword(data);
      this.setState({ errors: {} });
    }
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.user && nextProps.user.useraginstusername) {
      this.setState({
        user: nextProps.user.useraginstusername,
        userflag: true,
      });
    }
    if (nextProps.users && nextProps.users.password) {
      this.setState({ loading: nextProps.users.loading });
    }
  }

  render() {
    let user1 = this.state.user;
    let content = "";

    const { errors } = this.state;

    content = (
      <LoadingOverlay active={this.state.loading} spinner text="Please wait...">
        <Row>
          <Col md="5">
            <Col md="12">
              <FormGroup>
                <Label>User Name</Label>
                <Input
                  type="text"
                  disabled={this.state.userflag}
                  value={this.state.userName}
                  name="userName"
                  onBlur={this.onBlurUserName}
                  onChange={this.onChangeInput}
                  placeholder="Enter Current Password"
                />
                <p className="error">{errors.userName}</p>
              </FormGroup>
            </Col>
            <Col md="12">
              <FormGroup>
                <Label>Employee No</Label>
                <Input
                  disabled={true}
                  onChange={this.onChangeInput}
                  value={user1.employeeNo}
                />
              </FormGroup>
            </Col>
            <Col md="12">
              <FormGroup>
                <Label>Designation</Label>
                <Input
                  disabled={true}
                  onChange={this.onChangeInput}
                  value={user1.designation}
                />
              </FormGroup>
            </Col>
            <Col md="12">
              <FormGroup>
                <Label>Department</Label>
                <Input
                  disabled={true}
                  onChange={this.onChangeInput}
                  value={user1.department}
                />
              </FormGroup>
            </Col>

            <Col md="12">
              <FormGroup>
                <ButtonBoot onClick={this.changeUserPassword} color="success">
                  Change Password
                </ButtonBoot>
              </FormGroup>
            </Col>
          </Col>
        </Row>
      </LoadingOverlay>
    );

    return (
      <Row>
        <Col>
          <Card>
            <CardHeader>
              <h5>Change Specific User Password</h5>
            </CardHeader>
            <CardBody>
              <div className="content-section implementation">{content}</div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

ChangeSpecificUserPassword.propTypes = {
  changeSpecificUserPassword: PropTypes.func.isRequired, //STORE
  getUserAgaintUserName: PropTypes.func.isRequired, //STORE
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
  user: state.users, //STORE
});

export default connect(mapStateToProps, {
  changeSpecificUserPassword,
  getUserAgaintUserName,
})(ChangeSpecificUserPassword);
