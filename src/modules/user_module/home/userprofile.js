import React, { Component } from "react";
import { connect } from "react-redux";
import userimage from "../../../assets/Profileavatar.jpg";
//import roleRight from "../../../utils/checkRights";
import { loginUserprofile } from "../../../redux/user_module/actions";
import PropTypes from "prop-types";

import {
  Row,
  Col,
  Card,
  Badge,
  CardBody,
  CardHeader,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
//import isEmpty from "../../../utils/isEmpty";
import { Button } from "primereact/button";
const initialState = {
  disable: true,
  user: {},
  camerapicture: userimage,
};
class UserProfile extends Component {
  constructor() {
    super();
    this.state = initialState;
    document.title = "Profile";
  }

  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount = async () => {
    // this.setState({ user: JSON.parse(localStorage.getItem("user")) });
    // let responce = await roleRight("/dfs/userprofile");
    // console.log(responce);
    // if (!isEmpty(responce)) {
    //   if (responce < 0) {
    //     this.props.history.push("/");
    //   } else {
    //     this.props.loginUserprofile();
    //   }
    // }
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.users && nextProps.users.userprofile) {
      this.setState({
        user: nextProps.users.userprofile,
        loading: nextProps.users.loading,
      });
    }
    // console.log(nextProps);
    // console.log("test");
  }

  actionTemplate = (rowData, column) => {
    return (
      <div style={{ textAlign: "center" }}>
        <Button
          type="button"
          icon="pi pi-pencil"
          className="p-button-warning"
          tooltip="Edit User"
          tooltipOptions={{ position: "left" }}
          onClick={this.showUpdateModel.bind(this, rowData)}
        ></Button>
      </div>
    );
  };

  statusTemplate = (rowData, column) => {
    return (
      <div>
        {rowData.status && rowData.status === "Y" ? (
          <Badge color="info">Active</Badge>
        ) : (
          <Badge color="secondary">InActive</Badge>
        )}
      </div>
    );
  };

  render() {
    let { lkpCity } = this.state.user;
    if (lkpCity) {
      let { cityDescr } = lkpCity.cityDescr;
      if (cityDescr) {
      }
    }

    let content = "";

    // const { loading } = this.state;
    // if (loading) {
    //   content = <Loading />;
    // } else

    content = (
      <Row>
        <Col md="2">
          <img
            src={this.state.camerapicture}
            alt={this.state.camerapicture}
            style={{ width: "100%", height: "200px" }}
            className="shadow-sm"
          />
        </Col>
        <Col md="10">
          <Card>
            <Row>
              <Col md="12">
                <h3>Personal Details</h3>
                <hr />
              </Col>
            </Row>
            <Row>
              <Col md="4">
                <FormGroup>
                  <Label>Emp No#</Label>
                  <Input
                    disabled={this.state.disable}
                    type="text"
                    value={this.state.user.employeeNo}
                    name="current_pass"
                    onChange={this.onChangeInput}
                  />
                </FormGroup>
              </Col>
              <Col md="4">
                <FormGroup>
                  <Label> Name</Label>
                  <Input
                    disabled={this.state.disable}
                    type="text"
                    value={this.state.user.username}
                    name="current_pass"
                    onChange={this.onChangeInput}
                  />
                </FormGroup>
              </Col>

              <Col md="4">
                <FormGroup>
                  <Label>Mother Name</Label>
                  <Input
                    disabled={this.state.disable}
                    type="text"
                    value={this.state.user.motherName}
                    name="current_pass"
                    onChange={this.onChangeInput}
                  />
                </FormGroup>
              </Col>
              <Col md="4">
                <FormGroup>
                  <Label>CNIC</Label>
                  <Input
                    disabled={this.state.disable}
                    type="text"
                    value={this.state.user.cnic}
                    name="current_pass"
                    onChange={this.onChangeInput}
                  />
                </FormGroup>
              </Col>
              <Col md="4">
                <FormGroup>
                  <Label>Phone Number</Label>
                  <Input
                    disabled={this.state.disable}
                    type="text"
                    value={this.state.user.mobileNo}
                    name="current_pass"
                    onChange={this.onChangeInput}
                  />
                </FormGroup>
              </Col>
              <Col md="4">
                <FormGroup>
                  <Label>Email Address</Label>
                  <Input
                    disabled={this.state.disable}
                    type="text"
                    value={this.state.user.email}
                    name="current_pass"
                    onChange={this.onChangeInput}
                  />
                </FormGroup>
              </Col>
              <Col md="4">
                <FormGroup>
                  <Label>Desigination</Label>
                  <Input
                    disabled={this.state.disable}
                    type="text"
                    value={this.state.user.designation}
                    name="current_pass"
                    onChange={this.onChangeInput}
                  />
                </FormGroup>
              </Col>
              <Col md="4">
                <FormGroup>
                  <Label>Department</Label>
                  <Input
                    disabled={this.state.disable}
                    type="text"
                    value={this.state.user.department}
                    name="current_pass"
                    onChange={this.onChangeInput}
                  />
                </FormGroup>
              </Col>
              <Col md="4">
                <FormGroup>
                  <Label>Phone Ext</Label>
                  <Input
                    disabled={this.state.disable}
                    type="text"
                    value={this.state.user.phoneExt}
                    name="current_pass"
                    onChange={this.onChangeInput}
                  />
                </FormGroup>
              </Col>
              <Col md="4">
                <FormGroup>
                  <Label>Status</Label>
                  <Input
                    disabled={this.state.disable}
                    type="text"
                    value={
                      this.state.user.status === "A" ? "ACTIVE" : "INACTIVE"
                    }
                    name="current_pass"
                    onChange={this.onChangeInput}
                  />
                </FormGroup>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    );

    return (
      <Row>
        <Col>
          <Card>
            <CardHeader>
              <h5>User Profile</h5>
            </CardHeader>
            <CardBody>
              <div className="content-section implementation">{content}</div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

UserProfile.propTypes = { loginUserprofile: PropTypes.func.isRequired };

const mapStateToProps = (state) => ({ users: state.users });

export default connect(mapStateToProps, { loginUserprofile })(UserProfile);
