import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { changeUserPassword } from "../../../redux/user_module/actions";
import { logoutUser } from "../../../redux/global_auth/actions";

import {
  Row,
  Col,
  Card,
  CardBody,
  CardHeader,
  FormGroup,
  Label,
  Button as ButtonBoot,
  Input,
  InputGroup,
  InputGroupText,
} from "reactstrap";
import changePasswordValidation from "./validation/changePassword";
import isEmpty from "../../../utils/isEmpty";
import roleRight from "../../../utils/checkRights";
const initialState = {
  currentpass: "",
  newpass: "",
  confirmpass: "",
  loading: false,
  errors: {},
};
class ChangeUserPassword extends Component {
  constructor() {
    super();
    this.state = initialState;
    document.title = "Change User Password";
  }
  componentDidMount = async () => {
    // let responce = await roleRight("/dfs/changepassword");
    // if (!isEmpty(responce)) {
    //   if (responce < 0) {
    //     this.props.history.push("/");
    //   }
    // }
  };

  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  changeUserPassword = (e) => {
    e.preventDefault();
    this.setState({ errors: {} });

    const data = {
      oldPass: this.state.currentpass,
      newPass: this.state.newpass,
      confirmPass: this.state.confirmpass,
    };
    const { haserror, errors } = changePasswordValidation(data);

    if (haserror) {
      this.setState({ errors });
    } else {
      this.props.changeUserPassword(data);
      this.setState({ errors: {} });
      this.setState({ initialState });
    }
  };

  showCPassword = () => {
    var x = document.getElementById("cpassword");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  };

  showNPassword = () => {
    var x = document.getElementById("npassword");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  };
  showRPassword = () => {
    var x = document.getElementById("rpassword");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  };
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.users && !isEmpty(nextProps.users.password)) {
      this.setState({ loading: true });
      const data = {};
      this.props.logoutUser(data);
    }
  }

  render() {
    let content = "";
    const { errors } = this.state;
    content = (
      <Row>
        <Col md="5">
          <Col md="12">
            <FormGroup>
              <h3 style={{ color: "red" }}>
                Password Policy <i className="icon-lock"></i>
              </h3>
              <hr />
              <br />
              <ul style={{ color: "green" }}>
                <li>8 to 15 characters</li>
                <li>One lowercase</li>
                <li>One uppercase letter</li>
                <li>One numeric digit</li>
                <li>One special character</li>
              </ul>
            </FormGroup>
            <hr />
          </Col>

          <Col md="12">
            <FormGroup>
              <Label>Current Password</Label>
              <InputGroup className="mb-4">
                <InputGroupText>
                  <i onClick={this.showCPassword} className="icon-eye"></i>
                </InputGroupText>
                <Input
                  id="cpassword"
                  disabled={this.state.loading}
                  type="password"
                  value={this.state.currentpass}
                  name="currentpass"
                  onChange={this.onChangeInput}
                  placeholder="Enter Current Password"
                />
              </InputGroup>
              <p className="error">{errors.oldPass}</p>
            </FormGroup>
          </Col>
          <Col md="12">
            <FormGroup>
              <Label>New Password</Label>
              <InputGroup className="mb-4">
                <InputGroupText>
                  <i onClick={this.showNPassword} className="icon-eye"></i>
                </InputGroupText>
                <Input
                  id="npassword"
                  disabled={this.state.loading}
                  type="password"
                  value={this.state.newpass}
                  name="newpass"
                  onChange={this.onChangeInput}
                  placeholder="Enter New Password"
                />
              </InputGroup>
              <p className="error">{errors.newPass}</p>
            </FormGroup>
          </Col>

          <Col md="12">
            <FormGroup>
              <Label>Re-enter New Password</Label>
              <InputGroup className="mb-4">
                <InputGroupText>
                  <i onClick={this.showRPassword} className="icon-eye"></i>
                </InputGroupText>
                <Input
                  id="rpassword"
                  disabled={this.state.loading}
                  type="password"
                  value={this.state.confirmpass}
                  name="confirmpass"
                  onChange={this.onChangeInput}
                  placeholder="Enter Confirm Password"
                />
              </InputGroup>
              <p className="error">{errors.confirmPass}</p>
            </FormGroup>
          </Col>

          <Col md="12">
            <FormGroup>
              <ButtonBoot
                disabled={this.state.loading}
                onClick={this.changeUserPassword}
                color="success"
              >
                Change Password
              </ButtonBoot>
            </FormGroup>
          </Col>
        </Col>
      </Row>
    );

    return (
      <Row>
        <Col>
          <Card>
            <CardHeader>
              <h5>Change User Password</h5>
            </CardHeader>
            <CardBody>
              <div className="content-section implementation">{content}</div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

ChangeUserPassword.propTypes = {
  changeUserPassword: PropTypes.func.isRequired, //STORE
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
  users: state.users, //STORE
});

export default connect(mapStateToProps, { changeUserPassword, logoutUser })(
  ChangeUserPassword
);
