import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  getRoleLov,
  getCityLov,
  getCountryLov,
} from "../../../redux/lov/actions";
import { addUser, getUsers } from "../../../redux/user_module/actions";
import userValidation from "./validation/createUser";
import isEmpty from "../../../utils/isEmpty";
import Select from "react-select";
import Loading from "../../../utils/spinner";
import FullScreen from "../../../utils/maximize-dialog";
import { Calendar } from "primereact/calendar";
import validator from "validator";
import postAPI from "../../../utils/postAPITemplate";
import moment from "moment";
import {
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

const initialState = {
  status: "",
  statusList: [
    { value: "", label: "Select an Option" },
    { value: "A", label: "Activate" },
    { value: "I", label: "De-activate" },
  ],
  roleid: null,
  cityId: "",
  countryId: "",
  errors: {},
  role: [],
  email: "",
  userid: "",
  employeeName: "",
  cnic: "",
  dob: "",
  motherName: "",
  empNo: "",
  department: "",
  phone_ext: "",
  mobileNo: "",
  designation: "",
  cityList: [],
  countryList: [],
  glLimitClass: "",
  glLimitClassList: [],
  disableSubmitButton: false,
};

class AddUserRowModal extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }
  onChangeInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    this.props.getRoleLov();
    this.props.getCountryLov();
  }
  onChangeInputNumber = (e) => {
    if (e.target && !isEmpty(e.target) && validator.isNumeric(e.target.value)) {
      this.setState({ [e.target.name]: e.target.value });
    } else if (e.target.value.length === 0) {
      this.setState({ [e.target.name]: "" });
    }
  };

  formSubmit = async (e) => {
    e.preventDefault();
    this.setState({ errors: {} });
    const dob_date = moment(this.state.dob, "MM-DD-YYYY HH:mm:ss", true).format(
      "YYYY-MM-DD HH:mm:ss"
    );
    const data = {
      username: this.state.userid.toUpperCase(),
      employeeNo: this.state.empNo,
      employeeName: this.state.employeeName.toUpperCase(),
      cnic: this.state.cnic,
      dob: dob_date,
      motherName: this.state.motherName.toUpperCase(),
      department: this.state.department.toUpperCase(),
      roleId: this.state.roleid,
      email: this.state.email,
      mobileNo: this.state.mobileNo,
      phoneExt: this.state.phone_ext,
      designation: this.state.designation.toUpperCase(),
      countryId: this.state.countryId,
      cityId: this.state.cityId,
      status: this.state.status,
      url: "dfs/createUser",
      showMessage: "S",
    };

    const { haserror, errors } = userValidation(data);
    if (haserror) {
      console.log(errors);
      this.setState({ errors });
    } else {
      this.setState({ disableSubmitButton: true });
      const response = await postAPI(data);
      this.userResponse(response);
    }
  };

  userResponse = (response) => {
    if (response && !isEmpty(response)) {
      this.props.getUsers();
      this.setState({ errors: {} });
      this.setState(initialState);
      this.props.modelOpen();
    } else this.setState({ loading: false });
  };
  cancelDialog = () => {
    this.props.modelOpen();
    this.setState(initialState);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.lov && nextProps.lov.role_lov) {
      this.setState({
        role: nextProps.lov.role_lov.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    }
    if (nextProps && nextProps.lov && nextProps.lov.city_lov) {
      this.setState({
        cityList: nextProps.lov.city_lov.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    }
    if (nextProps && nextProps.lov && nextProps.lov.country_lov) {
      this.setState({
        countryList: nextProps.lov.country_lov.map((item) => {
          return {
            value: item.code,
            label: item.descr,
          };
        }),
      });
    }
  }

  setRoleValue = (e) => {
    this.setState({ roleid: e.value });
  };
  setcityValue = (e) => {
    console.log(e);
    this.setState({ cityId: e.value });
  };
  setcountryValue = (e) => {
    this.props.getCityLov(e.value);
    this.setState({ countryId: e.value });
  };
  setglLimitClassValue = (value) => {
    this.setState({ glLimitClass: value });
  };
  setstatusValue = (e) => {
    this.setState({ status: e.value });
  };
  render() {
    const { errors, loading } = this.state;
    let content = "";
    if (loading) {
      content = <Loading />;
    } else {
      content = (
        <div>
          <Row>
            <Col md="6">
              <FormGroup>
                <Label>
                  Registered User as :<span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  onChange={this.setRoleValue}
                  options={this.state.role}
                />
                <p className="error">{errors.roleId}</p>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md="4">
              <FormGroup>
                <Label>
                  User ID<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="text"
                  value={this.state.userid}
                  name="userid"
                  invalid={!isEmpty(errors.username)}
                  onChange={this.onChangeInput}
                  placeholder="Enter User ID"
                />
                <p className="error">{errors.username}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Employee Name<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="text"
                  value={this.state.employeeName}
                  name="employeeName"
                  invalid={!isEmpty(errors.employeeName)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Employee Name"
                />
                <p className="error">{errors.employeeName}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  CNIC<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  minLength="13"
                  maxLength="13"
                  pattern="\d*"
                  type="text"
                  value={this.state.cnic}
                  name="cnic"
                  invalid={!isEmpty(errors.cnic)}
                  onChange={this.onChangeInputNumber}
                  placeholder="3740512345677"
                />
                <p className="error">{errors.cnic}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Date of Birth:<span style={{ color: "red" }}> *</span>
                </Label>
                <br />
                <Calendar
                  yearNavigator={true}
                  yearRange="1980:2030"
                  value={this.state.dob}
                  name="dob"
                  onFocus={() => {
                    setTimeout(() => {
                      const element = document.getElementsByClassName(
                        "p-datepicker"
                      );
                      element[0].style.top = null;
                    }, 200);
                  }}
                  hideOnDateTimeSelect={true}
                  maxDate={new Date()}
                  dateFormat="dd/mm/yy"
                  onChange={this.onChangeInput}
                />

                <p className="error">{errors.dob}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Mother's Name<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="text"
                  value={this.state.motherName}
                  name="motherName"
                  invalid={!isEmpty(errors.motherName)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Mother's Name"
                />
                <p className="error">{errors.motherName}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Employee No:<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="text"
                  value={this.state.empNo}
                  name="empNo"
                  invalid={!isEmpty(errors.employeeNo)}
                  onChange={this.onChangeInputNumber}
                  placeholder="Enter Employee No."
                />
                <p className="error">{errors.employeeNo}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Department<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="text"
                  value={this.state.department}
                  name="department"
                  invalid={!isEmpty(errors.department)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Department"
                />
                <p className="error">{errors.department}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>Phone/Ext</Label>
                <Input
                  type="text"
                  maxLength="11"
                  value={this.state.phone_ext}
                  name="phone_ext"
                  invalid={!isEmpty(errors.phoneExt)}
                  onChange={this.onChangeInputNumber}
                  placeholder="Enter Phone No."
                />
                <p className="error">{errors.phoneExt}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Mobile No:<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  minLength="11"
                  maxLength="11"
                  pattern="\d*"
                  type="text"
                  value={this.state.mobileNo}
                  name="mobileNo"
                  invalid={!isEmpty(errors.mobileNo)}
                  onChange={this.onChangeInputNumber}
                  placeholder="03351574180"
                />
                <p className="error">{errors.mobileNo}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Designation:<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="text"
                  value={this.state.designation}
                  name="designation"
                  invalid={!isEmpty(errors.designation)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Designation"
                />
                <p className="error">{errors.designation}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Email<span style={{ color: "red" }}> *</span>
                </Label>
                <Input
                  type="email"
                  value={this.state.email}
                  name="email"
                  invalid={!isEmpty(errors.email)}
                  onChange={this.onChangeInput}
                  placeholder="Enter Email"
                />
                <p className="error">{errors.email}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>Country</Label>
                <Select
                  onChange={this.setcountryValue}
                  options={this.state.countryList}
                />
                <p className="error">{errors.countryId}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>City</Label>
                <Select
                  onChange={this.setcityValue}
                  options={this.state.cityList}
                />
                <p className="error">{errors.cityId}</p>
              </FormGroup>
            </Col>

            <Col md="4">
              <FormGroup>
                <Label>GL Limit Class:</Label>
                <Select
                  onChange={this.setglLimitClassValue}
                  options={this.state.glLimitClassList}
                />
                <p className="error">{errors.glLimitClass}</p>
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup>
                <Label>
                  Status<span style={{ color: "red" }}> *</span>
                </Label>
                <Select
                  onChange={this.setstatusValue}
                  options={this.state.statusList}
                />
                <p className="error">{errors.status}</p>
              </FormGroup>
            </Col>
          </Row>
        </div>
      );
    }

    return (
      <div>
        <Modal
          id="addusermodel"
          size="lg"
          isOpen={this.props.isOpen}
          toggle={this.props.modelOpen}
        >
          <ModalHeader>
            Register User
            <div className="float-right pt-0 mt-0 btn-group">
              <button
                className="close p-0 m-0 maximize mr-2"
                style={{ marginRight: "5px" }}
                type="button"
                aria-label="Resize"
                onClick={() => FullScreen("addusermodel")}
              >
                <span aria-hidden="true"> ❐</span>
              </button>
              <button
                className="close p-0 m-0 "
                type="button"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.cancelDialog}
              >
                <span aria-hidden="true"> ×</span>
              </button>
            </div>
          </ModalHeader>
          <Form onSubmit={this.formSubmit}>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <Button
                disabled={this.state.disableSubmitButton}
                type="submit"
                color="success"
              >
                Register
              </Button>
              <Button color="secondary" onClick={this.cancelDialog}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

AddUserRowModal.propType = {
  getRoleLov: PropTypes.func.isRequired, //STORE
  errors: PropTypes.object.isRequired, //STORE
  lov: PropTypes.array.isRequired, //STORE
  modelOpen: PropTypes.func.isRequired,
  isOpen: PropTypes.object.isRequired,
  addUser: PropTypes.func.isRequired, //STORE
  getUsers: PropTypes.func.isRequired, //STORE
  getCityLov: PropTypes.func.isRequired, //STORE
  getCountryLov: PropTypes.func.isRequired, //STORE
};

const mapStateToProps = (state) => ({
  errors: state.errors, //STORE
  lov: state.lov, //STORE
});

export default connect(mapStateToProps, {
  getRoleLov,
  getCityLov,
  getCountryLov,
  addUser,
  getUsers,
})(AddUserRowModal);
